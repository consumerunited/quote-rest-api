This will be the quote  engine replacement removing the need for Python and making us scalable using better design principles

Since we want to expose this to our business unit, we need this to be fail proof and easy to to use!

Version 2.0 

How do I get set up?

* Checkout this project
* Make sure all resources are configured according to environment
* There should not be any dependencies, so self contained, if issues, please contact me @ billy.noel@goji.com
* For now all the DB connection strings are using using PROD, so be careful. The end plan will be to change all the MVN pom files to only package on -P flag on build
* We will be using Swagger to test manually at this time, but eventually I would like to be using Chef/Jenkins to auto-test all scenarios during the build process
* We hope to go to distributed platform, but that still needs to be hashed out, will update then

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* billy.noel@goji.com
* developers@goji.com