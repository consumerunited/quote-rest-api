package com.goji;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.goji.quote.rest.api.BootstrapServlet;
import com.goji.quote.rest.api.config.QuoteRestApiConfig;
import com.hmsonline.dropwizard.spring.SpringService;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import io.swagger.jersey.config.JerseyJaxrsConfig;
import javax.servlet.Registration;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

/**
 * Hello world!
 *
 */
public class Service extends SpringService<QuoteRestApiConfig> {

    @Override
    public void initialize(Bootstrap<QuoteRestApiConfig> bootstrap) {
        bootstrap.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        bootstrap.addBundle(new AssetsBundle("/webapp", "/swagger/", "index.html"));
        super.initialize(bootstrap);
    }

    @Override
    public void run(QuoteRestApiConfig configuration, Environment environment) throws ClassNotFoundException {

        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORSFilter", CrossOriginFilter.class);
        
        environment.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        environment.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        //filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, environment.getApplicationContext().getContextPath() + "quotes/*");
        filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, environment.getApplicationContext().getContextPath() + "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,OPTIONS,HEAD,DELETE,PATCH");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "x-requested-with, authorization, client-security-token, Origin, Content-Type, Accept");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

//        environment.jersey().setUrlPattern("/api/*");
        environment.jersey().register(new SwaggerSerializers());
        environment.jersey().register(new ApiListingResource());
        
        
        environment.servlets().addServlet("bootstrap", new BootstrapServlet()).setLoadOnStartup(1);
        final Registration.Dynamic jerseyConfig = environment.servlets().addServlet("jerseyConfig", new JerseyJaxrsConfig());
        jerseyConfig.setInitParameter("api.version", "1.0.0");
        jerseyConfig.setInitParameter("swagger.api.basepath", "http://localhost:8080");
//        jerseyConfig.setInitParameter("scan.all.resources", "true");
        
        super.run(configuration, environment);
    }

    
    public static void main(String[] args) throws Exception {
        final Service service = new Service();
        if (args.length == 0 || args[0].equalsIgnoreCase("local")) {
            service.runWithClasspathConfig("local.yml");
        } else if (args[0].equalsIgnoreCase("localmysql")) {
            service.runWithClasspathConfig("localmysql.yml");
        } else if (args[0].equalsIgnoreCase("test")) {
            service.runWithClasspathConfig("test.yml");
        } else if (args[0].equalsIgnoreCase("stage")) {
            service.runWithClasspathConfig("stage.yml");
        } else if (args[0].equalsIgnoreCase("prod")) {
            service.runWithClasspathConfig("prod.yml");
        } else {
            service.run(args);
        }
    }

}
