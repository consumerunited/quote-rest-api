/*
 * Copyright (C) Consumer United LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.goji.quote.rs.dto;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Getter
@Setter
public class CarrierAutoEstimate {
    
    @ApiModelProperty(value="The carrier for the estimate", position = 1)
    private String carrier;
    
    @ApiModelProperty(name = "monthly_payment_range_low", value = "The expected low value of the range of possible monthly premiums", position = 2)
    private BigDecimal monthlyPaymentRangeLow;

    @ApiModelProperty(name = "monthly_payment_range_high", value = "The expected high value of the range of possible monthly premiums", position = 3)
    private BigDecimal monthlyPaymentRangeHigh;
    
}
