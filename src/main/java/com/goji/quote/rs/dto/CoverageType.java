package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import static com.goji.quote.utils.EnumUtils.getEnumFromString;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum CoverageType {
    
    CUSTOM, MINIMUM, RECOMMENDED, PREMIUM;

    @JsonCreator
    public static CoverageType fromValue(String value) {
        return getEnumFromString(CoverageType.class, value);
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase();
    }

}
