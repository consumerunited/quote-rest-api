package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.goji.quote.utils.EnumUtils;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum VehicleOwnership {
    LEASED, LIEN, OWNED;

    @JsonCreator
    public static VehicleOwnership fromValue(String value) {
        return EnumUtils.getEnumFromString(VehicleOwnership.class, value);
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase();
    }
    
}
