package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.goji.quote.utils.EnumUtils;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum VehicleMake {
    ACURA, AUDI, BMW, BUICK, CADILLAC, CHEVROLET, CHRYSLER, DAEWOO, DODGE, EAGLE, FORD, GEO, GMC, HONDA, HUMMER, HYUNDAI, INFINITI, ISUZU, JAGUAR, JEEP, KIA, LAND_ROVER, LEXUS, LINCOLN, MAZDA, MERCEDES_BENZ, MERCURY, MINI, MITSUBISHI, NISSAN, OLDSMOBILE, PLYMOUTH, PONTIAC, PORSCHE, RAM, SAAB, SATURN, SUBARU, SUZUKI, TOYOTA, VOLKSWAGEN, VOLVO;

    @JsonCreator
    public static VehicleMake fromValue(String value) {
        try {
            return VehicleMake.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException exp) {
            //ignore
        }
        return EnumUtils.getEnumFromString(VehicleMake.class, value.replace(" ", "_"));
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase().replace("_", " ");
    }
    
}
