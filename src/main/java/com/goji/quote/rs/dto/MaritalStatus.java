package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.goji.quote.utils.EnumUtils;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum MaritalStatus {
    DIVORCED, DOMESTIC_PARTNER, MARRIED, SEPARATED, SINGLE, WIDOWED;

    @JsonCreator
    public static MaritalStatus fromValue(String value) {
        try {
            return MaritalStatus.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException exp) {
            //other
        }
        return EnumUtils.getEnumFromString(MaritalStatus.class, value.replace(" ", "_"));
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase().replace("_", " ");
    }
    
}
