package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.goji.quote.utils.EnumUtils;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum Gender {
    MALE, FEMALE;

    @JsonCreator
    public static Gender fromValue(String value) {
        return EnumUtils.getEnumFromString(Gender.class, value);
    }

    @JsonValue
    public String toJson() {
        return name().toLowerCase();
    }
    
}
