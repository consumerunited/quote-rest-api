package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public enum HomeOwnership {
    APARTMENT("apartment"), CONDO_OWNED("condo (owned)"), HOME_OWNED("home (owned)"), RENTAL("rental home/condo"), LIVE_WITH_PARENTS("live with parents"), MOBILE_HOME("mobile home"), OTHER("other");
    private static Map<String, HomeOwnership> VALUES = null;
    private final String value;

    private HomeOwnership(String value) {
        this.value = value;
    }

    @JsonCreator
    public static HomeOwnership fromValue(final String value) {
        try {
            return HomeOwnership.valueOf(value);
        } catch (IllegalArgumentException exp) {
            //ignore
        }
        if (VALUES == null) {
            final Map<String, HomeOwnership> vals = new HashMap<>();
            for (final HomeOwnership val : HomeOwnership.values()) {
                vals.put(val.value, val);
            }
            VALUES = vals;
        }
        return value != null ? VALUES.get(value.toLowerCase()) : null;
    }

    @JsonValue
    public String toJson() {
        return value;
    }
    
}
