/*
 * Copyright (C) Consumer United LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.goji.quote.rs.dto;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Getter
@Setter
public class AutoQuoteEstimate {

    @ApiModelProperty(name = "coverage_type", required = true, position = 0)
    private CoverageType coverageType;

    @Pattern(regexp = "100/100|100/300|1000/1000|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|state minimum")
    @ApiModelProperty(name = "bodily_injury_liability_coverage", value = "In the format {per person}/{per incident} or 'state minimum'. Maximum value for bodily injury liability coverage", required = true, position = 3, example="state minimum")
    private String bodilyInjuryLiabilityCoverage;

    @ApiModelProperty(name = "property_damage_liability_coverage", value = "Maximum dollar amount covered for property damage per accident. -1 implies state minimum", required = true, position = 4)
    private int propertyDamageLiabilityCoverage;

    @Pattern(regexp = "100/100|100/300|1000/1000|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|reject|state minimum")
    @ApiModelProperty(name = "uninsured_motorist_coverage", value = "In the format {per person}/{per incident} or 'state minimum'. Maximum value for un/underinsured motorist bodily injury and property damage coverage", required = true, position = 5, example = "reject")
    private String uninsuredMotoristCoverage;

    @Pattern(regexp = "[1-9][0-9]{3,5}|none")
    @ApiModelProperty(name = "medical_payments_coverage", value = "Value in dollars (no comma) or 'none'. Maximum value for medical payments for insured", required = true, position = 6, example="none")
    private String medicalPaymentsCoverage;

    @ApiModelProperty(name = "full_coverage", value = "Does coverage include collision and comprehensive?", required = true, position = 7)
    private boolean fullCoverage;

    @ApiModelProperty(name = "collision_deductible", value = "Per incident deductible for collision coverage", position = 8)
    private Integer collisionDeductible;

    @ApiModelProperty(name = "comprehensive_deductible", value = "Per incident deductible for comprehensive coverage", position = 9)
    private Integer comprehensiveDeductible;

    @ApiModelProperty(name = "monthly_payment_range_low", value = "The expected low value of the range of possible monthly premiums", position = 10)
    private BigDecimal monthlyPaymentRangeLow;

    @ApiModelProperty(name = "monthly_payment_range_high", value = "The expected high value of the range of possible monthly premiums", position = 11)
    private BigDecimal monthlyPaymentRangeHigh;

    @ApiModelProperty(name = "carrier_rates", value = "Only included if 'carrier_rates=true' passed on quote request.  A list of specific rates by carrier for this coverage type", position = 12)
    private List<CarrierAutoEstimate> carrierRates;

}
