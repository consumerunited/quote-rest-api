package com.goji.quote.rs.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.goji.orm.spring.validation.Phone;
import com.goji.quote.rs.api.AutoResource;
import io.dropwizard.validation.ValidationMethod;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;
import org.joda.time.Period;

/**
 *
 * @author pcm
 */
@Getter
@Setter
public class AutoQuickQuote {

    @ApiModelProperty(value = "Ignored during quote request. Returned by the API in response", position = 0)
    private String id;
    
    @Max(119)
    @ApiModelProperty(name = "driver_age", value = "Drivers age. Either this or drivers date of birth required", position = 1, example = "25")
    private Integer driverAge;

    @ApiModelProperty(name = "driver_date_of_birth", value = "Drivers date of birth. Either this or drivers age is required", position = 2, example = "1991-03-20")
    private LocalDate driverDateOfBirth;

    @ApiModelProperty(required = true, value = "Drivers Gender", position = 3, example = "MALE")
    @NotNull
    private Gender gender;

//    @Pattern(flags = Pattern.Flag.CASE_INSENSITIVE, regexp = AutoResource.STATE_REGEX)
    @NotNull
    @ApiModelProperty(required = true, value = "The garaging US state. See GET /auto/states for supported states", position = 4, example = "TX")
    private String state;

    @Pattern(regexp = "[0-9]{5}\\-?([0-9]{4})?")
    @ApiModelProperty(name = "zip_code", required = true, value = "The garaging zip code", position = 5, example = "77429-6762")
    @NotNull
    private String zipCode;

    @ApiModelProperty(value = "The make of the vehicle.  See GET /auto/makes for supported manufacturers", required = true, name = "vehicle_make", position = 6, example = "honda")
    @NotNull
    private String vehicleMake;

    @ApiModelProperty(name = "vehicle_cost_new", required = true, value = "The purchasing cost of the vehicle", position = 7, example = "2000")
    @NotNull
    private Integer vehicleCostNew;

    @ApiModelProperty(name = "vehicle_year", required = true, value = "The year of the vehicle", position = 8, example = "2015")
    @NotNull
    private Integer vehicleYear;

    @ApiModelProperty(name = "vehicle_ownership", required = true, value = "The ownership of the vehicle", position = 9, example = "LIEN")
    @NotNull
    private VehicleOwnership vehicleOwnership;

    @ApiModelProperty(value = "What are the primary driver's living arrangements?", required = true, name = "home_ownership", position = 10, example = "apartment")
    @NotNull
    private HomeOwnership homeOwnership;

    @NotNull
    @ApiModelProperty(name = "prior_carrier", required = true, value = "Name of the drivers most recent insurance carrier. See GET /auto/carriers for valid values", position = 11, example = "aaa")
    private String priorCarrier;

    @Pattern(regexp = AutoResource.LIABILITY_LIMITS_REGEX, flags = Pattern.Flag.CASE_INSENSITIVE)
    @ApiModelProperty(name = "prior_liability_limit", required = true, value = "Liability limit with current insurance provider (in thousands).  See GET /auto/liability_limits for valid values", position = 12, example = "100/100")
    private String priorLiabilityLimit;

    @ApiModelProperty(name = "first_name", value = "Driver's first name", position = 13, example = "John")
    private String firstName;

    @ApiModelProperty(name = "last_name", value = "Driver's last name", position = 14, example = "Smith")
    private String lastName;

    @Phone
    @ApiModelProperty(value = "Driver's phone number", position = 15, example = "702-688-9090")
    private String phone;

    @ApiModelProperty(name = "email", position = 16, example = "text@example.com")
    @Pattern(regexp = ".*\\@.*")
    private String email;

    @ApiModelProperty(name = "marital_status", value = "'Single' if not specified", position = 17, example = "SINGLE")
    @NotNull
    private MaritalStatus maritalStatus = MaritalStatus.SINGLE;

    @ApiModelProperty(name = "vehicle_model", value = "The model of the vehicle", position = 18, example = "civic")
    private String vehicleModel;

    @Min(0)
    @Max(99)
    @ApiModelProperty(name = "years_with_prior_carrier", value = "Number of years with the current insurance provider. Assumed 0, if not specified", position = 19, example = "2")
    private int yearsWithPriorCarrier = 0;

    @Min(0)
    @Max(99)
    @ApiModelProperty(name = "years_licensed", value = "Number of years with a U.S. driving license. Assumed 2, if not specified", position = 20, example = "9")
    private int yearsLicensed = 2;

    @ApiModelProperty(name = "prior_monthly_premium", value = "Monthly premium with current insurance provider", position = 21, example = "200")
    private Integer priorMonthlyPremium;

    @ApiModelProperty(name = "driver_count", value = "Number of drivers on the policy", position = 22, example = "1")
    private int driverCount = 1;

    @ApiModelProperty(name = "vehicle_count", value = "Number of cars in the policy", position = 23, example = "1")
    private int vehicleCount = 1;

    @ApiModelProperty(value = "Returned by the API in response. If specified in request, determines which types of coverages will be returned. By Default MINIMUM, RECOMMENDED and PREMIUM are returned.  If an estimate of coverage_type CUSTOM is specified in the request, its attributes are used for the quote.", position = 24)
    @Valid
    private List<AutoQuoteEstimate> estimates;
    
    @JsonIgnore
    @ApiModelProperty(hidden=true)
    private String ipAddress;
    
    @JsonIgnore
    @ApiModelProperty(hidden=true)
    private String error;

    @ValidationMethod(message = "Only one of the fields, driver_date_of_birth or driver_age is required but when both are present they must not contradict with each other")
    @ApiModelProperty(hidden = true, position = 50)
    @JsonIgnore
    public boolean isDateOfBirthValid() {

        if (driverAge != null && driverAge > 0) {
            if (driverDateOfBirth != null) {
                return driverAge == new Period(driverDateOfBirth, new LocalDate()).getYears();
            }
        } else if (driverDateOfBirth != null) {
           return true;
        } else {
            return false;
        }
        return true;

    }
}
