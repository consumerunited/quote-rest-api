
package com.goji.quote.rs.api;

import com.goji.quote.rs.dto.AutoQuickQuote;

/**
 *
 * @author pcm
 */
public interface IAutoService {
    AutoQuickQuote createQuote(AutoQuickQuote auto, boolean includeCarrierRates);
    AutoQuickQuote findQuote(String id);
}
