package com.goji.quote.rs.api.service;

import com.goji.quote.rest.api.legacy.LegacyQuoteRequest;
import static com.goji.quote.rest.api.legacy.LegacyQuoteRequest.STATE_MINIMUM;
import com.goji.quote.rest.api.legacy.LegacyQuoteResponse;
import com.goji.quote.rest.api.legacy.LegacyQuoteService;
import com.goji.quote.rs.api.IAutoService;
import com.goji.quote.rs.dto.AutoQuickQuote;
import com.goji.quote.rs.dto.AutoQuoteEstimate;
import com.goji.quote.rs.dto.CarrierAutoEstimate;
import com.goji.quote.rs.dto.CoverageType;
import com.goji.quote.rs.dto.HomeOwnership;
import com.goji.quote.rs.dto.VehicleMake;
import com.goji.quote.rs.dto.VehicleOwnership;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author pcm
 */
@Service
public class AutoService implements IAutoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutoService.class);

    @Autowired
    LegacyQuoteService quoteService;

    @Autowired
    MongoTemplate mongo;

    @Override
    public AutoQuickQuote createQuote(AutoQuickQuote auto, boolean includeCarrierRates) {
        try {
            final LegacyQuoteRequest req = convertRequest(auto);

            final Set<CoverageType> types;
            if (auto.getEstimates() == null || auto.getEstimates().isEmpty()) {
                types = EnumSet.complementOf(EnumSet.of(CoverageType.CUSTOM));
                auto.setEstimates(new ArrayList<>(types.size()));
            } else {
                types = new HashSet<>();
                final Collection<AutoQuoteEstimate> estimates = auto.getEstimates();
                auto.setEstimates(new ArrayList<>(estimates.size()));
                estimates.stream().forEach((type) -> {
                    if (type.getCoverageType() != CoverageType.CUSTOM) {
                        types.add(type.getCoverageType());
                    } else {
                        setCoverages(req, type);
                        final LegacyQuoteResponse resp = quoteService.quote(req);
                        parseResponse(req, resp, auto, type.getCoverageType(), includeCarrierRates);
                    }
                });
            }
            types.forEach((coverageType) -> {
                setCoverages(req, coverageType);
                final LegacyQuoteResponse resp = quoteService.quote(req);
                parseResponse(req, resp, auto, coverageType, includeCarrierRates);
            });
        } catch (RuntimeException exp) {
            auto.setId(null);
            mongo.save(auto);
            LOGGER.error("Exception while calling legacy quote API for QQ with ID: " + auto.getId(), exp);
            auto.setError(exp.getMessage());
            throw exp;
        }

        auto.setId(null);
        mongo.save(auto);
        return auto;
    }

    @Override
    public AutoQuickQuote findQuote(String id) {
        return mongo.findById(id, AutoQuickQuote.class);
    }

    private LegacyQuoteRequest convertRequest(AutoQuickQuote auto) {
        LegacyQuoteRequest request = new LegacyQuoteRequest();

        request.setCountDrivers(auto.getDriverCount());
        // Ignored -- auto.getEmail();
        request.setFirstName(auto.getFirstName());
        request.setLastName(auto.getLastName());
        request.setGender(auto.getGender());
        request.setD1Age(auto.getDriverAge() != null && auto.getDriverAge() > 0 ? auto.getDriverAge() : getAge(auto.getDriverDateOfBirth()));
        request.setD1MaritalStatus(auto.getMaritalStatus());
        request.setState(auto.getState());
        request.setFullZip((auto.getZipCode().length() == 5) ? (auto.getZipCode() + "0000") : auto.getZipCode());
        request.setPhone(auto.getPhone());
        request.setEmail(auto.getEmail());
        request.setD1YearsLicensed(auto.getYearsLicensed());
        request.setHomeOwnership(auto.getHomeOwnership());

        request.setV1CostNew(auto.getVehicleCostNew());
        request.setCountVehicles(auto.getVehicleCount());
        request.setV1Make(VehicleMake.fromValue(auto.getVehicleMake()));
        request.setV1Model(auto.getVehicleModel() != null ? auto.getPriorCarrier().toLowerCase() : null);
        request.setV1Ownership(auto.getVehicleOwnership());
        request.setV1Age(new LocalDate().getYear() - auto.getVehicleYear());

        request.setYearsWithPriorCarrier(auto.getYearsWithPriorCarrier());
        request.setPriorCarrier(auto.getPriorCarrier() != null ? auto.getPriorCarrier().toLowerCase() : null);
        request.setPriorLiabilityLimit(auto.getPriorLiabilityLimit());
        request.setPriorMonthlyPremium(auto.getPriorMonthlyPremium());

        return request;
    }

    private void parseResponse(LegacyQuoteRequest request, LegacyQuoteResponse response, AutoQuickQuote auto, CoverageType coverageType, boolean includeCarriers) {
        List<AutoQuoteEstimate> estimates = auto.getEstimates();
        AutoQuoteEstimate currentEstimate = new AutoQuoteEstimate();
        currentEstimate.setCoverageType(coverageType);
        currentEstimate.setBodilyInjuryLiabilityCoverage(request.getBiCoverage());
        currentEstimate.setCollisionDeductible(request.getV1CollisionDeductible());
        currentEstimate.setComprehensiveDeductible(request.getV1OtherCollisionDeductible());
        currentEstimate.setFullCoverage(request.getV1CollisionDeductible() > 0 && request.getV1OtherCollisionDeductible() > 0);
        currentEstimate.setMedicalPaymentsCoverage(request.getMpCoverage());
        currentEstimate.setPropertyDamageLiabilityCoverage(request.getPdCoverage());
        currentEstimate.setUninsuredMotoristCoverage(request.getUimCoverage());
        BigDecimal lowestLow = null;
        BigDecimal lowestHigh = null;
        final List<CarrierAutoEstimate> carrierEstimates = includeCarriers ? new ArrayList<>(response.getPredictions().size()) : null;
        for (Map.Entry<String, String> entry : response.getPredictions().entrySet()) {
            final String value = entry.getValue();
            final String carrier = entry.getKey();
            String[] amounts = value.split("-");
            if (amounts != null && amounts.length > 1) {
                BigDecimal currentLow = new BigDecimal(amounts[0].replace("$", "").replace(",", ""));
                BigDecimal currentHigh = new BigDecimal(amounts[1].replace("$", "").replace(",", ""));
                if (lowestLow == null || lowestLow.compareTo(currentLow) > 0) {
                    lowestLow = currentLow;
                }

                if (lowestHigh == null || lowestHigh.compareTo(currentHigh) > 0) {
                    lowestHigh = currentHigh;
                }

                if (includeCarriers) {
                    final CarrierAutoEstimate estimate = new CarrierAutoEstimate();
                    estimate.setCarrier(WordUtils.capitalize(carrier));
                    estimate.setMonthlyPaymentRangeLow(currentLow);
                    estimate.setMonthlyPaymentRangeHigh(currentHigh);
                    carrierEstimates.add(estimate);
                }
            }
        }
        currentEstimate.setCarrierRates(carrierEstimates);
        currentEstimate.setMonthlyPaymentRangeLow(lowestLow);
        currentEstimate.setMonthlyPaymentRangeHigh(lowestHigh);
        estimates.add(currentEstimate);
        auto.setEstimates(estimates);
    }

    private void setCoverages(final LegacyQuoteRequest req, final AutoQuoteEstimate estimate) {
        req.setBiCoverage(estimate.getBodilyInjuryLiabilityCoverage() != null ? estimate.getBodilyInjuryLiabilityCoverage() : STATE_MINIMUM);
        req.setPdCoverage(estimate.getPropertyDamageLiabilityCoverage() > 0 ? estimate.getPropertyDamageLiabilityCoverage() : -1);
        req.setUmCoverage(estimate.getUninsuredMotoristCoverage() != null ? estimate.getUninsuredMotoristCoverage() : STATE_MINIMUM);
        req.setUimCoverage(estimate.getUninsuredMotoristCoverage() != null ? estimate.getUninsuredMotoristCoverage() : STATE_MINIMUM);
        if (estimate.isFullCoverage()) {
            if (estimate.getCollisionDeductible() == null || estimate.getCollisionDeductible() == 0) {
                req.setV1CollisionDeductible(500);
            } else {
                req.setV1CollisionDeductible(estimate.getCollisionDeductible());
            }

            if (estimate.getComprehensiveDeductible() == null || estimate.getComprehensiveDeductible() == 0) {
                req.setV1OtherCollisionDeductible(500);
            } else {
                req.setV1OtherCollisionDeductible(estimate.getComprehensiveDeductible());
            }
        } else {
            if (estimate.getCollisionDeductible() != null) {
                req.setV1CollisionDeductible(estimate.getCollisionDeductible());
            } else {
                req.setV1CollisionDeductible(0);
            }
            if (estimate.getComprehensiveDeductible() != null) {
                req.setV1OtherCollisionDeductible(estimate.getComprehensiveDeductible());
            } else {
                req.setV1OtherCollisionDeductible(0);
            }
        }
        req.setMpCoverage(estimate.getMedicalPaymentsCoverage());
    }

    private void setCoverages(final LegacyQuoteRequest req, final CoverageType coverageType) {
        switch (coverageType) {
            case MINIMUM:
                //Do nothing as the defaults are the minimum
                req.setBiCoverage(LegacyQuoteRequest.STATE_MINIMUM);
                req.setPdCoverage(-1);
                req.setUmCoverage(LegacyQuoteRequest.STATE_MINIMUM);
                req.setUimCoverage(LegacyQuoteRequest.STATE_MINIMUM);
                break;
            case RECOMMENDED:
                if (req.getHomeOwnership() == HomeOwnership.HOME_OWNED) {
                    req.setBiCoverage("100/300");
                    req.setPdCoverage(300);
                } else {
                    req.setBiCoverage("50/100");
                    req.setPdCoverage(100);
                }
                break;
            case PREMIUM:
                if (req.getHomeOwnership() == HomeOwnership.HOME_OWNED) {
                    req.setBiCoverage("250/500");
                    req.setPdCoverage(500);
                } else {
                    req.setBiCoverage("50/100");
                    req.setPdCoverage(100);
                }
                req.setMpCoverage("5000");
                break;
        }

        req.setUmCoverage(req.getBiCoverage());
        req.setUimCoverage(req.getBiCoverage());
        setDeductibles(req, coverageType);
    }

    private void setDeductibles(final LegacyQuoteRequest req, final CoverageType coverageType) {
        Integer deductible;
        if (coverageType == CoverageType.PREMIUM || coverageType == CoverageType.RECOMMENDED) {
            deductible = 500;
        } else {
            deductible = 1500;
        }
        if (coverageType == CoverageType.PREMIUM || req.getV1Ownership() == VehicleOwnership.LEASED || req.getV1Ownership() == VehicleOwnership.LIEN || (req.getV1Ownership() == VehicleOwnership.OWNED && (req.getV1CostNew() * (Math.pow(.875, req.getV1Age())) > 6000))) {
            req.setV1CollisionDeductible(deductible);
            req.setV1OtherCollisionDeductible(deductible);
        } else {
            req.setV1CollisionDeductible(0);
            req.setV1OtherCollisionDeductible(0);
        }
    }

    private int getAge(LocalDate dob) {
        LocalDate today = new LocalDate();
        Period period = new Period(dob, today);
        return period.getYears();
}

}
