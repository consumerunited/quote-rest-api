package com.goji.quote.rs.api;

import com.goji.orm.spring.dao.StateProvinceDAO;
import com.goji.quote.rs.dto.AutoQuickQuote;
import com.goji.quote.rs.dto.HomeOwnership;
import com.goji.quote.rs.dto.VehicleMake;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author pcm
 */
@Api("auto")
@Service
@Path("/auto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AutoResource {

    public static final String CARRIERS_REGEX = "21st century|a.central|aaa|aarp|acadia|access general|ace|acuity|adirondack ins exchange|alfa alliance|allied|allstate|america first|american commerce|american family|american national|amica|auto-owners|badger mutual|balboa|beacon national|bristol west|capital insurance group|central mutual of oh|colonial penn|countrywide|dairyland|direct|emc|electric|erie|esurance|explorer|farm bureau|farmers|fidelity|first american|foremost|gmac|geico|general casualty|germantown mutual|grange|grinnell|guide one|hallmark insurance company|hanover|hartford|hastings mutual|ifa|infinity|kemper|liberty mutual|mendota|mercury|metlife|metropolitan|nationwide|no prior insurance|ohio casualty|omni insurance co|other non-standard|other standard|palisades|pekin|pemco|penn national|proformance|progressive|rockford mutual|secura|safeco|shelter insurance|star casualty|state auto|state farm|travelers|usaa|united fire and casualty|unitrin|victoria|west bend|westfield";

    public static final String LIABILITY_LIMITS_REGEX = "100/100|100/300|1000/1000|100csl|12.5/25|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|500csl|none|state minimum";

    public static final String STATE_REGEX = "AL|AK|AZ|AR|CA|CO|CT|DE|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY";

    private final static Map<String, String> CARRIERS = Arrays.stream(CARRIERS_REGEX.split("\\|")).collect(Collectors.toMap(t -> t, t -> WordUtils.capitalize(t)));

    private final static List<String> LIABILITY_LIMITS = Arrays.asList(LIABILITY_LIMITS_REGEX.split("\\|"));

    private final static Collection<Serializable> STATES = Arrays.asList((Serializable[]) STATE_REGEX.split("\\|"));

    @Autowired
    private IAutoService autoService;

    @Autowired
    private StateProvinceDAO stateDAO;

    @POST
    @ApiOperation("Provides auto insurance quotes for a predefined set of coverages (state minimum, recommended & premium) or custom coverages.")
    public AutoQuickQuote createAutoQuickQuote(@Valid AutoQuickQuote auto, @ApiParam("Should detailed rates for each carrier be included?") @QueryParam("carrier_rates") boolean carrierRates, @Context HttpServletRequest request) {
        auto.setIpAddress(getIpAddress(request));
        try {
            return autoService.createQuote(auto, carrierRates);
        } catch (RuntimeException exp) {
            throw new WebApplicationException(exp.getMessage(), 500);
        }
    }

    @GET
    @Path("{id}")
    @ApiOperation("Returns a previously requested auto quote by its 'id'")
    public Response findAutoQuickQuote(@PathParam("id") String id) {
        final AutoQuickQuote auto = autoService.findQuote(id);
        return (auto != null)
                ? Response.ok(auto).build()
                : Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/makes")
    @ApiOperation("Gives the list of car Manufacturers which should be used in the quote request (vehicle_make)")
    public Map<String, String> getMakes() {
        return Arrays.stream(VehicleMake.values()).collect(Collectors.toMap(t -> t.name(), t -> WordUtils.capitalize(t.toJson())));
    }

    @GET
    @Path("/carriers")
    @ApiOperation("Gives a list of Insurance Carriers to be used in the quote request (prior_carrier).")
    public Map<String, String> getCarriers() {
        return Collections.unmodifiableMap(CARRIERS);
    }

    @GET
    @Path("/liability_limits")
    @ApiOperation("Gives a list of liability limits (in thousands) to be used in the quote request (prior_liability_limit).")
    public List<String> getLiabilityLimits() {
        return Collections.unmodifiableList(LIABILITY_LIMITS);
    }

    @GET
    @Path("/states")
    @ApiOperation("Gives the list of the supported US states which should be used in the quote request (state). State code is property name and state name is property value.  State code should be used when calling the /qq/auto API.")
    public Map<String, String> getStates() {
        return stateDAO.getAll(STATES).stream().collect(Collectors.toMap(t -> t.getStateCode(), t -> t.getStateName()));
    }

    @GET
    @Path("/home_ownership")
    @ApiOperation("Gives the list of the supported values for the home ownership status to be used in the quote request (home_ownership)")
    public Map<String, String> getHomeOwnership() {
        return Arrays.stream(HomeOwnership.values()).collect(Collectors.toMap(t -> t.name(), t -> WordUtils.capitalize(t.toJson())));
    }

    public String getIpAddress(HttpServletRequest request) {
        final String forwardedFor = request.getHeader("X-Forwarded-For");
        if (forwardedFor != null) {
            return forwardedFor;
        } else {
            return request.getRemoteAddr();
        }
    }
    }
