/*
 * Copyright (C) Consumer United LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.goji.quote.rest.api.mongo;

import com.goji.quote.rs.dto.AutoQuickQuote;
import java.math.BigInteger;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public interface AutoQuickQuoteRepository extends CrudRepository<AutoQuickQuote, BigInteger> {
    
}
