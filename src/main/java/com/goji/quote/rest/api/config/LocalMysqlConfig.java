package com.goji.quote.rest.api.config;

import com.goji.orm.connections.ConnectionInfo;
import com.goji.orm.spring.config.LiveRepositoryConfig;
import javax.sql.DataSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

/**
 *
 * @author Yinzara
 */
@Configuration
@Profile("localmysql")
public class LocalMysqlConfig extends LiveRepositoryConfig {

    @Override
    protected ConnectionInfo callsConnectionInfo() {
        return internalConnectionInfo();
    }
    
    @Override
    protected ConnectionInfo analyticsConnectionInfo() {
        return internalConnectionInfo();
    }
    
    @Override
    public DataSource gojiDataSource() {
        return dataSource("goji", internalConnectionInfo());
    }

    @Override
    protected ConnectionInfo internalConnectionInfo() {
       final String user = System.getProperty("mysql.user") != null?System.getProperty("mysql.user"):System.getenv("mysql.user");
       final String password = System.getProperty("mysql.password") != null?System.getProperty("mysql.password"):System.getenv("mysql.password");
       final String database = System.getProperty("mysql.database") != null?System.getProperty("mysql.database"):System.getenv("mysql.database");
       final String url = System.getProperty("mysql.url") != null?System.getProperty("mysql.url"):System.getenv("mysql.url");
       
       return new ConnectionInfo(
                url != null?url:("jdbc:mysql://localhost/" + (database != null?database:"goji")),
                user != null?user:"root",
                password != null?password:"password",
                "",
                ""
        );
    }
    
    @Override
    protected LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        final LocalSessionFactoryBean bean = super.sessionFactory(dataSource);
        bean.getHibernateProperties().setProperty("hibernate.hbm2ddl.auto", "update");
        bean.getHibernateProperties().setProperty("hibernate.show_sql", "true");
        return bean;
    }
    
    
    
}
