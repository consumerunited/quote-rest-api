package com.goji.quote.rest.api.service;

import com.goji.quote.utils.PithonInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

/**
 * def score_lead(data,carrierlist,conversion_weight,commission_weight):
 * alqme= ls.score_lead(payload,AllCarriers, 1,9,0)
 * 
 * @param data
 * @param carrierList
 * @param conversion_weight
 * @param commission_weight
 */
@Service
public class LeadScoringService implements PithonInterface {

    private GetValueFromX<Double> getNumericValFromX;
    private GetValueFromX<String> getStringValFromX;

    @PostConstruct
    private void init() {
        setGetNumericValFromX((name, defaultValue, x) -> x.get(name) == null ? defaultValue
                : x.get(name) instanceof Double ? (Double) x.get(name)
                : x.get(name) instanceof Integer ? (double) (int) (Integer) x.get(name)
                : x.get(name) instanceof Float ? (double) (float) (Float) x.get(name) : null);
        setGetStringValFromX((name, defaultValue, x) -> x.get(name) == null ? defaultValue
                : x.get(name) instanceof String ? (String) x.get(name) : defaultValue);
    }

    /**
     * @return the getNumericValFromX
     */
    public GetValueFromX<Double> getGetNumericValFromX() {
        return getNumericValFromX;
    }

    /**
     * @param getNumericValFromX the getNumericValFromX to set
     */
    public void setGetNumericValFromX(GetValueFromX<Double> getNumericValFromX) {
        this.getNumericValFromX = getNumericValFromX;
    }

    /**
     * @return the getStringValFromX
     */
    public GetValueFromX<String> getGetStringValFromX() {
        return getStringValFromX;
    }

    /**
     * @param getStringValFromX the getStringValFromX to set
     */
    public void setGetStringValFromX(GetValueFromX<String> getStringValFromX) {
        this.getStringValFromX = getStringValFromX;
    }

    /**
     * Jezz, there is just no way to optimize it.
     *
     * @param inputMap
     * @param doubleName
     * @param threshold
     * @param nullval
     * @param large
     * @param small
     * @return
     */
    private double DD(Map<String, Object> inputMap, String doubleName, double threshold, double nullval, double large, double small) {
        if (getGetNumericValFromX().get(doubleName, null, inputMap) == null) {
            return nullval;
        } else if (getGetNumericValFromX().get(doubleName, null, inputMap) > threshold) {
            return large;
        } else {
            return small;
        }
    }

    private double SS(Map<String, Object> inputMap, String str, StringPredicator sp, double nullval, double isTrue, double isFalse) {
        if (getGetStringValFromX().get(str, null, inputMap) == null) {
            return nullval;
        } else if (sp.validate(getGetStringValFromX().get(str, null, inputMap))) {
            return isTrue;
        } else {
            return isFalse;
        }
    }

    /**
     * original python doesn't have a default return value.
     *
     * @param data
     * @return
     */
    public int classifyConversion(Double data) {
        return classify(data, "conv_thresh.csv");
    }

    public int classifyCommission(Double data) {
        return classify(data, "comm_thresh.csv");
    }

    public int classify(Double data, String file) {
        String splittedBy = ",";
        int index_column = 0;

        /**
         * Seems the pandas.Series().from_csv('file', index_col = col) only
         * picks the first column after the index column, i.e. 0 {1~10}
         */
        CSVMapper<Integer, Double> mapper = new CSVMapper<Integer, Double>() {
            @Override
            public Map<Integer, Double> map(List<String[]> data, int keyColumn) {
                Map<Integer, Double> res = new HashMap<>();
                for (String[] s : data) {
                    int col = Integer.parseInt(s[keyColumn]);
                    res.put(col, Double.parseDouble(s[keyColumn + 1]));
                }
                return res;
            }
        };

        Map<Integer, Double> thresh = mapper.map(readCSV(file, splittedBy), index_column);
        thresh.entrySet().forEach(e -> System.out.println(e.getKey() + " : " + e.getValue()));

        if (data == null || data < 0) {
            return 1;
        } else if (data < thresh.get(1)) {
            return 1;
        } else if (data < thresh.get(2)) {
            return 2;
        } else if (data < thresh.get(3)) {
            return 3;
        } else if (data < thresh.get(4)) {
            return 4;
        } else if (data < thresh.get(5)) {
            return 5;
        } else if (data < thresh.get(6)) {
            return 6;
        } else if (data < thresh.get(7)) {
            return 7;
        } else if (data < thresh.get(8)) {
            return 8;
        } else if (data < thresh.get(9)) {
            return 9;
        } else if (data < 1000) {
            return 10;
        }
        return -1;
    }

    public double getCommission(Map<String, Object> data) {
        String file = "commission";
        String splittedBy = ",";
        
        CSVLister<Commission> lister = new CSVLister<Commission>() {
            @Override
            public List<Commission> list(List<String[]> data) {
                List<Commission> res = new ArrayList<>();
                data.stream().forEachOrdered(s -> {
                    Commission comm = new Commission();
                    comm.setCarrier(s[0]);
                    comm.setCode(s[1]);
                    comm.setCommRate(Double.parseDouble(s[2]));
                    comm.setFixedComm(Double.parseDouble(s[3]));
                    comm.setId(Double.parseDouble(s[4]));
                    comm.setProduct(s[5]);
                    comm.setSecRate(Double.parseDouble(s[6]));
                    comm.setTerm(Integer.parseInt(s[7]));
                    comm.setTier(s[8]);
                });
                return res;
            }
        };
        
        List<Commission> comm = lister.list(readCSV(file, splittedBy));
        String carrier = getGetStringValFromX().get("carrier", null, data);
        String prediction = getGetStringValFromX().get("prediction", null, data);
        int vehicleCounts = (int)(double)getGetNumericValFromX().get("count_vehicles", null, data);
        String state = getGetStringValFromX().get("state", null, data);
        Set<String> dairyLandStates = new HashSet<>();
        dairyLandStates.add("AZ");
        dairyLandStates.add("CO");
        dairyLandStates.add("NV");
        dairyLandStates.add("UT");
        dairyLandStates.add("WA");
        dairyLandStates.add("WI");
        
        if(carrier == null){
            return 0;
        }
        List<Commission> filterredByCarrier = comm.stream()
                .filter(c->c.getCarrier()!=null && c.getCarrier().equalsIgnoreCase(carrier))
                .collect(Collectors.toList());
        List<Commission> filterredByCode = filterredByCarrier;
        switch(carrier.toLowerCase()){
            case "safeco":
                filterredByCode = filterredByCarrier.stream()
                        .filter(c->("NS".equals(c.getCode())&&"NS".equals(prediction)) ||
                                (!"NS".equals(c.getCode())&&!"NS".equals(prediction)))
                        .collect(Collectors.toList());
                break;
            case "progressive":
                filterredByCode = filterredByCarrier.stream()
                        .filter(c->("NS".equals(c.getCode())&&"NS".equals(prediction)) ||
                                (!"NS".equals(c.getCode())&&!"NS".equals(prediction)))
                        .collect(Collectors.toList());
                break;
            case "esurance":
                filterredByCode = filterredByCarrier.stream()
                        .filter(c->("NS".equals(c.getCode())&&"NS".equals(prediction)) ||
                                (!"NS".equals(c.getCode())&&!"NS".equals(prediction)))
                        .filter(c->(vehicleCounts>=2 && "2".equals(c.getTier())) ||
                                ("1".equals(c.getTier()) && vehicleCounts<2))
                        .collect(Collectors.toList());
                break;
            case "dairyland":
                filterredByCode = filterredByCarrier.stream()
                        .filter(c->(dairyLandStates.contains(state)&&c.getCode().equalsIgnoreCase(state)) ||
                                ("Auto".equals(c.getCode()) && !dairyLandStates.contains(state)))
                        .collect(Collectors.toList());
                break;
            default:
                break;
        }

        //try catch block in original python code, so it could happends that there are multiple Commissions left
        //in that case, return 0
        if(filterredByCode!=null && filterredByCode.size()==1){
            Commission c= filterredByCode.get(0);
            double p = getGetNumericValFromX().get("premium", null, data);
            double f = c.getFixedComm();
            double cr = c.getCommRate();
            double sr = c.getSecRate();
            return f+(cr+sr)*p;
        } else {
            return 0.0;
        }
    }
    
    public double expectedCommission(Map<String, Object> data, List<String> carrierList){
        double commission = 0;
        
        
        
        return 0;
    }
    
    public double compRank(Map<String, Object> data, List<String> carrierList){
        
        return 0;
    }
    
    /*


#returns total estimated commission for the lead (commission * probability of closing)
def expected_commission(data, carrierlist):
    commission=0
    ranks = comp_rank(data,carrierlist)
    #UPDATE UPDATE UPDATE UPDATE UPDATE
    """For real, update this to reference pkl on server"""
    comp = pd.read_csv(path+'comp_rates.csv')
    for x in carrierlist:
        rank = ranks[ranks['carrier']==x]
        rank = rank['rank'].values
        a=[]
        #competitiveness locked at 5 carriers - probability beyond that is minimal and adds complication
        if (rank>5) or (not rank):
            pass
        else:
            share = comp.comp[comp['rank']==rank[0]].values
            df = dict(carrier=x,premium=data[x], prediction=data['prediction'],count_vehicles=data['count_vehicles'],state=data['state'])
            commission += float(get_commission(df)*share)
    return commission

#returns rank of given carrier in relation to others
def comp_rank(data, carrierlist):
    df=pd.Series(data)
    df=df[carrierlist]
    df=pd.DataFrame(df)
    df.columns = ['premium']
    df = df.dropna()
    df = df[df['premium']!=0]
    df['premium']=df.premium.apply(lambda x: x+np.random.rand()/100)
    df = df.rank(axis=0,method='max')
    df = df.reset_index()
    df.columns = ['carrier','rank']
    return df

#Updates commission table, pulling from SQL table on analytics.commission_data
def update_commission_table():
    analytics = db.connect('internal-db-rr0.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com','readonly','1227boston','analytics')

    comm_query = """
    SELECT * FROM analytics.commission_data
    WHERE product = 'Auto'
    AND term=6
    """

    commission_data=pd.read_sql(comm_query,analytics)
    analytics.close()
    print "Data read in and DB closed...."

    #simplify data
    commission_data['code']=commission_data.code.replace('NSTD','NS')

    #progressive preferred pricing
    prog_pr = commission_data[commission_data['carrier']=='Progressive']
    prog_pr = prog_pr[prog_pr['code'].isin(['PR','UL','ST'])]
    prog_pr = prog_pr.groupby('carrier').mean().reset_index()
    prog_pr['code']='PR'

    #progressive nonstandard pricing
    prog_ns = commission_data[commission_data['carrier']=='Progressive']
    prog_ns = prog_ns[prog_ns['code'].isin(['NS','MM'])]
    prog_ns = prog_ns.groupby('carrier').mean().reset_index()
    prog_ns['code']='NS'

    #safeco nonstandard pricing
    safe_ns = commission_data[commission_data['carrier']=='Safeco']
    safe_ns = safe_ns[safe_ns['code'].isin(['NS'])]
    safe_ns = safe_ns.groupby('carrier').mean().reset_index()
    safe_ns['code']='NS'

    #safeco preferred pricing
    safe_pr = commission_data[commission_data['carrier']=='Safeco']
    safe_pr = safe_pr[safe_pr['code'].isin(['PRF'])]
    safe_pr = safe_pr.groupby('carrier').mean().reset_index()
    safe_pr['code']='PR'

    #filtering esurance
    code_map ={
    'Multi-CarPrior': 'PR',
    'Multi-CarwithoutPrior':'NS',
    'Single-CarPrior':'PR',
    'Single-CarwithoutPrior':'NS',
    'Multi-Car Prior Insurance':2,
    'Single-Car Prior Insurance':1,
    'Multi-Car without Prior Insurance':2,
    'Single-Car without Prior Insurance':1
    }
    esur = commission_data[commission_data['carrier']=='Esurance']
    esur['code']=esur.code.map(code_map)
    esur['tier']=esur.tier.map(code_map)


    #remove prog/safe and rejoin
    commission_data = commission_data[-commission_data['carrier'].isin(['Progressive','Safeco','Esurance'])]
    commission_data = commission_data.append(prog_pr).append(prog_ns).append(safe_pr).append(safe_ns).append(esur)
    commission_data['fixed_comm']=commission_data['fixed_comm']/commission_data['term']

    #UPDATE UPDATE UPDATE UPDATE UPDATE
    """For real, update this to reference csv on server"""
    commission_data.to_csv(path+'commission.csv',index=False)
    return "Update successful."

#updates station 1-5 probabilities on a 90 day trailing basis
def update_competitiveness():
    #Queries
    ezlynx = """
    SELECT ls.lead_id, ls.carrier_sold, ezlynx_applicant_id, ez.created_date, CONCAT(CAST(ez.applicant_id as CHAR),ez.carrier_name) as delete_id, ez.carrier_name, ez.premium_amount
    FROM leads_stats ls
    LEFT JOIN leads l on l.lead_id=ls.lead_id
    LEFT JOIN quote_execution_details ez on ez.applicant_id=l.ezlynx_applicant_id
    WHERE date_added >= DATE_SUB(CURRENT_DATE, INTERVAL 90 DAY)
    AND ls.future_pay_date is not null
    AND ezlynx_applicant_id is not null
    LIMIT 10000000
    """

    #set up databases
    leads_database = db.connect('internal-db-rr0.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com','readonly','1227boston','consumer_united')

    #pull in queries
    ez_data = pd.read_sql(ezlynx, leads_database)
    print "EZ Data data read in..."
    leads_database.close()
    print "DB closed..."

    #remap carriers
    carrier_map={'21st Century Insurance':'21ST', 
    'Dairyland Insurance': 'DAIRYLAND', 
    'Esurance Auto': 'ESURANCE',
    'Foremost Insurance Group':'FOREMOST' ,
    'Hartford': 'HARTFORD',
    'Infinity Insurance Company': 'INFINITY' ,
    'Mercury Auto Insurance': 'MERCURY',
    'MetLife Auto & Home\xc3\x82\xc2\xae': 'METLIFE',
    'MetLife Auto & Home\xc2\xae': 'METLIFE',
    'National General': 'NATIONAL GENERAL', 
    'Progressive Insurance':'PROGRESSIVE',
    'Safeco Insurance': 'SAFECO', 
    'The General': 'THE GENERAL',
    'Travelers': 'TRAVELERS',
    'Plymouth Rock Assurance New Jersey':'PLYMOUTH ROCK'}



    # sort and dedupe
    cd = ez_data.sort('created_date', ascending=False)
    print cd.shape
    cd = cd.drop_duplicates('delete_id')
    print cd.shape

    #apply map
    cd['carrier_name']=cd.carrier_name.map(carrier_map)

    #pivot and rank
    dd = cd.pivot_table(values='premium_amount',index=['lead_id','carrier_sold'], columns='carrier_name')
    dd = dd.rank(axis=1,method='max')
    dd = dd.reset_index()

    #choose winning quote
    def get_sold_quote(x):
        try:
            carrier_sold=x['carrier_sold']
            toreturn=x[carrier_sold]
        except:
            return 0
        return toreturn
    dd['winner']=dd.apply(get_sold_quote, axis=1)
    final = dd[['carrier_sold','winner']]
    final.dropna(inplace=True)
    winner =  final[final['winner']!=0]
    winner = winner[winner['winner']<=5]
    total = winner.winner.count()
    winner = winner.groupby('winner').count()
    winner = winner.reset_index(drop=False)
    winner['carrier_sold']=winner['carrier_sold']/total
    winner.columns=['rank','comp']
    
    #UPDATE UPDATE UPDATE UPDATE UPDATE
    """For real, update this to reference csv on server"""
    winner.to_csv(path+'comp_rates.csv',index=False)

    return "Update successful."

def score_lead(data,carrierlist,conversion_weight,commission_weight):
    try:
        data['prediction']=tier_prediction(data)
        data['expected_commission']=expected_commission(data,carrierlist)
        data['commission_class']=classify_commission(data.get('expected_commission'))
        data['expected_conversion']=conversion_prediction(data)
        data['conversion_class']=classify_conversion(conversion_prediction(data))
        data['alqme_index']=((data['conversion_class']*10*conversion_weight)+(data['commission_class']*10*commission_weight))/(commission_weight+conversion_weight)
        alqme_results= {'prediction':data['prediction'],
                        'expected_commission': data['expected_commission'],
                        'commission_class': data['commission_class'],
                        'expected_conversion': data['expected_conversion'],
                        'conversion_class':data['conversion_class'],
                        'alqme_index': data['alqme_index']}       

        return alqme_results
    except ValueError:
        data['prediction']='NS'
        data['expected_commission']=10
        data['expected_conversion']=.08
        data['commission_class']=5
        data['conversion_class']=2
        data['alqme_index']=((data['conversion_class']*10*conversion_weight)+(data['commission_class']*10*commission_weight))/(commission_weight+conversion_weight)
        alqme_results= {'prediction':data['prediction'],
                        'expected_commission': data['expected_commission'],
                        'commission_class': data['commission_class'],
                        'expected_conversion': data['expected_conversion'],
                        'conversion_class':data['conversion_class'],
                        'alqme_index': data['alqme_index']}       
        return alqme_results
#bin function
def percentile_thresholds(data,bins):
    step = 10.0/bins
    if bins*step>100:
        return "Percentiles cannot be over 100"
    else:
        thresholds=pd.Series()
        i=1
        while i<=bins:
            thresholds = thresholds.append(pd.Series(np.percentile(data,i*10*step),index=[i]))
            i+=1
        return thresholds

    
    


     */
    
    
    
    public double conversionPrediction(Map<String, Object> data) {
        if (data == null) {
            data = new HashMap<>();
        }

        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) == null) {
            return 0.15298;
        }
        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) > 2) {
            if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                return 0.21774;
            }
            if (getGetNumericValFromX().get("annualized_premium", null, data) > 2055.58639) {
                if (getGetNumericValFromX().get("annualized_premium", null, data) > 3763.40348) {
//                    if (getGetNumericValFromX().get("zip", null, data) == null) {return 0.08946;} else if (getGetNumericValFromX().get("zip", null, data) > 12102) {return 0.06294;}if (getGetNumericValFromX().get("zip", null, data) <= 12102) {return 0.13079;}
                    return DD(data, "zip", 12102, 0.08946, 0.06294, 0.13079);
                }
                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 3763.40348) {
                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                        return 0.16858;
                    }
                    if (getGetNumericValFromX().get("zip", null, data) > 12904) {
                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                            return 0.15479;
                        }
                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 5) {
                            return 0.05521;
                        }
                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 5) {
                            if (getGetNumericValFromX().get("count_trucks", null, data) == null) {
                                return 0.15534;
                            }
                            if (getGetNumericValFromX().get("count_trucks", null, data) > 2) {
                                return 1;
                            }
                            if (getGetNumericValFromX().get("count_trucks", null, data) <= 2) {
                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 2567.16656) {
                                    if (getGetNumericValFromX().get("count_drivers", null, data) == null) {
                                        return 0.12615;
                                    }
                                    if (getGetNumericValFromX().get("count_drivers", null, data) > 1) {
                                        return DD(data, "driver_1_credit_mixed", 62, 0.21053, 1,
                                                DD(data, "driver_1_credit_mixed", 2, 0.19847, 0.17742,
                                                        getGetNumericValFromX().get("annualized_premium", null, data) > 3107.47333 ? 0 : 1));
                                    }
                                    if (getGetNumericValFromX().get("count_drivers", null, data) <= 1) {
                                        return 0.11096;
                                    }
                                }
                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 2567.16656) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                        return 0.17729;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 9) {
                                        return 0.21381;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 9) {
                                        return 0.13452;
                                    }
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("zip", null, data) <= 12904) {
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                            return 0.21253;
                        }
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Other") {
                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                return 0.29299;
                            }
                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 5) {
                                if (getGetNumericValFromX().get("zip", null, data) > 12280) {
                                    return 1;
                                }
                                if (getGetNumericValFromX().get("zip", null, data) <= 12280) {
                                    return 0.10256;
                                }
                            }
                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 5) {
                                return 0.3875;
                            }
                        }
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Other") {
                            return 0.17032;
                        }
                    }
                }
            }
            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 2055.58639) {
                if (getGetNumericValFromX().get("annualized_premium", null, data) > 801.14721) {
                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                        return 0.23016;
                    }
                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 8) {
                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                            return 0.23889;
                        }
                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 7) {
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                return 0.11211;
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 4) {
                                return 0.09439;
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 4) {
                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                    return 0.25;
                                }
                                if (getGetNumericValFromX().get("zip", null, data) > 74161) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                        return 0.43478;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 11250) {
                                        if (getGetNumericValFromX().get("zip", null, data) > 77444) {
                                            return 0;
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) <= 77444) {
                                            return 1;
                                        }
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 11250) {
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 1869.11429) {
                                            return 1;
                                        }
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1869.11429) {
                                            return 0.15385;
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("zip", null, data) <= 74161) {
                                    return 0.04762;
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 7) {
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                return 0.2477;
                            }
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 25) {
                                return 0.17898;
                            }
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 25) {
                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 6) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 1531.01568) {
                                        return 0.23077;
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1531.01568) {
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                            return 0.36803;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 2) {
                                            if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                return 0.35547;
                                            }
                                            if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 5) {
                                                return 0.77778;
                                            }
                                            if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 5) {
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 7) {
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                        return 0.35744;
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 45) {
                                                        return 0;
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 45) {
                                                        return 0.35325;
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 7) {
                                                    return 0;
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 2) {
                                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 16) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1048.99952) {
                                                    return 0;
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1048.99952) {
                                                    return 1;
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 16) {
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 1063.41167) {
                                                        return 1;
                                                    }
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1063.41167) {
                                                        return 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 6) {
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                            return 0.20726;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 11500) {
                                            return 0.08497;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 11500) {
                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return 0.25941;
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 18226) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1703.14108) {
                                                    return 0.08333;
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1703.14108) {
                                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                                        return 0.26667;
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                                        return 0.2375;
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 1) {
                                                            if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                                                return 0.69231;
                                                            }
                                                            if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1171.235) {
                                                                    return 0;
                                                                }
                                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1171.235) {
                                                                    return 1;
                                                                }
                                                            }
                                                            if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                                                return 1;
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 1) {
                                                            return 0;
                                                        }
                                                    }
                                                }
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 18226) {
                                                if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                                    return 0.47826;
                                                }
                                                if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                                    return 0.1;
                                                }
                                                if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 21) {
                                                        return 0;
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 21) {
                                                        return 0.90909;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 8) {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 917.98695) {
                                return 0.13678;
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 917.98695) {
                                if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == null) {
                                    return 0.2446;
                                }
                                if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == "SUV") {
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                        return 0.39394;
                                    }
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                                            return 0.72727;
                                        }
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 58) {
                                            return 0;
                                        }
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 58) {
                                            return 1;
                                        }
                                    }
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                        return 0.22727;
                                    }
                                }
                                if (getGetStringValFromX().get("vehicle_1_model_type", null, data) != "SUV") {
                                    return 0.18182;
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 801.14721) {
                        if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                            return 0.34352;
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 40) {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 574.69699) {
                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                    return 0.2522;
                                }
                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 21) {
                                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                                        return 0.19549;
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) > 10386) {
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                            return 0.18939;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 6) {
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                return 0.2094;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 4000) {
                                                return 0.19824;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 4000) {
                                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 12) {
                                                    return 1;
                                                }
                                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 12) {
                                                    return 0;
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 6) {
                                            return 0;
                                        }
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) <= 10386) {
                                        return 1;
                                    }
                                }
                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 21) {
                                    return 0.33714;
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 574.69699) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                    return 0.41714;
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 26) {
                                    return 1;
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 26) {
                                    if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == null) {
                                        return 0.40123;
                                    }
                                    if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == "Truck") {
                                        if (getGetNumericValFromX().get("zip", null, data) == null) {
                                            return 0.59259;
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) > 31866) {
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) > 52) {
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                    return 0.69231;
                                                }
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 3) {
                                                    return 0;
                                                }
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 3) {
                                                    return 0.81818;
                                                }
                                            }
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 52) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 558.6425) {
                                                    return 1;
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 558.6425) {
                                                    return 0;
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) <= 31866) {
                                            return 1;
                                        }
                                    }
                                    if (getGetStringValFromX().get("vehicle_1_model_type", null, data) != "Truck") {
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                                            return 0.368;
                                        }
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 4) {
                                            return 0.22857;
                                        }
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 4) {
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                return 0.48438;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 14) {
                                                return 0;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 14) {
                                                return 0.59524;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 40) {
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                return 0.39972;
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 13) {
                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                    return 0.21277;
                                }
                                if (getGetNumericValFromX().get("zip", null, data) > 96962) {
                                    return 1;
                                }
                                if (getGetNumericValFromX().get("zip", null, data) <= 96962) {
                                    return 0.18681;
                                }
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 13) {
                                return 0.44106;
                            }
                        }
                    }
                }
            }
            if (getGetNumericValFromX().get("no_prior_ins_index", null, data) <= 2) {
                if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                    return 0.13777;
                }
                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1597.38101) {
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                        return 0.11332;
                    }
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 6) {
                        if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                            return 0.06403;
                        }
                        if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                            return 0.05217;
                        }
                        if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 3098.25849) {
                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                    return 0.04266;
                                }
                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 37) {
                                    if (getGetNumericValFromX().get("count_trucks", null, data) == null) {
                                        return 0.4;
                                    }
                                    if (getGetNumericValFromX().get("count_trucks", null, data) > 0) {
                                        return 1;
                                    }
                                    if (getGetNumericValFromX().get("count_trucks", null, data) <= 0) {
                                        return 0;
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 37) {
                                    return 0.03868;
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 3098.25849) {
                                if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                    return 0.09598;
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                    return 0.12922;
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                    return 0.07935;
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 6) {
                        if (getGetNumericValFromX().get("count_drivers", null, data) == null) {
                            return 0.12339;
                        }
                        if (getGetNumericValFromX().get("count_drivers", null, data) > 1) {
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                return 0.15714;
                            }
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 2) {
                                return 0.12032;
                            }
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 2) {
                                if (getGetNumericValFromX().get("count_cars", null, data) == null) {
                                    return 0.19648;
                                }
                                if (getGetNumericValFromX().get("count_cars", null, data) > 0) {
                                    return 0.17008;
                                }
                                if (getGetNumericValFromX().get("count_cars", null, data) <= 0) {
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                        return 0.26599;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 9) {
                                        return 0.05556;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 9) {
                                        return 0.34677;
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("count_drivers", null, data) <= 1) {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 2250.71777) {
                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                    return 0.09713;
                                }
                                if (getGetNumericValFromX().get("zip", null, data) > 15448) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 3857.67551) {
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == null) {
                                            return 0.05222;
                                        }
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == "Truck") {
                                            return 0.10811;
                                        }
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) != "Truck") {
                                            return 0.04875;
                                        }
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 3857.67551) {
                                        if (getGetNumericValFromX().get("count_cars", null, data) == null) {
                                            return 0.09002;
                                        }
                                        if (getGetNumericValFromX().get("count_cars", null, data) > 1) {
                                            return 0.15123;
                                        }
                                        if (getGetNumericValFromX().get("count_cars", null, data) <= 1) {
                                            return 0.08475;
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("zip", null, data) <= 15448) {
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                        return 0.12299;
                                    }
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                        return 0.1013;
                                    }
                                    if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                            return 0.15;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 8) {
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                                return 0.15971;
                                            }
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 7) {
                                                return 1;
                                            }
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 7) {
                                                return 0.15707;
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 8) {
                                            return 0.07805;
                                        }
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 2250.71777) {
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                    return 0.1345;
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                        return 0.11853;
                                    }
                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 8) {
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                            return 0.11287;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 10) {
                                            return 0.08581;
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 10) {
                                            return 0.12296;
                                        }
                                    }
                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 8) {
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == null) {
                                            return 0.36364;
                                        }
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == "SUV") {
                                            return 1;
                                        }
                                        if (getGetStringValFromX().get("vehicle_1_model_type", null, data) != "SUV") {
                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return 0.1875;
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 78049) {
                                                return 1;
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 78049) {
                                                return 0.07143;
                                            }
                                        }
                                    }
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                        return 0.15309;
                                    }
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 5) {
                                        return 0.27273;
                                    }
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 5) {
                                        if (getGetNumericValFromX().get("zip", null, data) == null) {
                                            return 0.1483;
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) > 9813) {
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                return 0.1429;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 2) {
                                                return 0.14193;
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 2) {
                                                return 0.36;
                                            }
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) <= 9813) {
                                            return 0.1983;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1597.38101) {
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                        return 0.16446;
                    }
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 6) {
                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 1330.08827) {
                            return 0.07468;
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1330.08827) {
                            if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                                return 0.11401;
                            }
                            if (getGetNumericValFromX().get("driver_1_age", null, data) > 23) {
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                    return 0.11362;
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                    return 0.0984;
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                                        return 0.13634;
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) > 36116) {
                                        return 0.15465;
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) <= 36116) {
                                        return 0.10372;
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 23) {
                                return 1;
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 6) {
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                            return 0.174;
                        }
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Other") {
                            return 0.29536;
                        }
                        if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Other") {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 1031.5221) {
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == null) {
                                    return 0.15682;
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) == "Male") {
                                    return 0.14029;
                                }
                                if (getGetStringValFromX().get("driver_1_gender", null, data) != "Male") {
                                    return 0.17503;
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1031.5221) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                    return 0.20866;
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 13) {
                                    return 0.28148;
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 13) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                        return 0.18881;
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 4500) {
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                                            return 0.18496;
                                        }
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 22) {
                                            if (getGetNumericValFromX().get("count_vans", null, data) == null) {
                                                return 0.18373;
                                            }
                                            if (getGetNumericValFromX().get("count_vans", null, data) > 1) {
                                                return 1;
                                            }
                                            if (getGetNumericValFromX().get("count_vans", null, data) <= 1) {
                                                if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 1) {
                                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                                        return 0.1753;
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 5) {
                                                        return 0.10849;
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 5) {
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) == null) {
                                                            return 0.18888;
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) > 8) {
                                                            return 0.10078;
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) <= 8) {
                                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 11625) {
                                                                return 0.18182;
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 11625) {
                                                                return 0.27551;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 1) {
                                                    return 0.30986;
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 22) {
                                            return 1;
                                        }
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 4500) {
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                            return 0.66667;
                                        }
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Married") {
                                            return 0;
                                        }
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Married") {
                                            return 0.85714;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return -1.0;
    }

    public String tierPrediction(Map<String, Object> data) {
        if (data == null) {
            data = new HashMap<>();
        }
        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) == null) {
            return "NS";
        }
        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) > 2) {
            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                return "NS";
            }
            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 8) {
                if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                    return "NS";
                }
                if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                        return "NS";
                    }
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 40) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 40) {
                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 13875) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 13875) {
                                return "NS";
                            }
                        }
                    }
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 13125) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 13125) {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 951.95229) {
                                if (getGetNumericValFromX().get("count_trucks", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("count_trucks", null, data) > 2) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("count_trucks", null, data) <= 2) {
                                    return "NS";
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 951.95229) {
                                return "NS";
                            }
                        }
                    }
                }
                if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                    if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                        return "NS";
                    }
                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 358.38599) {
                        if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 41) {
                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                return "NS";
                            }
                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {

                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 50) {
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 2709.54) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 2709.54) {
                                            if (getGetNumericValFromX().get("driver_1_years_in_occupation", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_years_in_occupation", null, data) > 1) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_years_in_occupation", null, data) <= 1) {
                                                return "PR";
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 50) {
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 19) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 19) {
                                            return "NS";
                                        }
                                    }
                                }
                            }
                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 4) {
                                    if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) > 5) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) <= 5) {
                                        return "NS";
                                    }
                                }
                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 4) {
                                    return "NS";
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 41) {
                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 12) {
                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1935.50896) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1935.50896) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 9) {
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 17) {
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 20500) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 20500) {
                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 19500) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 19500) {
                                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 12750) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 12750) {
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) > 7) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) <= 7) {
                                                            return "NS";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 17) {
                                            return "PR";
                                        }
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 9) {
                                        return "NS";
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 12) {
                                if (getGetNumericValFromX().get("count_cars", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("count_cars", null, data) > 0) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 1159.48705) {
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 50000) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 50000) {
                                            if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Divorced") {

                                                return "PR";
                                            }
                                            if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Divorced") {

                                                if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 9) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 9) {
                                                    return "NS";
                                                }
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1159.48705) {
                                        if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                            if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) > 5) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) <= 5) {
                                                return "NS";
                                            }
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("count_cars", null, data) <= 0) {
                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                                        return "NS";
                                    }
                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {

                                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                            return "NS";
                                        } else if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 9) {
                                            return "NS";
                                        } else {
                                            return "NS";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 358.38599) {
                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 260.03016) {
                            return "NS";
                        } else {
                            return "NS";
                        }
                    }
                }
            }
            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 8) {
                if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                    return "NS";
                }
                if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {

                    if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                        return "NS";
                    }
                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 49) {
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 14500) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 14500) {
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) > 2) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) <= 2) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 483.08708) {
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 3631.9) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 3631.9) {
                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 13338) {
                                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 5) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 5) {
                                                    return "NS";
                                                }
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 13338) {
                                                return "PR";
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 483.08708) {
                                        return "NS";
                                    }
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 49) {
                        if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 563.74839) {
                            if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 11) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 11) {
                                        return "NS";
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 1841.39) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1841.39) {
                                        return "PR";
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 563.74839) {
                            return "NS";
                        }
                    }
                }
                if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                    if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                        return "NS";
                    }
                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 55) {
                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 9) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 9) {
                            return "PR";
                        }
                    }
                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 55) {
                        if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                            return "NS";
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 787.36355) {
                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 1879.44194) {
                                if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) > 12883) {
                                        if (getGetNumericValFromX().get("zip", null, data) > 62025) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) <= 62025) {
                                            return "NS";
                                        }
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) <= 12883) {
                                        return "NS";
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1879.44194) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 4) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 4) {
                                        if (getGetNumericValFromX().get("driver_1_count_accidents", null, data) == null) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("driver_1_count_accidents", null, data) > 0) {
                                            return "NS";
                                        }
                                        if (getGetNumericValFromX().get("driver_1_count_accidents", null, data) <= 0) {
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) > 38) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 38) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1838.18381) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1838.18381) {
                                                    if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 6) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 6) {
                                                            return "NS";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 37750) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 37750) {
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 14375) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 14375) {
                                            if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) > 3) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("medical_payments_coverage_mixed", null, data) <= 3) {
                                                return "NS";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 787.36355) {
                            return "NS";
                        }
                    }
                }
            }
        }
        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) <= 2) {
            if (getGetNumericValFromX().get("driver_1_age", null, data) == null) {
                return "PR";
            }
            if (getGetNumericValFromX().get("driver_1_age", null, data) > 44) {
                if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                    return "PR";
                }
                if (getGetNumericValFromX().get("annualized_premium", null, data) > 348.909) {
                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                        return "PR";
                    }
                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                        if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("count_vehicles", null, data) > 4) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("count_vehicles", null, data) <= 4) {
                            if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("zip", null, data) > 76747) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("zip", null, data) <= 76747) {
                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 7) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 7) {
                                        return "PR";
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 5) {
                                        if (getGetNumericValFromX().get("zip", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) > 14114) {
                                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 14) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 14) {
                                                return "NS";
                                            }
                                        }
                                        if (getGetNumericValFromX().get("zip", null, data) <= 14114) {
                                            return "PR";
                                        }
                                    }
                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 5) {
                                        return "PR";
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                    return "PR";
                                }
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 53) {
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 3) {
                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Married") {
                                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) > 82917) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("zip", null, data) <= 82917) {
                                        if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("count_vehicles", null, data) > 3) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("count_vehicles", null, data) <= 3) {
                                            return "PR";
                                        }
                                    }
                                }
                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Married") {

                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 5) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 5) {
                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 9) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 9) {
                                            return "PR";
                                        }
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 3) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                    if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 2) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 2) {
                                            return "PR";
                                        }
                                    }
                                    if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 9) {
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Rent") {

                                                return "PR";
                                            }
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Rent") {

                                                return "PR";
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 9) {
                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 75298) {
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 1) {
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 15) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 15) {
                                                        return "PR";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 1) {
                                                    return "NS";
                                                }
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 75298) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1453.70422) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1453.70422) {
                                                    return "PR";
                                                }
                                            }
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 2084.70439) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 2084.70439) {
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Single") {
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 14) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 14) {
                                                return "PR";
                                            }
                                        }
                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Single") {

                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 77018) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 77018) {
                                                return "PR";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 53) {
                            if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                                if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {

                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 4) {
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 1) {
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 21) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 21) {
                                                return "PR";
                                            }
                                        }
                                        if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 1) {
                                            return "PR";
                                        }
                                    }
                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 4) {
                                        return "PR";
                                    }
                                }
                                if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {

                                    if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) > 7) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_comp_mixed", null, data) <= 7) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 9) {
                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 1) {
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 16) {
                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Rent") {

                                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 4500) {
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 9) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 9) {
                                                                return "PR";
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 4500) {
                                                            return "NS";
                                                        }
                                                    }
                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Rent") {

                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 5) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 5) {
                                                            return "PR";
                                                        }
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 16) {
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 48) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 48) {
                                                        return "NS";
                                                    }
                                                }
                                            }
                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 1) {
                                                return "NS";
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 9) {
                                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 1614.89676) {
                                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                                    return "PR";
                                                }
                                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Married") {

                                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 28) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 28) {
                                                        return "PR";
                                                    }
                                                }
                                                if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Married") {

                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 1) {
                                                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 24) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 24) {
                                                            return "NS";
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 1) {
                                                        return "NS";
                                                    }
                                                }
                                            }
                                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1614.89676) {
                                                if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 4) {
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 534.63643) {
                                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 3) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 3) {
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                                return "PR";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Rent") {

                                                                return "PR";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Rent") {

                                                                return "PR";
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 534.63643) {
                                                        return "NS";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 4) {
                                                    return "PR";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 348.909) {
                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 78.53) {
                        if (getGetNumericValFromX().get("driver_1_age", null, data) > 52) {
                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("zip", null, data) > 74830) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("zip", null, data) <= 74830) {
                                if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 3) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 3) {
                                    return "PR";
                                }
                            }
                        }
                        if (getGetNumericValFromX().get("driver_1_age", null, data) <= 52) {
                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 25) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 25) {
                                return "NS";
                            }
                        }
                    }
                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 78.53) {
                        return "NS";
                    }
                }
            }
            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 44) {
                if (getGetNumericValFromX().get("count_vehicles", null, data) == null) {
                    return "NS";
                }
                if (getGetNumericValFromX().get("count_vehicles", null, data) > 1) {
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                        return "PR";
                    }
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {

                        if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 868.32933) {
                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 1) {
                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 8) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 8) {
                                        return "PR";
                                    }
                                }
                            }
                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 1) {
                                return "PR";
                            }
                        }
                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 868.32933) {
                            if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 8) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 8) {
                                return "PR";
                            }
                        }
                    }
                    if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {

                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                            return "PR";
                        }
                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 3) {
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 10) {
                                if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                                    return "PR";
                                }
                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 941.63178) {
                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 3) {
                                            return "PR";
                                        }
                                        if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 3) {
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 7) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 7) {
                                                return "NS";
                                            }
                                        }
                                    }
                                }
                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 941.63178) {
                                    return "NS";
                                }
                            }
                            if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 10) {
                                return "NS";
                            }
                        }
                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 3) {
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 2) {
                                return "PR";
                            }
                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 2) {
                                return "NS";
                            }
                            if (getGetNumericValFromX().get("count_vehicles", null, data) <= 1) {
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) == null) {
                                    return "NS";
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 9) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                                        return "NS";
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 275.24913) {
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 13) {
                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) > 15739) {
                                                if (getGetNumericValFromX().get("driver_1_age", null, data) > 24) {
                                                    if (getGetNumericValFromX().get("prior_insurance", null, data) == null) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("prior_insurance", null, data) > 0) {
                                                        if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 1) {
                                                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                                                                return "NS";
                                                            }
                                                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 2) {
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 14375) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 14375) {
                                                                    return "NS";
                                                                }
                                                            }
                                                            if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 2) {
                                                                return "NS";
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 1) {
                                                            return "NS";
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("prior_insurance", null, data) <= 0) {
                                                        return "NS";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("driver_1_age", null, data) <= 24) {
                                                    return "NS";
                                                }
                                            }
                                            if (getGetNumericValFromX().get("zip", null, data) <= 15739) {
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) == null) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) > 19) {
                                                    return "PR";
                                                }
                                                if (getGetNumericValFromX().get("driver_1_years_licensed", null, data) <= 19) {
                                                    return "NS";
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 13) {
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) > 38) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 553.66558) {
                                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 1) {
                                                        if (getGetNumericValFromX().get("driver_1_count_atfault", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_count_atfault", null, data) > 0) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("driver_1_count_atfault", null, data) <= 0) {
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                                return "NS";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {

                                                                return "PR";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                                                                return "NS";
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 1) {
                                                        return "NS";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 553.66558) {
                                                    return "NS";
                                                }
                                            }
                                            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 38) {
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 498.80018) {
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 1531.69194) {
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                                                            return "NS";
                                                        }
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                                return "NS";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 64) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 64) {
                                                                if (getGetNumericValFromX().get("count_drivers", null, data) == null) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("count_drivers", null, data) > 2) {
                                                                    return "PR";
                                                                }
                                                                if (getGetNumericValFromX().get("count_drivers", null, data) <= 2) {
                                                                    return "NS";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1531.69194) {
                                                        if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("zip", null, data) > 13292) {
                                                            if (getGetNumericValFromX().get("zip", null, data) > 20873) {
                                                                if (getGetNumericValFromX().get("driver_1_age", null, data) > 28) {
                                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                                        return "NS";
                                                                    }
                                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                                                                        return "PR";
                                                                    }
                                                                    if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {

                                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) == null) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) > 0) {
                                                                            if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == null) {
                                                                                return "NS";
                                                                            }
                                                                            if (getGetStringValFromX().get("vehicle_1_model_type", null, data) == "Van_Minivan") {

                                                                                return "NS";
                                                                            }
                                                                            if (getGetStringValFromX().get("vehicle_1_model_type", null, data) != "Van_Minivan") {

                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                                                    return "NS";
                                                                                }
                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 5) {
                                                                                    return "PR";
                                                                                }
                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 5) {
                                                                                    return "NS";
                                                                                }
                                                                            }
                                                                        }
                                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) <= 0) {
                                                                            return "NS";
                                                                        }
                                                                    }
                                                                }
                                                                if (getGetNumericValFromX().get("driver_1_age", null, data) <= 28) {
                                                                    return "NS";
                                                                }
                                                            }
                                                            if (getGetNumericValFromX().get("zip", null, data) <= 20873) {
                                                                return "NS";
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("zip", null, data) <= 13292) {
                                                            return "PR";
                                                        }
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 498.80018) {
                                                    return "NS";
                                                }
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 275.24913) {
                                        return "NS";
                                    }
                                }
                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 9) {
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) == null) {
                                        return "PR";
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) > 680.61005) {
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 1778.43715) {
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                                                if (getGetNumericValFromX().get("no_prior_ins_index", null, data) > 1) {
                                                    if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) == null) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) > 2) {
                                                        return "NS";
                                                    }
                                                    if (getGetNumericValFromX().get("requested_coverage_mixed", null, data) <= 2) {
                                                        return "NS";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("no_prior_ins_index", null, data) <= 1) {
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 14) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 14) {
                                                        return "PR";
                                                    }
                                                }
                                            }
                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("zip", null, data) > 15717) {
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 36) {
                                                        if (getGetNumericValFromX().get("count_drivers", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("count_drivers", null, data) > 1) {
                                                            return "PR";
                                                        }
                                                        if (getGetNumericValFromX().get("count_drivers", null, data) <= 1) {
                                                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 1) {
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 4000) {
                                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                                                        return "NS";
                                                                    }
                                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 1) {
                                                                        return "NS";
                                                                    }
                                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 1) {
                                                                        return "NS";
                                                                    }
                                                                }
                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 4000) {
                                                                    return "NS";
                                                                }
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 1) {
                                                                return "PR";
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 36) {
                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) == null) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) > 0) {
                                                            if (getGetNumericValFromX().get("annualized_premium", null, data) > 4631.33167) {
                                                                return "NS";
                                                            }
                                                            if (getGetNumericValFromX().get("annualized_premium", null, data) <= 4631.33167) {
                                                                if (getGetNumericValFromX().get("current_liability_limits_mixed", null, data) == null) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("current_liability_limits_mixed", null, data) > 5) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("current_liability_limits_mixed", null, data) <= 5) {
                                                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                                        return "NS";
                                                                    }
                                                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 5625) {
                                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) == null) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) > 8) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("vehicle_1_desired_coll_mixed", null, data) <= 8) {
                                                                            return "NS";
                                                                        }
                                                                    }
                                                                    if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 5625) {
                                                                        return "PR";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("prior_insurance", null, data) <= 0) {
                                                            return "NS";
                                                        }
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("zip", null, data) <= 15717) {
                                                    if (getGetNumericValFromX().get("zip", null, data) > 5556) {
                                                        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) > 1) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("no_prior_ins_index", null, data) <= 1) {
                                                            return "PR";
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("zip", null, data) <= 5556) {
                                                        return "NS";
                                                    }
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1778.43715) {
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 5) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 5) {
                                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) > 5) {
                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) == null) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) > 1) {
                                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) == "Single") {

                                                            if (getGetNumericValFromX().get("driver_1_age", null, data) > 39) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("driver_1_age", null, data) <= 39) {
                                                                if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                                    return "NS";
                                                                }
                                                                if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Other") {

                                                                    return "PR";
                                                                }
                                                                if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Other") {
                                                                    if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                                        return "NS";
                                                                    }
                                                                    if (getGetNumericValFromX().get("zip", null, data) > 13402) {
                                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 1743.53) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1743.53) {
                                                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) == null) {
                                                                                return "NS";
                                                                            }
                                                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) > 4) {
                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) == null) {
                                                                                    return "NS";
                                                                                }
                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) > 4) {
                                                                                    return "PR";
                                                                                }
                                                                                if (getGetNumericValFromX().get("driver_1_highest_degree_mixed", null, data) <= 4) {
                                                                                    return "NS";
                                                                                }
                                                                            }
                                                                            if (getGetNumericValFromX().get("pd_coverage_mixed", null, data) <= 4) {
                                                                                return "NS";
                                                                            }
                                                                        }
                                                                    }
                                                                    if (getGetNumericValFromX().get("zip", null, data) <= 13402) {
                                                                        return "PR";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (getGetStringValFromX().get("driver_1_marital_status_new", null, data) != "Single") {
                                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 13125) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 13125) {
                                                                return "PR";
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("current_insurance_coverage_length_mixed", null, data) <= 1) {
                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 798.10818) {
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                                return "NS";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Rent") {

                                                                return "NS";
                                                            }
                                                            if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Rent") {

                                                                return "PR";
                                                            }
                                                        }
                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 798.10818) {
                                                            return "NS";
                                                        }
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("vehicle_1_age_old", null, data) <= 5) {
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) > 40) {
                                                        return "PR";
                                                    }
                                                    if (getGetNumericValFromX().get("driver_1_age", null, data) <= 40) {
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == null) {
                                                            return "PR";
                                                        }
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) == "Own") {
                                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("zip", null, data) > 14718) {
                                                                if (getGetNumericValFromX().get("annualized_premium", null, data) > 1567.99951) {
                                                                    return "NS";
                                                                }
                                                                if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1567.99951) {
                                                                    if (getGetNumericValFromX().get("uim_coverage_mixed", null, data) == null) {
                                                                        return "PR";
                                                                    }
                                                                    if (getGetNumericValFromX().get("uim_coverage_mixed", null, data) > 1) {
                                                                        return "PR";
                                                                    }
                                                                    if (getGetNumericValFromX().get("uim_coverage_mixed", null, data) <= 1) {
                                                                        return "NS";
                                                                    }
                                                                }
                                                            }
                                                            if (getGetNumericValFromX().get("zip", null, data) <= 14718) {
                                                                return "PR";
                                                            }
                                                        }
                                                        if (getGetStringValFromX().get("current_resident_status_new", null, data) != "Own") {
                                                            if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("zip", null, data) > 13741) {
                                                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 4) {
                                                                    if (getGetNumericValFromX().get("zip", null, data) > 36448) {
                                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 1722.17409) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 1722.17409) {
                                                                            if (getGetNumericValFromX().get("zip", null, data) > 37669) {
                                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) == null) {
                                                                                    return "PR";
                                                                                }
                                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) > 24000) {
                                                                                    return "NS";
                                                                                }
                                                                                if (getGetNumericValFromX().get("vehicle_1_est_annual_miles", null, data) <= 24000) {
                                                                                    if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) == null) {
                                                                                        return "PR";
                                                                                    }
                                                                                    if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) > 4) {
                                                                                        if (getGetNumericValFromX().get("driver_1_count_violations", null, data) == null) {
                                                                                            return "PR";
                                                                                        }
                                                                                        if (getGetNumericValFromX().get("driver_1_count_violations", null, data) > 1) {
                                                                                            return "NS";
                                                                                        }
                                                                                        if (getGetNumericValFromX().get("driver_1_count_violations", null, data) <= 1) {
                                                                                            if (getGetNumericValFromX().get("zip", null, data) > 94931) {
                                                                                                return "PR";
                                                                                            }
                                                                                            if (getGetNumericValFromX().get("zip", null, data) <= 94931) {
                                                                                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) == null) {
                                                                                                    return "PR";
                                                                                                }
                                                                                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) > 4) {
                                                                                                    return "PR";
                                                                                                }
                                                                                                if (getGetNumericValFromX().get("vehicle_1_est_commute_miles", null, data) <= 4) {
                                                                                                    return "PR";
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    if (getGetNumericValFromX().get("bi_coverage_mixed", null, data) <= 4) {
                                                                                        return "NS";
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (getGetNumericValFromX().get("zip", null, data) <= 37669) {
                                                                                return "PR";
                                                                            }
                                                                        }
                                                                    }
                                                                    if (getGetNumericValFromX().get("zip", null, data) <= 36448) {
                                                                        if (getGetNumericValFromX().get("zip", null, data) > 29752) {
                                                                            return "NS";
                                                                        }
                                                                        if (getGetNumericValFromX().get("zip", null, data) <= 29752) {
                                                                            return "PR";
                                                                        }
                                                                    }
                                                                }
                                                                if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 4) {
                                                                    return "NS";
                                                                }
                                                            }
                                                            if (getGetNumericValFromX().get("zip", null, data) <= 13741) {
                                                                return "PR";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (getGetNumericValFromX().get("annualized_premium", null, data) <= 680.61005) {
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) > 102.0025) {
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) == null) {
                                                return "NS";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) > 6) {
                                                return "PR";
                                            }
                                            if (getGetNumericValFromX().get("driver_1_credit_mixed", null, data) <= 6) {
                                                if (getGetNumericValFromX().get("zip", null, data) == null) {
                                                    return "NS";
                                                }
                                                if (getGetNumericValFromX().get("zip", null, data) > 13661) {
                                                    if (getGetNumericValFromX().get("zip", null, data) > 36073) {
                                                        if (getGetNumericValFromX().get("zip", null, data) > 82708) {
                                                            return "NS";
                                                        }
                                                        if (getGetNumericValFromX().get("zip", null, data) <= 82708) {
                                                            if (getGetNumericValFromX().get("count_trucks", null, data) == null) {
                                                                return "NS";
                                                            }
                                                            if (getGetNumericValFromX().get("count_trucks", null, data) > 0) {
                                                                return "PR";
                                                            }
                                                            if (getGetNumericValFromX().get("count_trucks", null, data) <= 0) {
                                                                return "NS";
                                                            }
                                                        }
                                                    }
                                                    if (getGetNumericValFromX().get("zip", null, data) <= 36073) {
                                                        return "NS";
                                                    }
                                                }
                                                if (getGetNumericValFromX().get("zip", null, data) <= 13661) {
                                                    return "PR";
                                                }
                                            }
                                        }
                                        if (getGetNumericValFromX().get("annualized_premium", null, data) <= 102.0025) {
                                            return "NS";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
        /**
     * for read the commission.csv
     */
    public class Commission {

        private String carrier;
        private String code;
        private double commRate;
        private double fixedComm;
        private double id;
        private String product;
        private double secRate;
        private int term;
        private String tier;

        /**
         * @return the carrier
         */
        public String getCarrier() {
            return carrier;
        }

        /**
         * @param carrier the carrier to set
         */
        public void setCarrier(String carrier) {
            this.carrier = carrier;
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code the code to set
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return the commRate
         */
        public double getCommRate() {
            return commRate;
        }

        /**
         * @param commRate the commRate to set
         */
        public void setCommRate(double commRate) {
            this.commRate = commRate;
        }

        /**
         * @return the fixedComm
         */
        public double getFixedComm() {
            return fixedComm;
        }

        /**
         * @param fixedComm the fixedComm to set
         */
        public void setFixedComm(double fixedComm) {
            this.fixedComm = fixedComm;
        }

        /**
         * @return the id
         */
        public double getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(double id) {
            this.id = id;
        }

        /**
         * @return the product
         */
        public String getProduct() {
            return product;
        }

        /**
         * @param product the product to set
         */
        public void setProduct(String product) {
            this.product = product;
        }

        /**
         * @return the secRate
         */
        public double getSecRate() {
            return secRate;
        }

        /**
         * @param secRate the secRate to set
         */
        public void setSecRate(double secRate) {
            this.secRate = secRate;
        }

        /**
         * @return the term
         */
        public int getTerm() {
            return term;
        }

        /**
         * @param term the term to set
         */
        public void setTerm(int term) {
            this.term = term;
        }

        /**
         * @return the tier
         */
        public String getTier() {
            return tier;
        }

        /**
         * @param tier the tier to set
         */
        public void setTier(String tier) {
            this.tier = tier;
        }
    }

}
