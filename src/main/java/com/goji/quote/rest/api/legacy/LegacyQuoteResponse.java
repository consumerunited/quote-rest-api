package com.goji.quote.rest.api.legacy;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Getter
@Setter
public class LegacyQuoteResponse {
    
    private Integer id;
    
    private String status;
    
    @JsonIgnore
    private Map<String,String> predictions = new HashMap<>();
    
    @JsonAnyGetter
    public Map<String,String> get() {
        return predictions;
    }
    
    @JsonAnySetter
    public void set(String carrier, String prediction) {
        this.predictions.put(carrier, prediction);
    }
    
}
