package com.goji.quote.rest.api.service;

import com.goji.quote.utils.SkynetStaticUtil;
import com.goji.quote.utils.PithonInterface;
import com.goji.quote.utils.SimplePair;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.goji.quote.rest.api.legacy.LegacyQuoteRequest;
import com.goji.quote.utils.URLLib;
import java.io.IOException;
import static java.lang.Math.pow;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SkyNetService implements PithonInterface {

    private GetValueFromX<String> getStringValFromX;
    private GetValueFromX<Double> getNumericValFromX;

    private final static Logger LOG = Logger.getLogger(SkyNetService.class.getName());

    private final ZoneId USEastZone = ZoneId.of("US/Eastern");

    private static final Map<String, Set<String>> ALLOWED_STRING_VALUES;
    private static final Map<String, int[]> ALLOWED_NUMBER_RANGES;
    private static int pd;
    
    @Autowired
    private ObjectMapper objectMapper;

    static {
        ALLOWED_STRING_VALUES = new HashMap<>();
        ALLOWED_NUMBER_RANGES = new HashMap<>();
        initStaticDataAndParams();
    }

    @PostConstruct
    private void initLambda() {
        getStringValFromX = (name, defaultValue, x) -> x.get(name) == null ? defaultValue
                : x.get(name) instanceof String ? (String) x.get(name) : defaultValue;
        //it's gonna throw an exception after tried null, double, int and float.
        getNumericValFromX = (name, defaultValue, x) -> x.get(name) == null ? defaultValue
                : x.get(name) instanceof Double ? (Double) x.get(name)
                : x.get(name) instanceof Integer ? (double) (int) (Integer) x.get(name)
                : x.get(name) instanceof Float ? (double) (float) (Float) x.get(name) : (Double) x.get(name);
    }

    

    /**
     * Tested
     *
     * @param name
     * @param sp
     * @param defaultValue
     * @param predicatedValue
     * @param x
     * @return
     * @throws Exception
     */
    public double getArbitraryField(String name, StringPredicator sp, double defaultValue, double predicatedValue, Map<String, Object> x) throws Exception {
        double val = 0;
        try {
            val = getGetNumericValFromX().get(name, defaultValue, x);
        } catch (Exception ex) {
            LOG.warning("Unable to parse numeric value for: " + name);
            String valStr = getGetStringValFromX().get(name, null, x);
            if (sp.validate(valStr)) {
                val = predicatedValue;
            } else {
                throw new Exception("SkyNet cannot get field " + name + ", it's must be a numeric value or a valid string");
            }
        }
        return val;
    }

    /**
     * Tested
     *
     * @param s
     * @return
     */
    public String getHomeOwnership(String s) {
        if (s == null) {
            return null;
        }
        if ("rent".equals(s.toLowerCase())) {
            return "apartment";
        } else {
            return "home (owned)";
        }
    }

    /**
     * NOT tested, unnecessary
     *
     * @param x
     * @param qdD1Age
     * @return
     */
    public int getYearsLicensed(int x, int qdD1Age) {
        if (qdD1Age >= 30 && x == 0) {
            Calculator nonNegative = age -> age >= 0 ? age : 0;
            return (int) nonNegative.calculate(qdD1Age);
        } else {
            return x;
        }
    }

    /**
     * Tested
     *
     * @param x
     * @param qdV1Age
     * @return String[year, make, model]
     */
    public String[] getYearMakeModel(Map<String, Object> x, int qdV1Age) {
        String[] res = new String[3];
        LocalDateTime dateTime = LocalDateTime.now();
        ZonedDateTime USEastDateTime = ZonedDateTime.of(dateTime, USEastZone);
        int currentYear = USEastDateTime.getYear();

        int vehicleYear = currentYear - qdV1Age;
        String vehicleMake = (String) getGetStringValFromX().get("vehicle_1_make_new", "", x);
        String vehicleModel = (String) getGetStringValFromX().get("vehicle_1_model_old", "", x);
        if (vehicleMake.isEmpty() || vehicleModel.isEmpty() || !ALLOWED_STRING_VALUES.get("v1_make").contains(vehicleMake.toLowerCase())) {
            vehicleMake = "honda";
            vehicleModel = "civic";
        } else {
            vehicleMake = vehicleMake.toLowerCase();
            vehicleModel = vehicleModel.toLowerCase();
        }
        res[0] = vehicleYear + "";
        res[1] = vehicleMake;
        res[2] = vehicleModel;
        return res;
    }

    /**
     * Tested, but the result is invalid. Need to check carcost service.
     * @param year
     * @param vMake
     * @param vModel
     * @return double
     * @throws JsonProcessingException
     */
    public double getCarCost(int year, String vMake, String vModel) throws JsonProcessingException, IOException {
        final String url = "http://www.goji.com/carcost";
        String vehicleMake = title(vMake);
        String vehicleModel = title(vModel);
        
        final Client client = ClientBuilder.newClient().register(new JacksonJsonProvider(objectMapper));
        final WebTarget target = client.target(url);
        
        CarCostData c = new CarCostData();
        c.setVehicle_year(year);
        c.setVehicle_make(vehicleMake);
        c.setVehicle_model(vehicleModel);
        
        final Response response = target.request("application/json").buildPost(Entity.json(c)).invoke();
        response.bufferEntity();
        if (response.readEntity(String.class).equals("0")) {
            return 20849.0;
        } else {
            return response.readEntity(CarCostResponse.class).getCost();
        }
    }

    /**
     * probably throw runtime null pointer exception. Not tested, unnecessary,
     * just a decision tree.
     * @param qd
     * @return result from a not that big decision tree
     */
    public SimplePair<String, String> getRecoCoverage(LegacyQuoteRequest qd) {
        SimplePair<String, String> pair = new SimplePair<String, String>("BI_coverage", null);

        int d1Age = qd.getD1Age();
        double v1CostNew = qd.getV1CostNew();
        String homeOwnership = qd.getHomeOwnership().toJson();
        String d1MaritalStatus = qd.getD1MaritalStatus().toJson();

        String liability = null;

        if (d1Age < 30) {
            if (v1CostNew < 15000 || (v1CostNew >= 15000 && v1CostNew <= 30000)) {
                if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                    liability = "100/300";
                    pd = 100000;
                } else {
                    liability = "state minimum";
                    pd = -1;
                }
            } else if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                liability = "100/300";
                pd = 100000;
            } else if ("married".equals(d1MaritalStatus)) {
                liability = "100/300";
                pd = 100000;
            } else {
                liability = "50/100";
                pd = 50000;
            }
        } else if (d1Age >= 30 && d1Age <= 50) {
            if (v1CostNew < 15000) {
                if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                    liability = "100/300";
                    pd = 100000;
                } else {
                    liability = "state minimum";
                    pd = -1;
                }
            } else if (v1CostNew >= 15000 && v1CostNew <= 30000) {
                if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                    liability = "100/300";
                    pd = 100000;
                } else if ("married".equals(d1MaritalStatus)) {
                    liability = "50/100";
                    pd = 50000;
                } else {
                    liability = "state minimum";
                    pd = -1;
                }
            } else if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                liability = "100/300";
                pd = 100000;
            } else if ("married".equals(d1MaritalStatus)) {
                liability = "100/300";
                pd = 100000;
            } else {
                liability = "50/100";
                pd = 50000;
            }
        } else if (v1CostNew < 15000) {
            if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                liability = "100/300";
                pd = 100000;
            } else {
                liability = "state minimum";
                pd = -1;
            }
        } else if (v1CostNew >= 15000 && v1CostNew <= 30000) {
            if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
                liability = "100/300";
                pd = 100000;
            } else if ("married".equals(d1MaritalStatus)) {
                liability = "50/100";
                pd = 50000;
            } else {
                liability = "state minimum";
                pd = -1;
            }
        } else if ("condo (owned)".equals(homeOwnership) || "home (owned)".equals(homeOwnership)) {
            liability = "100/300";
            pd = 100000;
        } else if ("married".equals(d1MaritalStatus)) {
            liability = "100/300";
            pd = 100000;
        } else {
            liability = "50/100";
            pd = 50000;
        }
        pair.setValue(liability);
        return pair;
    }

    /**
     * Tested
     *
     * @param x
     * @param pair
     * @return [BI_coverage, UIM_coverage]
     */
    public String[] getBiUimCoverage(Map<String, Object> x, SimplePair<String, String> pair) {
        if (pair == null || !"BI_coverage".equals(pair.getKey())) {
            LOG.severe("Wrong pair passed in, key is not correct for BI_coverage");
            return null;
        }
        String[] res = new String[2];
        String BI_coverage = (String) getGetStringValFromX().get("BI_coverage_new", pair.getValue(), x);
        String UIM_coverage = (String) getGetStringValFromX().get("UIM_coverage_new", BI_coverage, x);
        String mincheck = (String) getGetStringValFromX().get("requested_coverage_new", "default", x);
        if (mincheck.toLowerCase().contains("min") || BI_coverage.toLowerCase().contains("min") || UIM_coverage.toLowerCase().contains("min")) {
            BI_coverage = "state minimum";
            UIM_coverage = "state minimum";
        }
        if (!ALLOWED_STRING_VALUES.get("BI_coverage").contains(BI_coverage)) {
            BI_coverage = pair.getValue();
        }

        if (!ALLOWED_STRING_VALUES.get("UIM_coverage").contains(UIM_coverage)) {
            /**
             * now UIM includes all the BI terms according to the init static
             * data. Therefore, logic simplified: if not in UIM, not in BI, UIM
             * = pair.liability.
             */
            if (!ALLOWED_STRING_VALUES.get("BI_coverage").contains(UIM_coverage)) {
                UIM_coverage = pair.getValue();
            } else {
                UIM_coverage = BI_coverage;
            }
        }
        res[0] = BI_coverage;
        res[1] = UIM_coverage;
        return res;
    }

    /**
     * sub-method of getPdCoverage, BI only allows the d/d and state minimum No
     * Not tested, unnecessary
     *
     * @param BI_coverage
     * @return
     */
    public double getRecoPd(String BI_coverage) {
        if ("state minimum".equals(BI_coverage) || BI_coverage == null) {
            return -1;
        } else {
            return (double) (Double.parseDouble(BI_coverage.split("/")[0])) * 1000;
        }
    }

    /**
     * Tested
     *
     * @param x
     * @param BI_coverage
     * @param minimum
     * @param maximum
     * @return
     */
    public double getPdCoverage(Map<String, Object> x, String BI_coverage, final double minimum, final double maximum) {

        double coverage = 0.0;
        String minCheck = (String) getGetStringValFromX().get("requested_coverage_new", "default", x);

        /**
         * Calculator c1(python): coverage = (lambda x: -1 if (x=='State
         * Minimum' or ('min' in mincheck.lower())) else x)(coverage) Calculator
         * c2 = input->isNumeric(input)?input:getRecoPd(BI_coverage);
         */
        String coverageStr = (String) getGetStringValFromX().get("PD_coverage_new", null, x);
        if ((coverageStr != null && !coverageStr.isEmpty() && ("State Minimum".equalsIgnoreCase(coverageStr)) || minCheck.toLowerCase().contains("min"))) {
            coverage = -1;
        };
        try {
            coverage = (double) getGetNumericValFromX().get("PD_coverage_new", getRecoPd(BI_coverage), x);
        } catch (Exception ex) {
            //Exception happens when state minimum, or other string values
            LOG.warning("parsing PD to double failed, coverage value is " + coverage + ", coverage Str is " + coverageStr);
            if (coverage == 0.0) { //handle non state minimum case
                coverage = getRecoPd(BI_coverage);
            }
        }

        //Calculator c3(python): coverage = (lambda x: get_reco_pd() if np.isnan(x) else x)(float(coverage))   unnessary null check in Java, cuz it should have been double already
        Calculator c4 = input -> minimum > input || maximum < input ? getRecoPd(BI_coverage) : input;
        return c4.calculate(coverage);
    }

    /**
     * submethod of getCompColl Not tested, unnecessary
     *
     * @param qd
     * @return
     */
    public double getRecoCompcol(LegacyQuoteRequest qd) {
        double discountedcarcost = qd.getV1CostNew() * pow(0.875, qd.getV1Age());
        Set<String> tempSet = new HashSet<>();
        tempSet.add("leased");
        tempSet.add("lien");
        Set<String> tempSet2 = new HashSet<>();
        tempSet2.add("owned");
        if (tempSet.contains(qd.getV1Ownership().toJson())) {
            return 500;
        } else if (tempSet2.contains(qd.getV1Ownership().toJson()) && discountedcarcost < 6000) {
            return 0;
        } else {
            return 500;
        }
    }

    /**
     * Tested
     *
     * @param x
     * @param qd
     * @return double[comp, coll]
     * @throws Exception
     */
    public double[] getCompColl(Map<String, Object> x, LegacyQuoteRequest qd) throws Exception {
        double[] res = new double[2];
        double recoColl = getRecoCompcol(qd);
        double recoComp = recoColl;
        StringPredicator isNoCoverage = str -> "no coverage".equalsIgnoreCase(str);
        res[0] = this.getArbitraryField("desired_comprehensive_deductible", isNoCoverage, recoComp, 0, x);
        res[1] = this.getArbitraryField("desired_collision_deductible", isNoCoverage, recoColl, 0, x);
        return res;
    }

    /**
     * submethod of getPriorCarrier Tested ! Really has to test based on real
     * data, current logic assumes 1-2 = 12 (combine numbers around "-");
     *
     * @return
     */
    public int parseYearsPrior(String s) {
        if ("0".equals(s) || s == null || s.isEmpty()) {
            return 0;
        } else if ("less than 1 year".equals(s.toLowerCase())) {
            return 0;
        } else {
            s = s.replaceAll("-", "");
            //split into words [a-zA-Z0-9]
            List<String> splitted = split(s);
            List<String> nums = splitted.stream().filter(str -> isNumeric(str)).collect(Collectors.toList());
            if (nums == null || nums.size() < 1) {
                return 0;
            }
            double mean = (nums.stream().mapToDouble(str -> Double.parseDouble(str)).reduce(0.0, (a, b) -> a + b)) / nums.size();
            return (int) mean;
        }
    }

    /**
     * Not Tested, unnecessary
     *
     * @param x
     * @return
     */
    public String[] getPriorCarrier(Map<String, Object> x) {
        String carrier = getGetStringValFromX().get("current_insurance_carrier_new", "no prior insurance", x);
        String unsureNotListedCheck = getGetStringValFromX().get("current_insurance_carrier_old", "no prior insurance", x);
        String yearsPrior = getGetStringValFromX().get("current_insurance_coverage_length_new", "0", x);
        String priorLimit = getGetStringValFromX().get("current_liability_limits_new", "state minimum", x).toLowerCase();

        int priorYear = parseYearsPrior(yearsPrior);
        if ("no prior insurance".equalsIgnoreCase(carrier)) {
            if ("company not listed".equalsIgnoreCase(unsureNotListedCheck) || "unsure".equalsIgnoreCase(unsureNotListedCheck)) {
                if (priorYear < 1) {
                    carrier = "other non-standard";
                } else {
                    carrier = "other standard";
                }
            }
        }
        if (!ALLOWED_STRING_VALUES.get("prior_carrier").contains(carrier.toLowerCase())) {
            if (priorYear < 1) {
                carrier = "other non-standard";
            } else {
                carrier = "other standard";
            }
        }

        if ("no prior insurance".equalsIgnoreCase(carrier)) {
            if ("state min".equalsIgnoreCase(priorLimit)) {
                priorLimit = "state minimum";
                if (priorYear <= 1) {
                    carrier = "other non-standard";
                } else {
                    carrier = "other standard";
                }
            } else if (ALLOWED_STRING_VALUES.get("prior_liability_limit").contains(priorLimit.toLowerCase())) {
                if (priorYear <= 1) {
                    carrier = "other non-standard";
                } else {
                    carrier = "other standard";
                }
            } else {
                priorLimit = "none";
            }
        } else if (!ALLOWED_STRING_VALUES.get("prior_liability_limit").contains(priorLimit.toLowerCase())) {
            priorLimit = "state minimum";
        } else {
            //the python code really makes no sense: prior_limit=prior_limit, really?
        }
        String[] res = new String[3];
        res[0] = priorYear + "";
        res[1] = carrier;
        res[2] = priorLimit;
        return res;
    }

    /**
     * init all the static map, set, or other data/param here
     */
    private static void initStaticDataAndParams() {
        ALLOWED_STRING_VALUES.put("prior_liability_limit", SkynetStaticUtil.getValidPriorLiabilityLimit());
        ALLOWED_STRING_VALUES.put("UM_coverage", SkynetStaticUtil.getUMCoverage());
        ALLOWED_STRING_VALUES.put("UIM_coverage", SkynetStaticUtil.getUIMCoverage());
        ALLOWED_STRING_VALUES.put("gender", SkynetStaticUtil.getGenders());
        ALLOWED_STRING_VALUES.put("BI_coverage", SkynetStaticUtil.getBICoverage());
        ALLOWED_STRING_VALUES.put("v1_ownership", SkynetStaticUtil.getV1Ownership());
        ALLOWED_STRING_VALUES.put("MP_coverage", SkynetStaticUtil.getMPCoverage());
        ALLOWED_STRING_VALUES.put("home_ownership", SkynetStaticUtil.getHomeOwnership());
        ALLOWED_STRING_VALUES.put("d1_marital_status", SkynetStaticUtil.getMaritalStatus());
        ALLOWED_STRING_VALUES.put("v1_make", SkynetStaticUtil.getV1Make());
        ALLOWED_STRING_VALUES.put("prior_carrier", SkynetStaticUtil.getPriorCarrier());
        ALLOWED_STRING_VALUES.put("state", SkynetStaticUtil.getStates());

        ALLOWED_NUMBER_RANGES.put("PD_coverage", SkynetStaticUtil.getPDCoveragesRange());
        ALLOWED_NUMBER_RANGES.put("v1_collision_deductible", SkynetStaticUtil.getV1CollisionDeductibleRange());
        ALLOWED_NUMBER_RANGES.put("v1_other_collision_deductible", SkynetStaticUtil.getV1OtherCollisionDeductible());
        ALLOWED_NUMBER_RANGES.put("v1_cost_new", SkynetStaticUtil.getV1CostNew());
        ALLOWED_NUMBER_RANGES.put("years_with_prior_carrier", SkynetStaticUtil.getYearsWithPriorCarrier());
        ALLOWED_NUMBER_RANGES.put("d1_years_licensed", SkynetStaticUtil.getD1YearsLicensed());
        ALLOWED_NUMBER_RANGES.put("d1_age", SkynetStaticUtil.getD1Age());
        ALLOWED_NUMBER_RANGES.put("v1_age", SkynetStaticUtil.getV1Age());
        ALLOWED_NUMBER_RANGES.put("count_drivers", SkynetStaticUtil.getCountDrivers());
        ALLOWED_NUMBER_RANGES.put("count_vehicles", SkynetStaticUtil.getCountVehicles());

    }

    /**
     * @return the getStringValFromX
     */
    public GetValueFromX<String> getGetStringValFromX() {
        return getStringValFromX;
    }

    /**
     * @return the getNumericValFromX
     */
    public GetValueFromX<Double> getGetNumericValFromX() {
        return getNumericValFromX;
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class CarCostResponse{
        private int cost;

        /**
         * @return the cost
         */
        public int getCost() {
            return cost;
        }

        /**
         * @param cost the cost to set
         */
        public void setCost(int cost) {
            this.cost = cost;
        }
    }

    public static class CarCostData {

        private int vehicle_year;
        private String vehicle_make;
        private String vehicle_model;

        /**
         * @return the vehicle_year
         */
        public int getVehicle_year() {
            return vehicle_year;
        }

        /**
         * @param vehicle_year the vehicle_year to set
         */
        public void setVehicle_year(int vehicle_year) {
            this.vehicle_year = vehicle_year;
        }

        /**
         * @return the vehicle_make
         */
        public String getVehicle_make() {
            return vehicle_make;
        }

        /**
         * @param vehicle_make the vehicle_make to set
         */
        public void setVehicle_make(String vehicle_make) {
            this.vehicle_make = vehicle_make;
        }

        /**
         * @return the vehicle_model
         */
        public String getVehicle_model() {
            return vehicle_model;
        }

        /**
         * @param vehicle_model the vehicle_model to set
         */
        public void setVehicle_model(String vehicle_model) {
            this.vehicle_model = vehicle_model;
        }
        
        
    }

}
