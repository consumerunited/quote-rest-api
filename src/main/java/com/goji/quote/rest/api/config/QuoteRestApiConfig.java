package com.goji.quote.rest.api.config;

import com.hmsonline.dropwizard.spring.SpringServiceConfiguration;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Getter
@Setter
public class QuoteRestApiConfig extends SpringServiceConfiguration {

    private String legacyQuoteApiURL = "https://developer.goji.com/legacy";
    
}
