package com.goji.quote.rest.api.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;
import java.util.List;
import jersey.repackaged.com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * This is the hibernate configuration object for this project
 *
 * @author Yinzara
 */
@Configuration
@EnableMongoRepositories
public class DefaultConfig extends AbstractMongoConfiguration {

    @Autowired
    @SuppressWarnings("rawtypes")
    private List<Converter> converters;

    @Bean
    public ConversionServiceFactoryBean conversionService() {
        final ConversionServiceFactoryBean cs = new ConversionServiceFactoryBean();
        cs.setConverters(Sets.newConcurrentHashSet(converters));
        return cs;
    }

    @Override
    public Mongo mongo() throws Exception {
        final Mongo mongo = new MongoClient("10.0.1.223", 27017);
        mongo.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        return mongo;
    }

    @Override
    protected String getDatabaseName() {
        return "quote-rest-api";
    }

    @Override
    public CustomConversions customConversions() {
        return new CustomConversions(converters);
    }

}
