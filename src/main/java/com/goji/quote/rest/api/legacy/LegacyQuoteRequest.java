package com.goji.quote.rest.api.legacy;

import com.goji.quote.rs.dto.VehicleMake;
import com.goji.quote.rs.dto.HomeOwnership;
import com.goji.quote.rs.dto.MaritalStatus;
import com.goji.quote.rs.dto.VehicleOwnership;
import com.goji.quote.rs.dto.Gender;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.goji.orm.spring.validation.Phone;
import com.goji.quote.rs.api.AutoResource;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Getter
@Setter
public class LegacyQuoteRequest {

    public static final String STATE_MINIMUM = "state minimum";

    public static final String REJET = "reject";

    public static final String NONE = "none";

    /// FIELDS THAT DONT HAVE DEFAULT THAT ARE REQUIRED TO BE SET /////
    @Min(18)
    @Max(99)
    private int d1Age;

    @Min(0)
    @Max(99)
    @NotNull
    private Integer d1YearsLicensed;

    @NotNull
    private MaritalStatus d1MaritalStatus;

    @Pattern(regexp = "[0-9]{5}\\-?([0-9]{4})?")
    @NotNull
    private String fullZip;

//    @Pattern(flags = Pattern.Flag.CASE_INSENSITIVE, regexp = "MD|RI|PA|TX|AZ|CA|CO|NC|OH|VA|GA|IL|MO|NY|WA|AL|CT|FL|KS|MN|NV|NJ|SC|TN|UT|WI|AR|IN|MS|NM|OK|OR|WV|IA|NH|VT|KY|ME")
    @NotNull
    private String state;

    @NotNull
    private VehicleMake v1Make;

    @NotNull
    private Gender gender;

    @NotNull
    private HomeOwnership homeOwnership;

    ///// END OF REQUIRED FIELDS
    @NotNull
    private String apikey = "goji123";

    private String firstName;

    private String lastName;

    @JsonProperty("BI_coverage")
    @Pattern(regexp = "100/100|100/300|1000/1000|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|state minimum")
    @NotNull
    private String biCoverage = STATE_MINIMUM;

    private LocalDate birthDate;

    @Min(1)
    @Max(4)
    private int countDrivers = 1;

    @Min(1)
    @Max(4)
    private int countVehicles = 1;

    @JsonProperty("MP_coverage")
    @NotNull
    @Pattern(regexp = "1000|10000|100000|15000|2000|2500|25000|500|5000|50000|none")
    private String mpCoverage = NONE;

    @JsonProperty("PD_coverage")
    @Min(-1)
    @Max(500000)
    private int pdCoverage = -1;

    @Phone
    private String phone;
   
    @Pattern(regexp=".*\\@.*")
    private String email;

    @NotNull
    @Pattern(regexp = AutoResource.CARRIERS_REGEX)
    private String priorCarrier = "other standard";

    @NotNull
    @Pattern(regexp = AutoResource.LIABILITY_LIMITS_REGEX)
    private String priorLiabilityLimit = "none";

    private Integer priorMonthlyPremium;

    @JsonProperty("UIM_coverage")
    @Pattern(regexp = "100/100|100/300|1000/1000|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|reject|state minimum")
    @NotNull
    private String uimCoverage = STATE_MINIMUM;

    @Pattern(regexp = "100/100|100/300|1000/1000|15/30|20/40|25/25|25/50|250/500|300/300|50/100|50/50|500/500|reject|state minimum")
    @JsonProperty("UM_coverage")
    @NotNull
    private String umCoverage = STATE_MINIMUM;

    @Min(-1)
    @Max(99)
    private int v1Age = -1;

    @JsonProperty("v1_collision_deductible")
    @Min(0)
    @Max(1500)
    private int v1CollisionDeductible = 1000;

    @Min(0)
    @Max(999999)
    private int v1CostNew = 0;

    private String v1Model;

    @JsonProperty("v1_other_collision_deductible")
    @Min(0)
    @Max(1500)
    private int v1OtherCollisionDeductible = 1000;

    @NotNull
    private VehicleOwnership v1Ownership = VehicleOwnership.OWNED;

    private String websiteSession;

    @Min(0)
    @Max(99)
    private int yearsWithPriorCarrier = 0;

}
