package com.goji.quote.rest.api;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.models.Contact;
import io.swagger.models.Info;
import io.swagger.models.License;
import java.util.Arrays;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.springframework.web.context.WebApplicationContext;

public class BootstrapServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        Info info = new Info()
                .title("Swagger Sample App")
                .description("This is a sample server Petstore server.  You can find out more about Swagger "
                        + "at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, "
                        + "you can use the api key `special-key` to test the authorization filters.")
                .termsOfService("http://swagger.io/terms/")
                .contact(new Contact()
                        .email("development@goji.com"))
                .license(new License()
                        .name("Apache 2.0")
                        .url("http://www.apache.org/licenses/LICENSE-2.0.html"));

        ServletContext context = config.getServletContext();
//        Swagger swagger = new Swagger().info(info);
//    swagger.securityDefinition("api_key", new ApiKeyAuthDefinition("api_key", In.HEADER));
//    swagger.securityDefinition("petstore_auth", 
//      new OAuth2Definition()
//        .implicit("http://petstore.swagger.io/api/oauth/dialog")
//        .scope("read:pets", "read your pets")
//        .scope("write:pets", "modify pets in your account"));

        final WebApplicationContext wac = (WebApplicationContext)config.getServletContext().getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
        
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.3");
        if (Arrays.binarySearch(wac.getEnvironment().getActiveProfiles(),"local") < 0) {
            beanConfig.setSchemes(new String[]{"https"});
            beanConfig.setHost("developer.goji.com");
            beanConfig.setBasePath("/qq");
        } else {
            beanConfig.setSchemes(new String[]{"http"});
            beanConfig.setHost("127.0.0.1:8080");
            beanConfig.setBasePath("");
        }
        
        beanConfig.setTitle("Quick Quote API");
        beanConfig.setResourcePackage("com.goji.quote.rs.api.service");
        beanConfig.setScan(true);

        beanConfig.getSwagger().info(info);

        context.setAttribute("swagger", beanConfig.getSwagger());
    }
}
