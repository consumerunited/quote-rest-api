package com.goji.quote.rest.api.legacy;

import com.goji.quote.rs.dto.VehicleMake;
import com.goji.quote.rs.dto.HomeOwnership;
import com.goji.quote.rs.dto.MaritalStatus;
import com.goji.quote.rs.dto.Gender;
import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.goji.orm.spring.config.BaseRepositoryConfig;
import com.goji.orm.spring.validation.PhoneNumberValidator;
import com.goji.quote.rest.api.config.QuoteRestApiConfig;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.logging.BootstrapLogging;
import java.util.logging.Logger;
import javax.validation.Valid;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.filter.LoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
@Service
public class LegacyQuoteService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private QuoteRestApiConfig config;
    
    @Autowired
    private BaseRepositoryConfig repoConfig;

    public LegacyQuoteResponse quote(@Valid final LegacyQuoteRequest request) {
        request.setState(request.getState().toLowerCase());
        request.setFullZip(request.getFullZip().replace("-",""));
        request.setPhone(PhoneNumberValidator.format(request.getPhone(), PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
        return target()
                .path("/")
                .request()
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), LegacyQuoteResponse.class);
    }

    private WebTarget target() {
        Client client = ClientBuilder.newClient().register(new JacksonJsonProvider(objectMapper));
        if (repoConfig!=null && !repoConfig.isProduction()) {
            client = client.register(new LoggingFilter(Logger.getLogger(LoggingFilter.class.getName()), true));
        }
        return client.target(this.config.getLegacyQuoteApiURL());
    }
    
//    public static void main(String[] args) throws Exception {
//        BootstrapLogging.bootstrap(Level.DEBUG);
//        final LegacyQuoteService service = new LegacyQuoteService();
//        service.objectMapper = Jackson.newObjectMapper();
//        service.objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
//        service.config = new QuoteRestApiConfig();
//        
//        
//        final LegacyQuoteRequest request = new LegacyQuoteRequest();
//        request.setD1Age(33);
//        request.setD1YearsLicensed(16);
//        request.setD1MaritalStatus(MaritalStatus.SINGLE);
//        request.setGender(Gender.MALE);
//        request.setFullZip("89129-5980");
//        request.setState("NV");
//        request.setV1Make(VehicleMake.BUICK);
//        request.setHomeOwnership(HomeOwnership.HOME_OWNED);
//        final LegacyQuoteResponse resp = service.quote(request);
//        
//        System.out.println(service.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp));
//    }
}
