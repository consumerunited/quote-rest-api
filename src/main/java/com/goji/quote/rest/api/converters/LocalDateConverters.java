/*
 * Copyright (C) Consumer United LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.goji.quote.rest.api.converters;

import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Matt Morrissette <matt@goji.com>
 */
public class LocalDateConverters {
    
    @Component
    public static class StringToLocalDataConverter implements Converter<String, LocalDate> {

        @Override
        public LocalDate convert(String s) {
            return ISODateTimeFormat.localDateParser().parseLocalDate(s);
        }
        
    }
    
    @Component
    public static class LocalToStringDataConverter implements Converter<LocalDate, String> {

        @Override
        public String convert(LocalDate s) {
           return s.toString();
        }
        
    }
}
