/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.quote.utils;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author paul.ye
 */
public class SkynetStaticUtil {
    
    public static Set<String> getValidPriorLiabilityLimit(){
        Set<String> validPriorLiabilityLimit = new HashSet<>();
        validPriorLiabilityLimit.add("100/100");
        validPriorLiabilityLimit.add("100/300");
        validPriorLiabilityLimit.add("1000/1000");
        validPriorLiabilityLimit.add("100csl");
        validPriorLiabilityLimit.add("12.5/25");
        validPriorLiabilityLimit.add("15/30");
        validPriorLiabilityLimit.add("20/40");
        validPriorLiabilityLimit.add("25/25");
        validPriorLiabilityLimit.add("25/50");
        validPriorLiabilityLimit.add("250/500");
        validPriorLiabilityLimit.add("300/300");
        validPriorLiabilityLimit.add("50/100");
        validPriorLiabilityLimit.add("50/50");
        validPriorLiabilityLimit.add("500/500");
        validPriorLiabilityLimit.add("500csl");
        validPriorLiabilityLimit.add("none");
        validPriorLiabilityLimit.add("state minimum");
        return validPriorLiabilityLimit;
    }
    
    public static Set<String> getUMCoverage(){
        Set<String> umCoverages = new HashSet<>();
        umCoverages.add("100/100");
        umCoverages.add("100/300");
        umCoverages.add("1000/1000");
        umCoverages.add("15/30");
        umCoverages.add("20/40");
        umCoverages.add("25/25");
        umCoverages.add("25/50");
        umCoverages.add("250/500");
        umCoverages.add("300/300");
        umCoverages.add("50/100");
        umCoverages.add("50/50");
        umCoverages.add("500/500");
        umCoverages.add("reject");
        umCoverages.add("state minimum");
        return umCoverages;
    }
    
    public static Set<String> getUIMCoverage(){
        return getUMCoverage();
    }
    
    public static Set<String> getGenders(){
        Set<String> genders = new HashSet<>();
        genders.add("male");
        genders.add("female");
        return genders;
    }
    
    public static Set<String> getBICoverage(){
        Set<String> biCoverages = getUMCoverage();
        biCoverages.remove("reject");
        return biCoverages;
    }
    
    public static Set<String> getV1Ownership(){
        Set<String> voship = new HashSet<>();
        voship.add("leased");
        voship.add("lien");
        voship.add("owned");
        return voship;
    }
    
    public static Set<String> getMPCoverage(){
        Set<String> mpCoverages = new HashSet<>();
        mpCoverages.add("1000");
        mpCoverages.add("10000");
        mpCoverages.add("100000");
        mpCoverages.add("15000");
        mpCoverages.add("2000");
        mpCoverages.add("2500");
        mpCoverages.add("500");
        mpCoverages.add("5000");
        mpCoverages.add("50000");
        mpCoverages.add("none");
        return mpCoverages;
    }
    
    public static Set<String> getHomeOwnership(){
        Set<String> homeOwnership = new HashSet<>();
        homeOwnership.add("apartment");
        homeOwnership.add("condo (owned)");
        homeOwnership.add("home (owned)");
        homeOwnership.add("live with parents");
        homeOwnership.add("mobile home");
        homeOwnership.add("other");
        homeOwnership.add("rental home/condo");
        return homeOwnership;
    }
    
    public static Set<String> getMaritalStatus(){
        Set<String> maritalStatus = new HashSet<>();
        maritalStatus.add("divorced");
        maritalStatus.add("domestic partner");
        maritalStatus.add("married");
        maritalStatus.add("separated");
        maritalStatus.add("single");
        maritalStatus.add("widowed");
        return maritalStatus;
    }
    
    public static Set<String> getV1Make(){
	
        Set<String> v1Make = new HashSet<>();
        v1Make.add("acura");
        v1Make.add("mercedes benz");
        v1Make.add("audi");
        v1Make.add("bmw");
        v1Make.add("buick");
        v1Make.add("cadillac");
        v1Make.add("chevrolet");
        v1Make.add("chrysler");
        v1Make.add("daewoo");
        v1Make.add("dodge");
        v1Make.add("eagle");
        v1Make.add("ford");
        v1Make.add("geo");
        v1Make.add("gmc");
        v1Make.add("honda");
        v1Make.add("hummer");
        v1Make.add("hyundai");
        v1Make.add("infiniti");
        v1Make.add("isuzu");
        v1Make.add("jaguar");
        v1Make.add("jeep");
        v1Make.add("kia");
        v1Make.add("land rover");
        v1Make.add("lexus");
        v1Make.add("lincoln");
        v1Make.add("mazda");
        v1Make.add("mercury");
        v1Make.add("mini");
        v1Make.add("mitsubishi");
        v1Make.add("nissan");
        v1Make.add("oldsmobile");
        v1Make.add("plymouth");
        v1Make.add("pontiac");
        v1Make.add("porsche");
        v1Make.add("ram");
        v1Make.add("saab");
        v1Make.add("saturn");
        v1Make.add("subaru");
        v1Make.add("suzuki");
        v1Make.add("toyota");
        v1Make.add("volkswagen");
        v1Make.add("volvo");
        return v1Make;
    }
    
    public static Set<String> getPriorCarrier(){
        
        Set<String> priorCarriers = new HashSet<>();
        priorCarriers.add("21st century");
        priorCarriers.add("a.central");
        priorCarriers.add("aaa");
        priorCarriers.add("aarp");
        priorCarriers.add("acadia");
        priorCarriers.add("access general");
        priorCarriers.add("ace");
        priorCarriers.add("acuity");
        priorCarriers.add("adirondack ins exchange");
        priorCarriers.add("alfa alliance");
        priorCarriers.add("allied");
        priorCarriers.add("allstate");
        priorCarriers.add("america first");
        priorCarriers.add("american commerce");
        priorCarriers.add("american family");
        priorCarriers.add("american national");
        priorCarriers.add("amica");
        priorCarriers.add("auto-owners");
        priorCarriers.add("badger mutual");
        priorCarriers.add("balboa");
        priorCarriers.add("beacon national");
        priorCarriers.add("bristol west");
        priorCarriers.add("capital insurance group");
        priorCarriers.add("central mutual of oh");
        priorCarriers.add("colonial penn");
        priorCarriers.add("countrywide");
        priorCarriers.add("dairyland");
        priorCarriers.add("direct");
        priorCarriers.add("emc");
        priorCarriers.add("electric");
        priorCarriers.add("erie");
        priorCarriers.add("esurance");
        priorCarriers.add("explorer");
        priorCarriers.add("farm bureau");
        priorCarriers.add("farmers");
        priorCarriers.add("fidelity");
        priorCarriers.add("first american");
        priorCarriers.add("foremost");
        priorCarriers.add("gmac");
        priorCarriers.add("geico");
        priorCarriers.add("general casualty");
        priorCarriers.add("germantown mutual");
        priorCarriers.add("grange");
        priorCarriers.add("grinnell");
        priorCarriers.add("guide one");
        priorCarriers.add("hallmark insurance company");
        priorCarriers.add("hanover");
        priorCarriers.add("hartford");
        priorCarriers.add("hastings mutual");
        priorCarriers.add("ifa");
        priorCarriers.add("infinity");
        priorCarriers.add("kemper");
        priorCarriers.add("liberty mutual");
        priorCarriers.add("mendota");
        priorCarriers.add("mercury");
        priorCarriers.add("metlife");
        priorCarriers.add("metropolitan");
        priorCarriers.add("nationwide");
        priorCarriers.add("no prior insurance");
        priorCarriers.add("ohio casualty");
        priorCarriers.add("omni insurance co");
        priorCarriers.add("other non-standard");
        priorCarriers.add("other standard");
        priorCarriers.add("palisades");
        priorCarriers.add("pekin");
        priorCarriers.add("pemco");
        priorCarriers.add("penn national");
        priorCarriers.add("proformance");
        priorCarriers.add("progressive");
        priorCarriers.add("rockford mutual");
        priorCarriers.add("secura");
        priorCarriers.add("safeco");
        priorCarriers.add("shelter insurance");
        priorCarriers.add("star casualty");
        priorCarriers.add("state auto");
        priorCarriers.add("state farm");
        priorCarriers.add("travelers");
        priorCarriers.add("usaa");
        priorCarriers.add("united fire and casualty");
        priorCarriers.add("unitrin");
        priorCarriers.add("victoria");
        priorCarriers.add("west bend");
        priorCarriers.add("westfield");
        return priorCarriers;
    }
    
    public static Set<String> getStates(){
        Set<String> states = new HashSet<>();
        states.add("md");
        states.add("ri");
        states.add("pa");
        states.add("tx");
        states.add("az");
        states.add("ca");
        states.add("co");
        states.add("nc");
        states.add("oh");
        states.add("va");
        states.add("ga");
        states.add("il");
        states.add("mo");
        states.add("ny");
        states.add("wa");
        states.add("al");
        states.add("ct");
        states.add("fl");
        states.add("ks");
        states.add("mn");
        states.add("nv");
        states.add("nj");
        states.add("sc");
        states.add("tn");
        states.add("ut");
        states.add("wi");
        states.add("ar");
        states.add("in");
        states.add("ms");
        states.add("nm");
        states.add("ok");
        states.add("or");
        states.add("wv");
        states.add("ia");
        states.add("nh");
        states.add("vt");
        states.add("ky");
        states.add("me");
        
        return states;
    }
    
    public static int[] getPDCoveragesRange(){
        return new int[]{-1,500000};
    }
    
    public static int[] getV1CollisionDeductibleRange(){
        return new int[]{0, 1500};
    }
    
    public static int[] getV1OtherCollisionDeductible(){
        return new int[]{0, 1500};
    }
    
    public static int[] getV1CostNew(){
        return new int[]{0, 999999};
    }
    
    public static int[] getYearsWithPriorCarrier(){
        return new int[]{0, 99};
    }
    
    public static int[] getD1YearsLicensed(){
        return new int[]{0, 99};
    }
    
    public static int[] getD1Age(){
        return new int[]{18,99};
    }
    
    public static int[] getV1Age(){
        return new int[]{-1, 99};
    }
    
    public static int[] getCountDrivers(){
        return new int[]{1, 4};
    }
    
    public static int[] getCountVehicles(){
        return new int[]{1, 4};
    }
}
