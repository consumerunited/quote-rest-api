package com.goji.quote.utils;

import java.net.*;
import java.io.*;

public class URLLib {

    public static StringBuffer urlopen(String url, String cType, String args) {
        try {
            URLConnection connection = new URL(url).openConnection();
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //connection.setRequestProperty("Content-Type", cType + "; charset=utf-8");
            connection.setRequestProperty("Accept", cType);
            connection.setRequestProperty("Method", "GET");
            connection.setDoOutput(true);
            OutputStream os = connection.getOutputStream();
            os.write(args.toString().getBytes("UTF-8"));
//            os.write(args.toString().getBytes("ISO-8859-1"));
            os.close();
            StringBuffer sb = new StringBuffer();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getURL().openStream()));
            String str = null;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
            in.close();
            return sb;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
