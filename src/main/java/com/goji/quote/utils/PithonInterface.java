/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.quote.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author paul.ye
 */
public interface PithonInterface {

    @FunctionalInterface
    public interface GetValueFromX<T> {

        public T get(String name, T defaultValue, Map<String, Object> x);
    }

    @FunctionalInterface
    public interface Calculator {

        public double calculate(double x);
    }
    
    @FunctionalInterface
    public interface StringPredicator {
        
        public boolean validate(String str);
    }
    
    @FunctionalInterface
    public interface CSVMapper<I,D>{
        public Map<I,D> map(List<String[]> data, int keyColumn);
    }
    
    @FunctionalInterface
    public interface CSVLister<T>{
        public List<T> list(List<String[]> data);
    }


    /**
     * python.string.title
     *
     * @param str
     * @return
     */
    default String title(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        char[] cs = str.toCharArray();
        boolean toTitle = true;
        for (int i = 0; i < cs.length; i++) {
            char c = cs[i];
            //[a-z] capitalize it and set toTitle to false
            if (c >= 97 && c <= 122 && toTitle) {
                c -= 32;
                cs[i] = c;
                toTitle = false;
            } else if (c < 65 || (c > 90 && c < 97) || c > 122) { //non word, set toTitle true in order to capitalize next word
                toTitle = true;
            }
            sb.append(cs[i]);
        }
        return sb.toString();
    }
    
    default List<String> split(String str){
        List<String> res = new ArrayList<>();
        if(str==null || str.isEmpty()){
            return res;
        }
        StringBuilder sb = new StringBuilder();
        char[] cs = str.toCharArray();
        for(char c : cs){
            if((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97 && c<=122)){
                //[a-zA-Z0-9]
                sb.append(c);
            } else {
                if(sb.length()>0){
                    res.add(sb.toString());
                    sb = new StringBuilder();
                } 
                //else do nothing
            }
        }
        if(sb.length()>0){
            res.add(sb.toString());
            sb = new StringBuilder();
        } 
        return res;
    }

    default boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    default List<String[]> readCSV(String file, String cvsSplitBy){
        List<String[]> data = new ArrayList<>();
        BufferedReader br = null;
        String line = "";
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] nextLine = line.split(cvsSplitBy);
                data.add(nextLine);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }

}
