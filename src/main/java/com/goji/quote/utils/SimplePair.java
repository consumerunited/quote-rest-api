/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.goji.quote.utils;

import java.util.Map;

/**
 *
 * @author paul.ye
 */
public class SimplePair<K, V> implements Map.Entry<K, V> {

    private K key;
    private V value;

    public SimplePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    SimplePair() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public K getKey() {
        return this.key;
    }

    @Override
    public V getValue() {
        return this.value;
    }

    public K setKey(K key) {
        return this.key = key;
    }

    @Override
    public V setValue(V value) {
        return this.value = value;
    }

}
