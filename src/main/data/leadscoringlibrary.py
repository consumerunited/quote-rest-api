#!/usr/bin/env python
"""Provides functions for scoring incoming leads and returns ALQME LS Index Score"""

__author__='Sean Parenti'
__copyright__="Copyright 2015, Goji"

__version__='0.1'
__email__='sean.parenti@goji.com'
__status__='Testing'

import pandas as pd
import os
import numpy as np
import joblib
import MySQLdb as db
path = os.path.dirname(__file__)+'/'
#predicts conversion probability. Requires skynet lead data and annualized premium from quote estimates



def conversion_prediction(data={}):
    """ Predictor for closed_x from model/54b80d01c4063776a200122f
        Reference Conversion Predict from BigML.ipynb
        Predictive model by BigML - Machine Learning Made Easy
    """
    if (data.get('no_prior_ins_index') is None):
        return 0.15298
    if (data['no_prior_ins_index'] > 2):
        if (data.get('annualized_premium') is None):
            return 0.21774
        if (data['annualized_premium'] > 2055.58639):
            if (data['annualized_premium'] > 3763.40348):
                if (data.get('zip') is None):
                    return 0.08946
                if (data['zip'] > 12102):
                    return 0.06294
                if (data['zip'] <= 12102):
                    return 0.13079
            if (data['annualized_premium'] <= 3763.40348):
                if (data.get('zip') is None):
                    return 0.16858
                if (data['zip'] > 12904):
                    if (data.get('requested_coverage_mixed') is None):
                        return 0.15479
                    if (data['requested_coverage_mixed'] > 5):
                        return 0.05521
                    if (data['requested_coverage_mixed'] <= 5):
                        if (data.get('count_trucks') is None):
                            return 0.15534
                        if (data['count_trucks'] > 2):
                            return 1
                        if (data['count_trucks'] <= 2):
                            if (data['annualized_premium'] > 2567.16656):
                                if (data.get('count_drivers') is None):
                                    return 0.12615
                                if (data['count_drivers'] > 1):
                                    if (data.get('driver_1_age') is None):
                                        return 0.21053
                                    if (data['driver_1_age'] > 62):
                                        return 1
                                    if (data['driver_1_age'] <= 62):
                                        if (data.get('driver_1_credit_mixed') is None):
                                            return 0.19847
                                        if (data['driver_1_credit_mixed'] > 2):
                                            return 0.17742
                                        if (data['driver_1_credit_mixed'] <= 2):
                                            if (data['annualized_premium'] > 3107.47333):
                                                return 0
                                            if (data['annualized_premium'] <= 3107.47333):
                                                return 1
                                if (data['count_drivers'] <= 1):
                                    return 0.11096
                            if (data['annualized_premium'] <= 2567.16656):
                                if (data.get('vehicle_1_est_commute_miles') is None):
                                    return 0.17729
                                if (data['vehicle_1_est_commute_miles'] > 9):
                                    return 0.21381
                                if (data['vehicle_1_est_commute_miles'] <= 9):
                                    return 0.13452
                if (data['zip'] <= 12904):
                    if (data.get('current_resident_status_new') is None):
                        return 0.21253
                    if (data['current_resident_status_new'] == 'Other'):
                        if (data.get('pd_coverage_mixed') is None):
                            return 0.29299
                        if (data['pd_coverage_mixed'] > 5):
                            if (data['zip'] > 12280):
                                return 1
                            if (data['zip'] <= 12280):
                                return 0.10256
                        if (data['pd_coverage_mixed'] <= 5):
                            return 0.3875
                    if (data['current_resident_status_new'] != 'Other'):
                        return 0.17032
        if (data['annualized_premium'] <= 2055.58639):
            if (data['annualized_premium'] > 801.14721):
                if (data.get('vehicle_1_desired_coll_mixed') is None):
                    return 0.23016
                if (data['vehicle_1_desired_coll_mixed'] > 8):
                    if (data.get('bi_coverage_mixed') is None):
                        return 0.23889
                    if (data['bi_coverage_mixed'] > 7):
                        if (data.get('vehicle_1_est_commute_miles') is None):
                            return 0.11211
                        if (data['vehicle_1_est_commute_miles'] > 4):
                            return 0.09439
                        if (data['vehicle_1_est_commute_miles'] <= 4):
                            if (data.get('zip') is None):
                                return 0.25
                            if (data['zip'] > 74161):
                                if (data.get('vehicle_1_est_annual_miles') is None):
                                    return 0.43478
                                if (data['vehicle_1_est_annual_miles'] > 11250):
                                    if (data['zip'] > 77444):
                                        return 0
                                    if (data['zip'] <= 77444):
                                        return 1
                                if (data['vehicle_1_est_annual_miles'] <= 11250):
                                    if (data['annualized_premium'] > 1869.11429):
                                        return 1
                                    if (data['annualized_premium'] <= 1869.11429):
                                        return 0.15385
                            if (data['zip'] <= 74161):
                                return 0.04762
                    if (data['bi_coverage_mixed'] <= 7):
                        if (data.get('driver_1_years_licensed') is None):
                            return 0.2477
                        if (data['driver_1_years_licensed'] > 25):
                            return 0.17898
                        if (data['driver_1_years_licensed'] <= 25):
                            if (data['bi_coverage_mixed'] > 6):
                                if (data['annualized_premium'] > 1531.01568):
                                    return 0.23077
                                if (data['annualized_premium'] <= 1531.01568):
                                    if (data.get('vehicle_1_age_old') is None):
                                        return 0.36803
                                    if (data['vehicle_1_age_old'] > 2):
                                        if (data.get('driver_1_highest_degree_mixed') is None):
                                            return 0.35547
                                        if (data['driver_1_highest_degree_mixed'] > 5):
                                            return 0.77778
                                        if (data['driver_1_highest_degree_mixed'] <= 5):
                                            if (data['driver_1_years_licensed'] > 7):
                                                if (data.get('vehicle_1_est_commute_miles') is None):
                                                    return 0.35744
                                                if (data['vehicle_1_est_commute_miles'] > 45):
                                                    return 0
                                                if (data['vehicle_1_est_commute_miles'] <= 45):
                                                    return 0.35325
                                            if (data['driver_1_years_licensed'] <= 7):
                                                return 0
                                    if (data['vehicle_1_age_old'] <= 2):
                                        if (data['driver_1_years_licensed'] > 16):
                                            if (data['annualized_premium'] > 1048.99952):
                                                return 0
                                            if (data['annualized_premium'] <= 1048.99952):
                                                return 1
                                        if (data['driver_1_years_licensed'] <= 16):
                                            if (data['annualized_premium'] > 1063.41167):
                                                return 1
                                            if (data['annualized_premium'] <= 1063.41167):
                                                return 0
                            if (data['bi_coverage_mixed'] <= 6):
                                if (data.get('vehicle_1_est_annual_miles') is None):
                                    return 0.20726
                                if (data['vehicle_1_est_annual_miles'] > 11500):
                                    return 0.08497
                                if (data['vehicle_1_est_annual_miles'] <= 11500):
                                    if (data.get('zip') is None):
                                        return 0.25941
                                    if (data['zip'] > 18226):
                                        if (data['annualized_premium'] > 1703.14108):
                                            return 0.08333
                                        if (data['annualized_premium'] <= 1703.14108):
                                            if (data.get('vehicle_1_age_old') is None):
                                                return 0.26667
                                            if (data['vehicle_1_age_old'] > 5):
                                                return 0.2375
                                            if (data['vehicle_1_age_old'] <= 5):
                                                if (data['vehicle_1_age_old'] > 1):
                                                    if (data.get('driver_1_gender') is None):
                                                        return 0.69231
                                                    if (data['driver_1_gender'] == 'Male'):
                                                        if (data['annualized_premium'] > 1171.235):
                                                            return 0
                                                        if (data['annualized_premium'] <= 1171.235):
                                                            return 1
                                                    if (data['driver_1_gender'] != 'Male'):
                                                        return 1
                                                if (data['vehicle_1_age_old'] <= 1):
                                                    return 0
                                    if (data['zip'] <= 18226):
                                        if (data.get('driver_1_gender') is None):
                                            return 0.47826
                                        if (data['driver_1_gender'] == 'Male'):
                                            return 0.1
                                        if (data['driver_1_gender'] != 'Male'):
                                            if (data['driver_1_years_licensed'] > 21):
                                                return 0
                                            if (data['driver_1_years_licensed'] <= 21):
                                                return 0.90909
                if (data['vehicle_1_desired_coll_mixed'] <= 8):
                    if (data['annualized_premium'] > 917.98695):
                        return 0.13678
                    if (data['annualized_premium'] <= 917.98695):
                        if (data.get('vehicle_1_model_type') is None):
                            return 0.2446
                        if (data['vehicle_1_model_type'] == 'SUV'):
                            if (data.get('driver_1_gender') is None):
                                return 0.39394
                            if (data['driver_1_gender'] == 'Male'):
                                if (data.get('driver_1_age') is None):
                                    return 0.72727
                                if (data['driver_1_age'] > 58):
                                    return 0
                                if (data['driver_1_age'] <= 58):
                                    return 1
                            if (data['driver_1_gender'] != 'Male'):
                                return 0.22727
                        if (data['vehicle_1_model_type'] != 'SUV'):
                            return 0.18182
            if (data['annualized_premium'] <= 801.14721):
                if (data.get('driver_1_age') is None):
                    return 0.34352
                if (data['driver_1_age'] > 40):
                    if (data['annualized_premium'] > 574.69699):
                        if (data.get('driver_1_years_licensed') is None):
                            return 0.2522
                        if (data['driver_1_years_licensed'] > 21):
                            if (data.get('zip') is None):
                                return 0.19549
                            if (data['zip'] > 10386):
                                if (data.get('vehicle_1_age_old') is None):
                                    return 0.18939
                                if (data['vehicle_1_age_old'] > 6):
                                    if (data.get('vehicle_1_est_annual_miles') is None):
                                        return 0.2094
                                    if (data['vehicle_1_est_annual_miles'] > 4000):
                                        return 0.19824
                                    if (data['vehicle_1_est_annual_miles'] <= 4000):
                                        if (data['vehicle_1_age_old'] > 12):
                                            return 1
                                        if (data['vehicle_1_age_old'] <= 12):
                                            return 0
                                if (data['vehicle_1_age_old'] <= 6):
                                    return 0
                            if (data['zip'] <= 10386):
                                return 1
                        if (data['driver_1_years_licensed'] <= 21):
                            return 0.33714
                    if (data['annualized_premium'] <= 574.69699):
                        if (data.get('vehicle_1_age_old') is None):
                            return 0.41714
                        if (data['vehicle_1_age_old'] > 26):
                            return 1
                        if (data['vehicle_1_age_old'] <= 26):
                            if (data.get('vehicle_1_model_type') is None):
                                return 0.40123
                            if (data['vehicle_1_model_type'] == 'Truck'):
                                if (data.get('zip') is None):
                                    return 0.59259
                                if (data['zip'] > 31866):
                                    if (data['driver_1_age'] > 52):
                                        if (data.get('driver_1_highest_degree_mixed') is None):
                                            return 0.69231
                                        if (data['driver_1_highest_degree_mixed'] > 3):
                                            return 0
                                        if (data['driver_1_highest_degree_mixed'] <= 3):
                                            return 0.81818
                                    if (data['driver_1_age'] <= 52):
                                        if (data['annualized_premium'] > 558.6425):
                                            return 1
                                        if (data['annualized_premium'] <= 558.6425):
                                            return 0
                                if (data['zip'] <= 31866):
                                    return 1
                            if (data['vehicle_1_model_type'] != 'Truck'):
                                if (data.get('requested_coverage_mixed') is None):
                                    return 0.368
                                if (data['requested_coverage_mixed'] > 4):
                                    return 0.22857
                                if (data['requested_coverage_mixed'] <= 4):
                                    if (data.get('vehicle_1_est_commute_miles') is None):
                                        return 0.48438
                                    if (data['vehicle_1_est_commute_miles'] > 14):
                                        return 0
                                    if (data['vehicle_1_est_commute_miles'] <= 14):
                                        return 0.59524
                if (data['driver_1_age'] <= 40):
                    if (data.get('vehicle_1_est_commute_miles') is None):
                        return 0.39972
                    if (data['vehicle_1_est_commute_miles'] > 13):
                        if (data.get('zip') is None):
                            return 0.21277
                        if (data['zip'] > 96962):
                            return 1
                        if (data['zip'] <= 96962):
                            return 0.18681
                    if (data['vehicle_1_est_commute_miles'] <= 13):
                        return 0.44106
    if (data['no_prior_ins_index'] <= 2):
        if (data.get('annualized_premium') is None):
            return 0.13777
        if (data['annualized_premium'] > 1597.38101):
            if (data.get('pd_coverage_mixed') is None):
                return 0.11332
            if (data['pd_coverage_mixed'] > 6):
                if (data.get('driver_1_gender') is None):
                    return 0.06403
                if (data['driver_1_gender'] == 'Male'):
                    return 0.05217
                if (data['driver_1_gender'] != 'Male'):
                    if (data['annualized_premium'] > 3098.25849):
                        if (data.get('vehicle_1_est_commute_miles') is None):
                            return 0.04266
                        if (data['vehicle_1_est_commute_miles'] > 37):
                            if (data.get('count_trucks') is None):
                                return 0.4
                            if (data['count_trucks'] > 0):
                                return 1
                            if (data['count_trucks'] <= 0):
                                return 0
                        if (data['vehicle_1_est_commute_miles'] <= 37):
                            return 0.03868
                    if (data['annualized_premium'] <= 3098.25849):
                        if (data.get('count_vehicles') is None):
                            return 0.09598
                        if (data['count_vehicles'] > 1):
                            return 0.12922
                        if (data['count_vehicles'] <= 1):
                            return 0.07935
            if (data['pd_coverage_mixed'] <= 6):
                if (data.get('count_drivers') is None):
                    return 0.12339
                if (data['count_drivers'] > 1):
                    if (data.get('current_insurance_coverage_length_mixed') is None):
                        return 0.15714
                    if (data['current_insurance_coverage_length_mixed'] > 2):
                        return 0.12032
                    if (data['current_insurance_coverage_length_mixed'] <= 2):
                        if (data.get('count_cars') is None):
                            return 0.19648
                        if (data['count_cars'] > 0):
                            return 0.17008
                        if (data['count_cars'] <= 0):
                            if (data.get('vehicle_1_desired_coll_mixed') is None):
                                return 0.26599
                            if (data['vehicle_1_desired_coll_mixed'] > 9):
                                return 0.05556
                            if (data['vehicle_1_desired_coll_mixed'] <= 9):
                                return 0.34677
                if (data['count_drivers'] <= 1):
                    if (data['annualized_premium'] > 2250.71777):
                        if (data.get('zip') is None):
                            return 0.09713
                        if (data['zip'] > 15448):
                            if (data['annualized_premium'] > 3857.67551):
                                if (data.get('vehicle_1_model_type') is None):
                                    return 0.05222
                                if (data['vehicle_1_model_type'] == 'Truck'):
                                    return 0.10811
                                if (data['vehicle_1_model_type'] != 'Truck'):
                                    return 0.04875
                            if (data['annualized_premium'] <= 3857.67551):
                                if (data.get('count_cars') is None):
                                    return 0.09002
                                if (data['count_cars'] > 1):
                                    return 0.15123
                                if (data['count_cars'] <= 1):
                                    return 0.08475
                        if (data['zip'] <= 15448):
                            if (data.get('driver_1_gender') is None):
                                return 0.12299
                            if (data['driver_1_gender'] == 'Male'):
                                return 0.1013
                            if (data['driver_1_gender'] != 'Male'):
                                if (data.get('vehicle_1_est_commute_miles') is None):
                                    return 0.15
                                if (data['vehicle_1_est_commute_miles'] > 8):
                                    if (data.get('bi_coverage_mixed') is None):
                                        return 0.15971
                                    if (data['bi_coverage_mixed'] > 7):
                                        return 1
                                    if (data['bi_coverage_mixed'] <= 7):
                                        return 0.15707
                                if (data['vehicle_1_est_commute_miles'] <= 8):
                                    return 0.07805
                    if (data['annualized_premium'] <= 2250.71777):
                        if (data.get('driver_1_gender') is None):
                            return 0.1345
                        if (data['driver_1_gender'] == 'Male'):
                            if (data.get('driver_1_years_licensed') is None):
                                return 0.11853
                            if (data['driver_1_years_licensed'] > 8):
                                if (data.get('vehicle_1_est_commute_miles') is None):
                                    return 0.11287
                                if (data['vehicle_1_est_commute_miles'] > 10):
                                    return 0.08581
                                if (data['vehicle_1_est_commute_miles'] <= 10):
                                    return 0.12296
                            if (data['driver_1_years_licensed'] <= 8):
                                if (data.get('vehicle_1_model_type') is None):
                                    return 0.36364
                                if (data['vehicle_1_model_type'] == 'SUV'):
                                    return 1
                                if (data['vehicle_1_model_type'] != 'SUV'):
                                    if (data.get('zip') is None):
                                        return 0.1875
                                    if (data['zip'] > 78049):
                                        return 1
                                    if (data['zip'] <= 78049):
                                        return 0.07143
                        if (data['driver_1_gender'] != 'Male'):
                            if (data.get('driver_1_highest_degree_mixed') is None):
                                return 0.15309
                            if (data['driver_1_highest_degree_mixed'] > 5):
                                return 0.27273
                            if (data['driver_1_highest_degree_mixed'] <= 5):
                                if (data.get('zip') is None):
                                    return 0.1483
                                if (data['zip'] > 9813):
                                    if (data.get('vehicle_1_est_commute_miles') is None):
                                        return 0.1429
                                    if (data['vehicle_1_est_commute_miles'] > 2):
                                        return 0.14193
                                    if (data['vehicle_1_est_commute_miles'] <= 2):
                                        return 0.36
                                if (data['zip'] <= 9813):
                                    return 0.1983
        if (data['annualized_premium'] <= 1597.38101):
            if (data.get('pd_coverage_mixed') is None):
                return 0.16446
            if (data['pd_coverage_mixed'] > 6):
                if (data['annualized_premium'] > 1330.08827):
                    return 0.07468
                if (data['annualized_premium'] <= 1330.08827):
                    if (data.get('driver_1_age') is None):
                        return 0.11401
                    if (data['driver_1_age'] > 23):
                        if (data.get('driver_1_gender') is None):
                            return 0.11362
                        if (data['driver_1_gender'] == 'Male'):
                            return 0.0984
                        if (data['driver_1_gender'] != 'Male'):
                            if (data.get('zip') is None):
                                return 0.13634
                            if (data['zip'] > 36116):
                                return 0.15465
                            if (data['zip'] <= 36116):
                                return 0.10372
                    if (data['driver_1_age'] <= 23):
                        return 1
            if (data['pd_coverage_mixed'] <= 6):
                if (data.get('current_resident_status_new') is None):
                    return 0.174
                if (data['current_resident_status_new'] == 'Other'):
                    return 0.29536
                if (data['current_resident_status_new'] != 'Other'):
                    if (data['annualized_premium'] > 1031.5221):
                        if (data.get('driver_1_gender') is None):
                            return 0.15682
                        if (data['driver_1_gender'] == 'Male'):
                            return 0.14029
                        if (data['driver_1_gender'] != 'Male'):
                            return 0.17503
                    if (data['annualized_premium'] <= 1031.5221):
                        if (data.get('vehicle_1_age_old') is None):
                            return 0.20866
                        if (data['vehicle_1_age_old'] > 13):
                            return 0.28148
                        if (data['vehicle_1_age_old'] <= 13):
                            if (data.get('vehicle_1_est_annual_miles') is None):
                                return 0.18881
                            if (data['vehicle_1_est_annual_miles'] > 4500):
                                if (data.get('driver_1_age') is None):
                                    return 0.18496
                                if (data['driver_1_age'] > 22):
                                    if (data.get('count_vans') is None):
                                        return 0.18373
                                    if (data['count_vans'] > 1):
                                        return 1
                                    if (data['count_vans'] <= 1):
                                        if (data['pd_coverage_mixed'] > 1):
                                            if (data.get('driver_1_credit_mixed') is None):
                                                return 0.1753
                                            if (data['driver_1_credit_mixed'] > 5):
                                                return 0.10849
                                            if (data['driver_1_credit_mixed'] <= 5):
                                                if (data.get('vehicle_1_desired_comp_mixed') is None):
                                                    return 0.18888
                                                if (data['vehicle_1_desired_comp_mixed'] > 8):
                                                    return 0.10078
                                                if (data['vehicle_1_desired_comp_mixed'] <= 8):
                                                    if (data['vehicle_1_est_annual_miles'] > 11625):
                                                        return 0.18182
                                                    if (data['vehicle_1_est_annual_miles'] <= 11625):
                                                        return 0.27551
                                        if (data['pd_coverage_mixed'] <= 1):
                                            return 0.30986
                                if (data['driver_1_age'] <= 22):
                                    return 1
                            if (data['vehicle_1_est_annual_miles'] <= 4500):
                                if (data.get('driver_1_marital_status_new') is None):
                                    return 0.66667
                                if (data['driver_1_marital_status_new'] == 'Married'):
                                    return 0
                                if (data['driver_1_marital_status_new'] != 'Married'):
                                    return 0.85714

def expected_commission(data,carrierlist=None):
    if data['prediction']=='PR':
        return data['annualized_premium']*1.0
    else:
        return data['annualized_premium']*.90



#predicts tier base on NS/ST. Reference: Tier Prediction from BigML
def tier_prediction(data={}):
    if (data.get('no_prior_ins_index') is None):
        return 'NS'
    if (data['no_prior_ins_index'] > 2):
        if (data.get('vehicle_1_age_old') is None):
            return 'NS'
        if (data['vehicle_1_age_old'] > 8):
            if (data.get('count_vehicles') is None):
                return 'NS'
            if (data['count_vehicles'] > 1):
                if (data.get('current_resident_status_new') is None):
                    return 'NS'
                if (data['current_resident_status_new'] == 'Own'):
                    if (data.get('driver_1_years_licensed') is None):
                        return 'PR'
                    if (data['driver_1_years_licensed'] > 40):
                        return 'PR'
                    if (data['driver_1_years_licensed'] <= 40):
                        if (data.get('vehicle_1_est_annual_miles') is None):
                            return 'NS'
                        if (data['vehicle_1_est_annual_miles'] > 13875):
                            return 'PR'
                        if (data['vehicle_1_est_annual_miles'] <= 13875):
                            return 'NS'
                if (data['current_resident_status_new'] != 'Own'):
                    if (data.get('vehicle_1_est_annual_miles') is None):
                        return 'NS'
                    if (data['vehicle_1_est_annual_miles'] > 13125):
                        return 'NS'
                    if (data['vehicle_1_est_annual_miles'] <= 13125):
                        if (data.get('annualized_premium') is None):
                            return 'NS'
                        if (data['annualized_premium'] > 951.95229):
                            if (data.get('count_trucks') is None):
                                return 'NS'
                            if (data['count_trucks'] > 2):
                                return 'PR'
                            if (data['count_trucks'] <= 2):
                                return 'NS'
                        if (data['annualized_premium'] <= 951.95229):
                            return 'NS'
            if (data['count_vehicles'] <= 1):
                if (data.get('annualized_premium') is None):
                    return 'NS'
                if (data['annualized_premium'] > 358.38599):
                    if (data.get('driver_1_age') is None):
                        return 'NS'
                    if (data['driver_1_age'] > 41):
                        if (data.get('current_resident_status_new') is None):
                            return 'NS'
                        if (data['current_resident_status_new'] == 'Own'):
                            if (data.get('driver_1_credit_mixed') is None):
                                return 'NS'
                            if (data['driver_1_credit_mixed'] > 6):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] <= 6):
                                if (data['driver_1_age'] > 50):
                                    if (data['annualized_premium'] > 2709.54):
                                        return 'NS'
                                    if (data['annualized_premium'] <= 2709.54):
                                        if (data.get('driver_1_years_in_occupation') is None):
                                            return 'NS'
                                        if (data['driver_1_years_in_occupation'] > 1):
                                            return 'NS'
                                        if (data['driver_1_years_in_occupation'] <= 1):
                                            return 'PR'
                                if (data['driver_1_age'] <= 50):
                                    if (data.get('vehicle_1_est_commute_miles') is None):
                                        return 'NS'
                                    if (data['vehicle_1_est_commute_miles'] > 19):
                                        return 'NS'
                                    if (data['vehicle_1_est_commute_miles'] <= 19):
                                        return 'NS'
                        if (data['current_resident_status_new'] != 'Own'):
                            if (data.get('bi_coverage_mixed') is None):
                                return 'NS'
                            if (data['bi_coverage_mixed'] > 4):
                                if (data.get('medical_payments_coverage_mixed') is None):
                                    return 'NS'
                                if (data['medical_payments_coverage_mixed'] > 5):
                                    return 'PR'
                                if (data['medical_payments_coverage_mixed'] <= 5):
                                    return 'NS'
                            if (data['bi_coverage_mixed'] <= 4):
                                return 'NS'
                    if (data['driver_1_age'] <= 41):
                        if (data['vehicle_1_age_old'] > 12):
                            if (data['annualized_premium'] > 1935.50896):
                                return 'NS'
                            if (data['annualized_premium'] <= 1935.50896):
                                if (data.get('vehicle_1_est_commute_miles') is None):
                                    return 'NS'
                                if (data['vehicle_1_est_commute_miles'] > 9):
                                    if (data['driver_1_age'] > 17):
                                        if (data.get('vehicle_1_est_annual_miles') is None):
                                            return 'NS'
                                        if (data['vehicle_1_est_annual_miles'] > 20500):
                                            return 'NS'
                                        if (data['vehicle_1_est_annual_miles'] <= 20500):
                                            if (data['vehicle_1_est_annual_miles'] > 19500):
                                                return 'NS'
                                            if (data['vehicle_1_est_annual_miles'] <= 19500):
                                                if (data['vehicle_1_est_annual_miles'] > 12750):
                                                    return 'NS'
                                                if (data['vehicle_1_est_annual_miles'] <= 12750):
                                                    if (data.get('vehicle_1_desired_comp_mixed') is None):
                                                        return 'NS'
                                                    if (data['vehicle_1_desired_comp_mixed'] > 7):
                                                        return 'NS'
                                                    if (data['vehicle_1_desired_comp_mixed'] <= 7):
                                                        return 'NS'
                                    if (data['driver_1_age'] <= 17):
                                        return 'PR'
                                if (data['vehicle_1_est_commute_miles'] <= 9):
                                    return 'NS'
                        if (data['vehicle_1_age_old'] <= 12):
                            if (data.get('count_cars') is None):
                                return 'NS'
                            if (data['count_cars'] > 0):
                                if (data['annualized_premium'] > 1159.48705):
                                    if (data.get('vehicle_1_est_annual_miles') is None):
                                        return 'NS'
                                    if (data['vehicle_1_est_annual_miles'] > 50000):
                                        return 'PR'
                                    if (data['vehicle_1_est_annual_miles'] <= 50000):
                                        if (data.get('driver_1_marital_status_new') is None):
                                            return 'NS'
                                        if (data['driver_1_marital_status_new'] == 'Divorced'):
                                            return 'PR'
                                        if (data['driver_1_marital_status_new'] != 'Divorced'):
                                            if (data.get('vehicle_1_desired_coll_mixed') is None):
                                                return 'NS'
                                            if (data['vehicle_1_desired_coll_mixed'] > 9):
                                                return 'NS'
                                            if (data['vehicle_1_desired_coll_mixed'] <= 9):
                                                return 'NS'
                                if (data['annualized_premium'] <= 1159.48705):
                                    if (data.get('driver_1_credit_mixed') is None):
                                        return 'NS'
                                    if (data['driver_1_credit_mixed'] > 6):
                                        return 'NS'
                                    if (data['driver_1_credit_mixed'] <= 6):
                                        if (data.get('vehicle_1_desired_comp_mixed') is None):
                                            return 'NS'
                                        if (data['vehicle_1_desired_comp_mixed'] > 5):
                                            return 'NS'
                                        if (data['vehicle_1_desired_comp_mixed'] <= 5):
                                            return 'NS'
                            if (data['count_cars'] <= 0):
                                if (data.get('current_resident_status_new') is None):
                                    return 'NS'
                                if (data['current_resident_status_new'] == 'Own'):
                                    return 'NS'
                                if (data['current_resident_status_new'] != 'Own'):
                                    if (data.get('vehicle_1_est_commute_miles') is None):
                                        return 'NS'
                                    if (data['vehicle_1_est_commute_miles'] > 9):
                                        return 'NS'
                                    if (data['vehicle_1_est_commute_miles'] <= 9):
                                        return 'NS'
                if (data['annualized_premium'] <= 358.38599):
                    if (data['annualized_premium'] > 260.03016):
                        return 'NS'
                    if (data['annualized_premium'] <= 260.03016):
                        return 'NS'
        if (data['vehicle_1_age_old'] <= 8):
            if (data.get('current_resident_status_new') is None):
                return 'NS'
            if (data['current_resident_status_new'] == 'Own'):
                if (data.get('driver_1_age') is None):
                    return 'NS'
                if (data['driver_1_age'] > 49):
                    if (data.get('vehicle_1_est_annual_miles') is None):
                        return 'PR'
                    if (data['vehicle_1_est_annual_miles'] > 14500):
                        return 'PR'
                    if (data['vehicle_1_est_annual_miles'] <= 14500):
                        if (data.get('driver_1_credit_mixed') is None):
                            return 'PR'
                        if (data['driver_1_credit_mixed'] > 6):
                            return 'PR'
                        if (data['driver_1_credit_mixed'] <= 6):
                            if (data.get('count_vehicles') is None):
                                return 'PR'
                            if (data['count_vehicles'] > 2):
                                return 'PR'
                            if (data['count_vehicles'] <= 2):
                                if (data.get('annualized_premium') is None):
                                    return 'NS'
                                if (data['annualized_premium'] > 483.08708):
                                    if (data['annualized_premium'] > 3631.9):
                                        return 'NS'
                                    if (data['annualized_premium'] <= 3631.9):
                                        if (data.get('zip') is None):
                                            return 'PR'
                                        if (data['zip'] > 13338):
                                            if (data.get('bi_coverage_mixed') is None):
                                                return 'NS'
                                            if (data['bi_coverage_mixed'] > 5):
                                                return 'PR'
                                            if (data['bi_coverage_mixed'] <= 5):
                                                return 'NS'
                                        if (data['zip'] <= 13338):
                                            return 'PR'
                                if (data['annualized_premium'] <= 483.08708):
                                    return 'NS'
                if (data['driver_1_age'] <= 49):
                    if (data.get('annualized_premium') is None):
                        return 'NS'
                    if (data['annualized_premium'] > 563.74839):
                        if (data.get('count_vehicles') is None):
                            return 'NS'
                        if (data['count_vehicles'] > 1):
                            return 'PR'
                        if (data['count_vehicles'] <= 1):
                            if (data['vehicle_1_age_old'] > 5):
                                if (data.get('vehicle_1_est_commute_miles') is None):
                                    return 'NS'
                                if (data['vehicle_1_est_commute_miles'] > 11):
                                    return 'NS'
                                if (data['vehicle_1_est_commute_miles'] <= 11):
                                    return 'NS'
                            if (data['vehicle_1_age_old'] <= 5):
                                if (data['annualized_premium'] > 1841.39):
                                    return 'NS'
                                if (data['annualized_premium'] <= 1841.39):
                                    return 'PR'
                    if (data['annualized_premium'] <= 563.74839):
                        return 'NS'
            if (data['current_resident_status_new'] != 'Own'):
                if (data.get('driver_1_age') is None):
                    return 'NS'
                if (data['driver_1_age'] > 55):
                    if (data.get('vehicle_1_est_commute_miles') is None):
                        return 'NS'
                    if (data['vehicle_1_est_commute_miles'] > 9):
                        return 'NS'
                    if (data['vehicle_1_est_commute_miles'] <= 9):
                        return 'PR'
                if (data['driver_1_age'] <= 55):
                    if (data.get('annualized_premium') is None):
                        return 'NS'
                    if (data['annualized_premium'] > 787.36355):
                        if (data['annualized_premium'] > 1879.44194):
                            if (data.get('count_vehicles') is None):
                                return 'NS'
                            if (data['count_vehicles'] > 1):
                                return 'NS'
                            if (data['count_vehicles'] <= 1):
                                if (data.get('zip') is None):
                                    return 'NS'
                                if (data['zip'] > 12883):
                                    if (data['zip'] > 62025):
                                        return 'NS'
                                    if (data['zip'] <= 62025):
                                        return 'NS'
                                if (data['zip'] <= 12883):
                                    return 'NS'
                        if (data['annualized_premium'] <= 1879.44194):
                            if (data['vehicle_1_age_old'] > 5):
                                if (data.get('driver_1_highest_degree_mixed') is None):
                                    return 'NS'
                                if (data['driver_1_highest_degree_mixed'] > 4):
                                    return 'NS'
                                if (data['driver_1_highest_degree_mixed'] <= 4):
                                    if (data.get('driver_1_count_accidents') is None):
                                        return 'NS'
                                    if (data['driver_1_count_accidents'] > 0):
                                        return 'NS'
                                    if (data['driver_1_count_accidents'] <= 0):
                                        if (data['driver_1_age'] > 38):
                                            return 'NS'
                                        if (data['driver_1_age'] <= 38):
                                            if (data['annualized_premium'] > 1838.18381):
                                                return 'NS'
                                            if (data['annualized_premium'] <= 1838.18381):
                                                if (data.get('count_vehicles') is None):
                                                    return 'NS'
                                                if (data['count_vehicles'] > 1):
                                                    return 'NS'
                                                if (data['count_vehicles'] <= 1):
                                                    if (data.get('bi_coverage_mixed') is None):
                                                        return 'NS'
                                                    if (data['bi_coverage_mixed'] > 6):
                                                        return 'NS'
                                                    if (data['bi_coverage_mixed'] <= 6):
                                                        return 'NS'
                            if (data['vehicle_1_age_old'] <= 5):
                                if (data.get('vehicle_1_est_annual_miles') is None):
                                    return 'NS'
                                if (data['vehicle_1_est_annual_miles'] > 37750):
                                    return 'NS'
                                if (data['vehicle_1_est_annual_miles'] <= 37750):
                                    if (data['vehicle_1_est_annual_miles'] > 14375):
                                        return 'PR'
                                    if (data['vehicle_1_est_annual_miles'] <= 14375):
                                        if (data.get('medical_payments_coverage_mixed') is None):
                                            return 'NS'
                                        if (data['medical_payments_coverage_mixed'] > 3):
                                            return 'NS'
                                        if (data['medical_payments_coverage_mixed'] <= 3):
                                            return 'NS'
                    if (data['annualized_premium'] <= 787.36355):
                        return 'NS'
    if (data['no_prior_ins_index'] <= 2):
        if (data.get('driver_1_age') is None):
            return 'PR'
        if (data['driver_1_age'] > 44):
            if (data.get('annualized_premium') is None):
                return 'PR'
            if (data['annualized_premium'] > 348.909):
                if (data.get('driver_1_credit_mixed') is None):
                    return 'PR'
                if (data['driver_1_credit_mixed'] > 6):
                    if (data.get('count_vehicles') is None):
                        return 'PR'
                    if (data['count_vehicles'] > 4):
                        return 'PR'
                    if (data['count_vehicles'] <= 4):
                        if (data['count_vehicles'] > 1):
                            if (data.get('zip') is None):
                                return 'PR'
                            if (data['zip'] > 76747):
                                return 'PR'
                            if (data['zip'] <= 76747):
                                if (data.get('vehicle_1_age_old') is None):
                                    return 'PR'
                                if (data['vehicle_1_age_old'] > 7):
                                    return 'PR'
                                if (data['vehicle_1_age_old'] <= 7):
                                    return 'PR'
                        if (data['count_vehicles'] <= 1):
                            if (data.get('vehicle_1_age_old') is None):
                                return 'PR'
                            if (data['vehicle_1_age_old'] > 5):
                                if (data.get('pd_coverage_mixed') is None):
                                    return 'PR'
                                if (data['pd_coverage_mixed'] > 5):
                                    if (data.get('zip') is None):
                                        return 'PR'
                                    if (data['zip'] > 14114):
                                        if (data.get('driver_1_years_licensed') is None):
                                            return 'PR'
                                        if (data['driver_1_years_licensed'] > 14):
                                            return 'PR'
                                        if (data['driver_1_years_licensed'] <= 14):
                                            return 'NS'
                                    if (data['zip'] <= 14114):
                                        return 'PR'
                                if (data['pd_coverage_mixed'] <= 5):
                                    return 'PR'
                            if (data['vehicle_1_age_old'] <= 5):
                                return 'PR'
                if (data['driver_1_credit_mixed'] <= 6):
                    if (data['driver_1_age'] > 53):
                        if (data.get('current_insurance_coverage_length_mixed') is None):
                            return 'PR'
                        if (data['current_insurance_coverage_length_mixed'] > 3):
                            if (data.get('driver_1_marital_status_new') is None):
                                return 'PR'
                            if (data['driver_1_marital_status_new'] == 'Married'):
                                if (data.get('zip') is None):
                                    return 'PR'
                                if (data['zip'] > 82917):
                                    return 'PR'
                                if (data['zip'] <= 82917):
                                    if (data.get('count_vehicles') is None):
                                        return 'PR'
                                    if (data['count_vehicles'] > 3):
                                        return 'PR'
                                    if (data['count_vehicles'] <= 3):
                                        return 'PR'
                            if (data['driver_1_marital_status_new'] != 'Married'):
                                if (data['current_insurance_coverage_length_mixed'] > 5):
                                    return 'PR'
                                if (data['current_insurance_coverage_length_mixed'] <= 5):
                                    if (data.get('vehicle_1_desired_coll_mixed') is None):
                                        return 'PR'
                                    if (data['vehicle_1_desired_coll_mixed'] > 9):
                                        return 'PR'
                                    if (data['vehicle_1_desired_coll_mixed'] <= 9):
                                        return 'PR'
                        if (data['current_insurance_coverage_length_mixed'] <= 3):
                            if (data.get('vehicle_1_age_old') is None):
                                return 'PR'
                            if (data['vehicle_1_age_old'] > 5):
                                if (data.get('count_vehicles') is None):
                                    return 'PR'
                                if (data['count_vehicles'] > 1):
                                    if (data['current_insurance_coverage_length_mixed'] > 2):
                                        return 'PR'
                                    if (data['current_insurance_coverage_length_mixed'] <= 2):
                                        return 'PR'
                                if (data['count_vehicles'] <= 1):
                                    if (data['vehicle_1_age_old'] > 9):
                                        if (data.get('current_resident_status_new') is None):
                                            return 'PR'
                                        if (data['current_resident_status_new'] == 'Rent'):
                                            return 'PR'
                                        if (data['current_resident_status_new'] != 'Rent'):
                                            return 'PR'
                                    if (data['vehicle_1_age_old'] <= 9):
                                        if (data.get('zip') is None):
                                            return 'PR'
                                        if (data['zip'] > 75298):
                                            if (data.get('driver_1_highest_degree_mixed') is None):
                                                return 'PR'
                                            if (data['driver_1_highest_degree_mixed'] > 1):
                                                if (data.get('vehicle_1_est_commute_miles') is None):
                                                    return 'PR'
                                                if (data['vehicle_1_est_commute_miles'] > 15):
                                                    return 'PR'
                                                if (data['vehicle_1_est_commute_miles'] <= 15):
                                                    return 'PR'
                                            if (data['driver_1_highest_degree_mixed'] <= 1):
                                                return 'NS'
                                        if (data['zip'] <= 75298):
                                            if (data['annualized_premium'] > 1453.70422):
                                                return 'PR'
                                            if (data['annualized_premium'] <= 1453.70422):
                                                return 'PR'
                            if (data['vehicle_1_age_old'] <= 5):
                                if (data['annualized_premium'] > 2084.70439):
                                    return 'PR'
                                if (data['annualized_premium'] <= 2084.70439):
                                    if (data.get('driver_1_marital_status_new') is None):
                                        return 'PR'
                                    if (data['driver_1_marital_status_new'] == 'Single'):
                                        if (data.get('vehicle_1_est_commute_miles') is None):
                                            return 'PR'
                                        if (data['vehicle_1_est_commute_miles'] > 14):
                                            return 'PR'
                                        if (data['vehicle_1_est_commute_miles'] <= 14):
                                            return 'PR'
                                    if (data['driver_1_marital_status_new'] != 'Single'):
                                        if (data.get('zip') is None):
                                            return 'PR'
                                        if (data['zip'] > 77018):
                                            return 'PR'
                                        if (data['zip'] <= 77018):
                                            return 'PR'
                    if (data['driver_1_age'] <= 53):
                        if (data.get('count_vehicles') is None):
                            return 'PR'
                        if (data['count_vehicles'] > 1):
                            if (data.get('current_resident_status_new') is None):
                                return 'PR'
                            if (data['current_resident_status_new'] == 'Own'):
                                if (data['driver_1_credit_mixed'] > 4):
                                    if (data.get('requested_coverage_mixed') is None):
                                        return 'PR'
                                    if (data['requested_coverage_mixed'] > 1):
                                        if (data.get('vehicle_1_est_commute_miles') is None):
                                            return 'PR'
                                        if (data['vehicle_1_est_commute_miles'] > 21):
                                            return 'PR'
                                        if (data['vehicle_1_est_commute_miles'] <= 21):
                                            return 'PR'
                                    if (data['requested_coverage_mixed'] <= 1):
                                        return 'PR'
                                if (data['driver_1_credit_mixed'] <= 4):
                                    return 'PR'
                            if (data['current_resident_status_new'] != 'Own'):
                                if (data.get('vehicle_1_desired_comp_mixed') is None):
                                    return 'PR'
                                if (data['vehicle_1_desired_comp_mixed'] > 7):
                                    return 'PR'
                                if (data['vehicle_1_desired_comp_mixed'] <= 7):
                                    return 'PR'
                        if (data['count_vehicles'] <= 1):
                            if (data.get('vehicle_1_age_old') is None):
                                return 'PR'
                            if (data['vehicle_1_age_old'] > 9):
                                if (data.get('pd_coverage_mixed') is None):
                                    return 'PR'
                                if (data['pd_coverage_mixed'] > 1):
                                    if (data.get('driver_1_years_licensed') is None):
                                        return 'PR'
                                    if (data['driver_1_years_licensed'] > 16):
                                        if (data.get('current_resident_status_new') is None):
                                            return 'PR'
                                        if (data['current_resident_status_new'] == 'Rent'):
                                            if (data.get('vehicle_1_est_annual_miles') is None):
                                                return 'PR'
                                            if (data['vehicle_1_est_annual_miles'] > 4500):
                                                if (data.get('vehicle_1_est_commute_miles') is None):
                                                    return 'PR'
                                                if (data['vehicle_1_est_commute_miles'] > 9):
                                                    return 'PR'
                                                if (data['vehicle_1_est_commute_miles'] <= 9):
                                                    return 'PR'
                                            if (data['vehicle_1_est_annual_miles'] <= 4500):
                                                return 'NS'
                                        if (data['current_resident_status_new'] != 'Rent'):
                                            if (data.get('vehicle_1_desired_coll_mixed') is None):
                                                return 'PR'
                                            if (data['vehicle_1_desired_coll_mixed'] > 5):
                                                return 'PR'
                                            if (data['vehicle_1_desired_coll_mixed'] <= 5):
                                                return 'PR'
                                    if (data['driver_1_years_licensed'] <= 16):
                                        if (data['driver_1_age'] > 48):
                                            return 'PR'
                                        if (data['driver_1_age'] <= 48):
                                            return 'NS'
                                if (data['pd_coverage_mixed'] <= 1):
                                    return 'NS'
                            if (data['vehicle_1_age_old'] <= 9):
                                if (data['annualized_premium'] > 1614.89676):
                                    if (data.get('driver_1_marital_status_new') is None):
                                        return 'PR'
                                    if (data['driver_1_marital_status_new'] == 'Married'):
                                        if (data.get('driver_1_years_licensed') is None):
                                            return 'PR'
                                        if (data['driver_1_years_licensed'] > 28):
                                            return 'PR'
                                        if (data['driver_1_years_licensed'] <= 28):
                                            return 'PR'
                                    if (data['driver_1_marital_status_new'] != 'Married'):
                                        if (data.get('current_insurance_coverage_length_mixed') is None):
                                            return 'PR'
                                        if (data['current_insurance_coverage_length_mixed'] > 1):
                                            if (data.get('driver_1_years_licensed') is None):
                                                return 'PR'
                                            if (data['driver_1_years_licensed'] > 24):
                                                return 'PR'
                                            if (data['driver_1_years_licensed'] <= 24):
                                                return 'NS'
                                        if (data['current_insurance_coverage_length_mixed'] <= 1):
                                            return 'NS'
                                if (data['annualized_premium'] <= 1614.89676):
                                    if (data.get('pd_coverage_mixed') is None):
                                        return 'PR'
                                    if (data['pd_coverage_mixed'] > 4):
                                        if (data['annualized_premium'] > 534.63643):
                                            if (data.get('current_insurance_coverage_length_mixed') is None):
                                                return 'PR'
                                            if (data['current_insurance_coverage_length_mixed'] > 3):
                                                return 'PR'
                                            if (data['current_insurance_coverage_length_mixed'] <= 3):
                                                if (data.get('current_resident_status_new') is None):
                                                    return 'PR'
                                                if (data['current_resident_status_new'] == 'Rent'):
                                                    return 'PR'
                                                if (data['current_resident_status_new'] != 'Rent'):
                                                    return 'PR'
                                        if (data['annualized_premium'] <= 534.63643):
                                            return 'NS'
                                    if (data['pd_coverage_mixed'] <= 4):
                                        return 'PR'
            if (data['annualized_premium'] <= 348.909):
                if (data['annualized_premium'] > 78.53):
                    if (data['driver_1_age'] > 52):
                        if (data.get('zip') is None):
                            return 'PR'
                        if (data['zip'] > 74830):
                            return 'NS'
                        if (data['zip'] <= 74830):
                            if (data.get('current_insurance_coverage_length_mixed') is None):
                                return 'PR'
                            if (data['current_insurance_coverage_length_mixed'] > 3):
                                return 'PR'
                            if (data['current_insurance_coverage_length_mixed'] <= 3):
                                return 'PR'
                    if (data['driver_1_age'] <= 52):
                        if (data.get('vehicle_1_age_old') is None):
                            return 'NS'
                        if (data['vehicle_1_age_old'] > 25):
                            return 'PR'
                        if (data['vehicle_1_age_old'] <= 25):
                            return 'NS'
                if (data['annualized_premium'] <= 78.53):
                    return 'NS'
        if (data['driver_1_age'] <= 44):
            if (data.get('count_vehicles') is None):
                return 'NS'
            if (data['count_vehicles'] > 1):
                if (data.get('current_resident_status_new') is None):
                    return 'PR'
                if (data['current_resident_status_new'] == 'Own'):
                    if (data.get('annualized_premium') is None):
                        return 'PR'
                    if (data['annualized_premium'] > 868.32933):
                        if (data.get('requested_coverage_mixed') is None):
                            return 'PR'
                        if (data['requested_coverage_mixed'] > 1):
                            if (data.get('driver_1_credit_mixed') is None):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] > 6):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] <= 6):
                                if (data.get('vehicle_1_desired_coll_mixed') is None):
                                    return 'PR'
                                if (data['vehicle_1_desired_coll_mixed'] > 8):
                                    return 'PR'
                                if (data['vehicle_1_desired_coll_mixed'] <= 8):
                                    return 'PR'
                        if (data['requested_coverage_mixed'] <= 1):
                            return 'PR'
                    if (data['annualized_premium'] <= 868.32933):
                        if (data.get('vehicle_1_desired_coll_mixed') is None):
                            return 'PR'
                        if (data['vehicle_1_desired_coll_mixed'] > 8):
                            return 'NS'
                        if (data['vehicle_1_desired_coll_mixed'] <= 8):
                            return 'PR'
                if (data['current_resident_status_new'] != 'Own'):
                    if (data.get('vehicle_1_age_old') is None):
                        return 'PR'
                    if (data['vehicle_1_age_old'] > 3):
                        if (data.get('driver_1_years_licensed') is None):
                            return 'PR'
                        if (data['driver_1_years_licensed'] > 10):
                            if (data.get('annualized_premium') is None):
                                return 'PR'
                            if (data['annualized_premium'] > 941.63178):
                                if (data.get('driver_1_credit_mixed') is None):
                                    return 'PR'
                                if (data['driver_1_credit_mixed'] > 6):
                                    return 'PR'
                                if (data['driver_1_credit_mixed'] <= 6):
                                    if (data.get('current_insurance_coverage_length_mixed') is None):
                                        return 'PR'
                                    if (data['current_insurance_coverage_length_mixed'] > 3):
                                        return 'PR'
                                    if (data['current_insurance_coverage_length_mixed'] <= 3):
                                        if (data.get('bi_coverage_mixed') is None):
                                            return 'PR'
                                        if (data['bi_coverage_mixed'] > 7):
                                            return 'PR'
                                        if (data['bi_coverage_mixed'] <= 7):
                                            return 'NS'
                            if (data['annualized_premium'] <= 941.63178):
                                return 'NS'
                        if (data['driver_1_years_licensed'] <= 10):
                            return 'NS'
                    if (data['vehicle_1_age_old'] <= 3):
                        if (data.get('driver_1_credit_mixed') is None):
                            return 'PR'
                        if (data['driver_1_credit_mixed'] > 2):
                            return 'PR'
                        if (data['driver_1_credit_mixed'] <= 2):
                            return 'NS'
            if (data['count_vehicles'] <= 1):
                if (data.get('vehicle_1_age_old') is None):
                    return 'NS'
                if (data['vehicle_1_age_old'] > 9):
                    if (data.get('annualized_premium') is None):
                        return 'NS'
                    if (data['annualized_premium'] > 275.24913):
                        if (data['vehicle_1_age_old'] > 13):
                            if (data.get('zip') is None):
                                return 'NS'
                            if (data['zip'] > 15739):
                                if (data['driver_1_age'] > 24):
                                    if (data.get('prior_insurance') is None):
                                        return 'NS'
                                    if (data['prior_insurance'] > 0):
                                        if (data.get('driver_1_highest_degree_mixed') is None):
                                            return 'NS'
                                        if (data['driver_1_highest_degree_mixed'] > 1):
                                            if (data.get('requested_coverage_mixed') is None):
                                                return 'NS'
                                            if (data['requested_coverage_mixed'] > 2):
                                                if (data.get('vehicle_1_est_annual_miles') is None):
                                                    return 'NS'
                                                if (data['vehicle_1_est_annual_miles'] > 14375):
                                                    return 'NS'
                                                if (data['vehicle_1_est_annual_miles'] <= 14375):
                                                    return 'NS'
                                            if (data['requested_coverage_mixed'] <= 2):
                                                return 'NS'
                                        if (data['driver_1_highest_degree_mixed'] <= 1):
                                            return 'NS'
                                    if (data['prior_insurance'] <= 0):
                                        return 'NS'
                                if (data['driver_1_age'] <= 24):
                                    return 'NS'
                            if (data['zip'] <= 15739):
                                if (data.get('driver_1_years_licensed') is None):
                                    return 'NS'
                                if (data['driver_1_years_licensed'] > 19):
                                    return 'PR'
                                if (data['driver_1_years_licensed'] <= 19):
                                    return 'NS'
                        if (data['vehicle_1_age_old'] <= 13):
                            if (data['driver_1_age'] > 38):
                                if (data['annualized_premium'] > 553.66558):
                                    if (data.get('pd_coverage_mixed') is None):
                                        return 'NS'
                                    if (data['pd_coverage_mixed'] > 1):
                                        if (data.get('driver_1_count_atfault') is None):
                                            return 'NS'
                                        if (data['driver_1_count_atfault'] > 0):
                                            return 'NS'
                                        if (data['driver_1_count_atfault'] <= 0):
                                            if (data.get('current_resident_status_new') is None):
                                                return 'NS'
                                            if (data['current_resident_status_new'] == 'Own'):
                                                return 'PR'
                                            if (data['current_resident_status_new'] != 'Own'):
                                                return 'NS'
                                    if (data['pd_coverage_mixed'] <= 1):
                                        return 'NS'
                                if (data['annualized_premium'] <= 553.66558):
                                    return 'NS'
                            if (data['driver_1_age'] <= 38):
                                if (data['annualized_premium'] > 498.80018):
                                    if (data['annualized_premium'] > 1531.69194):
                                        if (data.get('current_resident_status_new') is None):
                                            return 'NS'
                                        if (data['current_resident_status_new'] == 'Own'):
                                            return 'NS'
                                        if (data['current_resident_status_new'] != 'Own'):
                                            if (data.get('vehicle_1_est_commute_miles') is None):
                                                return 'NS'
                                            if (data['vehicle_1_est_commute_miles'] > 64):
                                                return 'PR'
                                            if (data['vehicle_1_est_commute_miles'] <= 64):
                                                if (data.get('count_drivers') is None):
                                                    return 'NS'
                                                if (data['count_drivers'] > 2):
                                                    return 'PR'
                                                if (data['count_drivers'] <= 2):
                                                    return 'NS'
                                    if (data['annualized_premium'] <= 1531.69194):
                                        if (data.get('zip') is None):
                                            return 'NS'
                                        if (data['zip'] > 13292):
                                            if (data['zip'] > 20873):
                                                if (data['driver_1_age'] > 28):
                                                    if (data.get('current_resident_status_new') is None):
                                                        return 'NS'
                                                    if (data['current_resident_status_new'] == 'Own'):
                                                        return 'PR'
                                                    if (data['current_resident_status_new'] != 'Own'):
                                                        if (data.get('prior_insurance') is None):
                                                            return 'NS'
                                                        if (data['prior_insurance'] > 0):
                                                            if (data.get('vehicle_1_model_type') is None):
                                                                return 'NS'
                                                            if (data['vehicle_1_model_type'] == 'Van_Minivan'):
                                                                return 'NS'
                                                            if (data['vehicle_1_model_type'] != 'Van_Minivan'):
                                                                if (data.get('driver_1_highest_degree_mixed') is None):
                                                                    return 'NS'
                                                                if (data['driver_1_highest_degree_mixed'] > 5):
                                                                    return 'PR'
                                                                if (data['driver_1_highest_degree_mixed'] <= 5):
                                                                    return 'NS'
                                                        if (data['prior_insurance'] <= 0):
                                                            return 'NS'
                                                if (data['driver_1_age'] <= 28):
                                                    return 'NS'
                                            if (data['zip'] <= 20873):
                                                return 'NS'
                                        if (data['zip'] <= 13292):
                                            return 'PR'
                                if (data['annualized_premium'] <= 498.80018):
                                    return 'NS'
                    if (data['annualized_premium'] <= 275.24913):
                        return 'NS'
                if (data['vehicle_1_age_old'] <= 9):
                    if (data.get('annualized_premium') is None):
                        return 'PR'
                    if (data['annualized_premium'] > 680.61005):
                        if (data['annualized_premium'] > 1778.43715):
                            if (data.get('current_resident_status_new') is None):
                                return 'NS'
                            if (data['current_resident_status_new'] == 'Own'):
                                if (data['no_prior_ins_index'] > 1):
                                    if (data.get('requested_coverage_mixed') is None):
                                        return 'NS'
                                    if (data['requested_coverage_mixed'] > 2):
                                        return 'NS'
                                    if (data['requested_coverage_mixed'] <= 2):
                                        return 'NS'
                                if (data['no_prior_ins_index'] <= 1):
                                    if (data.get('vehicle_1_est_commute_miles') is None):
                                        return 'PR'
                                    if (data['vehicle_1_est_commute_miles'] > 14):
                                        return 'PR'
                                    if (data['vehicle_1_est_commute_miles'] <= 14):
                                        return 'PR'
                            if (data['current_resident_status_new'] != 'Own'):
                                if (data.get('zip') is None):
                                    return 'NS'
                                if (data['zip'] > 15717):
                                    if (data['driver_1_age'] > 36):
                                        if (data.get('count_drivers') is None):
                                            return 'NS'
                                        if (data['count_drivers'] > 1):
                                            return 'PR'
                                        if (data['count_drivers'] <= 1):
                                            if (data['vehicle_1_age_old'] > 1):
                                                if (data.get('vehicle_1_est_annual_miles') is None):
                                                    return 'NS'
                                                if (data['vehicle_1_est_annual_miles'] > 4000):
                                                    if (data.get('current_insurance_coverage_length_mixed') is None):
                                                        return 'NS'
                                                    if (data['current_insurance_coverage_length_mixed'] > 1):
                                                        return 'NS'
                                                    if (data['current_insurance_coverage_length_mixed'] <= 1):
                                                        return 'NS'
                                                if (data['vehicle_1_est_annual_miles'] <= 4000):
                                                    return 'NS'
                                            if (data['vehicle_1_age_old'] <= 1):
                                                return 'PR'
                                    if (data['driver_1_age'] <= 36):
                                        if (data.get('prior_insurance') is None):
                                            return 'NS'
                                        if (data['prior_insurance'] > 0):
                                            if (data['annualized_premium'] > 4631.33167):
                                                return 'NS'
                                            if (data['annualized_premium'] <= 4631.33167):
                                                if (data.get('current_liability_limits_mixed') is None):
                                                    return 'NS'
                                                if (data['current_liability_limits_mixed'] > 5):
                                                    return 'NS'
                                                if (data['current_liability_limits_mixed'] <= 5):
                                                    if (data.get('vehicle_1_est_annual_miles') is None):
                                                        return 'NS'
                                                    if (data['vehicle_1_est_annual_miles'] > 5625):
                                                        if (data.get('vehicle_1_desired_coll_mixed') is None):
                                                            return 'NS'
                                                        if (data['vehicle_1_desired_coll_mixed'] > 8):
                                                            return 'NS'
                                                        if (data['vehicle_1_desired_coll_mixed'] <= 8):
                                                            return 'NS'
                                                    if (data['vehicle_1_est_annual_miles'] <= 5625):
                                                        return 'PR'
                                        if (data['prior_insurance'] <= 0):
                                            return 'NS'
                                if (data['zip'] <= 15717):
                                    if (data['zip'] > 5556):
                                        if (data['no_prior_ins_index'] > 1):
                                            return 'NS'
                                        if (data['no_prior_ins_index'] <= 1):
                                            return 'PR'
                                    if (data['zip'] <= 5556):
                                        return 'NS'
                        if (data['annualized_premium'] <= 1778.43715):
                            if (data.get('driver_1_credit_mixed') is None):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] > 5):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] <= 5):
                                if (data['vehicle_1_age_old'] > 5):
                                    if (data.get('current_insurance_coverage_length_mixed') is None):
                                        return 'PR'
                                    if (data['current_insurance_coverage_length_mixed'] > 1):
                                        if (data.get('driver_1_marital_status_new') is None):
                                            return 'PR'
                                        if (data['driver_1_marital_status_new'] == 'Single'):
                                            if (data['driver_1_age'] > 39):
                                                return 'PR'
                                            if (data['driver_1_age'] <= 39):
                                                if (data.get('current_resident_status_new') is None):
                                                    return 'NS'
                                                if (data['current_resident_status_new'] == 'Other'):
                                                    return 'PR'
                                                if (data['current_resident_status_new'] != 'Other'):
                                                    if (data.get('zip') is None):
                                                        return 'NS'
                                                    if (data['zip'] > 13402):
                                                        if (data['annualized_premium'] > 1743.53):
                                                            return 'NS'
                                                        if (data['annualized_premium'] <= 1743.53):
                                                            if (data.get('pd_coverage_mixed') is None):
                                                                return 'NS'
                                                            if (data['pd_coverage_mixed'] > 4):
                                                                if (data.get('driver_1_highest_degree_mixed') is None):
                                                                    return 'NS'
                                                                if (data['driver_1_highest_degree_mixed'] > 4):
                                                                    return 'PR'
                                                                if (data['driver_1_highest_degree_mixed'] <= 4):
                                                                    return 'NS'
                                                            if (data['pd_coverage_mixed'] <= 4):
                                                                return 'NS'
                                                    if (data['zip'] <= 13402):
                                                        return 'PR'
                                        if (data['driver_1_marital_status_new'] != 'Single'):
                                            if (data.get('vehicle_1_est_annual_miles') is None):
                                                return 'PR'
                                            if (data['vehicle_1_est_annual_miles'] > 13125):
                                                return 'PR'
                                            if (data['vehicle_1_est_annual_miles'] <= 13125):
                                                return 'PR'
                                    if (data['current_insurance_coverage_length_mixed'] <= 1):
                                        if (data['annualized_premium'] > 798.10818):
                                            if (data.get('current_resident_status_new') is None):
                                                return 'NS'
                                            if (data['current_resident_status_new'] == 'Rent'):
                                                return 'NS'
                                            if (data['current_resident_status_new'] != 'Rent'):
                                                return 'PR'
                                        if (data['annualized_premium'] <= 798.10818):
                                            return 'NS'
                                if (data['vehicle_1_age_old'] <= 5):
                                    if (data['driver_1_age'] > 40):
                                        return 'PR'
                                    if (data['driver_1_age'] <= 40):
                                        if (data.get('current_resident_status_new') is None):
                                            return 'PR'
                                        if (data['current_resident_status_new'] == 'Own'):
                                            if (data.get('zip') is None):
                                                return 'PR'
                                            if (data['zip'] > 14718):
                                                if (data['annualized_premium'] > 1567.99951):
                                                    return 'NS'
                                                if (data['annualized_premium'] <= 1567.99951):
                                                    if (data.get('uim_coverage_mixed') is None):
                                                        return 'PR'
                                                    if (data['uim_coverage_mixed'] > 1):
                                                        return 'PR'
                                                    if (data['uim_coverage_mixed'] <= 1):
                                                        return 'NS'
                                            if (data['zip'] <= 14718):
                                                return 'PR'
                                        if (data['current_resident_status_new'] != 'Own'):
                                            if (data.get('zip') is None):
                                                return 'PR'
                                            if (data['zip'] > 13741):
                                                if (data['driver_1_credit_mixed'] > 4):
                                                    if (data['zip'] > 36448):
                                                        if (data['annualized_premium'] > 1722.17409):
                                                            return 'NS'
                                                        if (data['annualized_premium'] <= 1722.17409):
                                                            if (data['zip'] > 37669):
                                                                if (data.get('vehicle_1_est_annual_miles') is None):
                                                                    return 'PR'
                                                                if (data['vehicle_1_est_annual_miles'] > 24000):
                                                                    return 'NS'
                                                                if (data['vehicle_1_est_annual_miles'] <= 24000):
                                                                    if (data.get('bi_coverage_mixed') is None):
                                                                        return 'PR'
                                                                    if (data['bi_coverage_mixed'] > 4):
                                                                        if (data.get('driver_1_count_violations') is None):
                                                                            return 'PR'
                                                                        if (data['driver_1_count_violations'] > 1):
                                                                            return 'NS'
                                                                        if (data['driver_1_count_violations'] <= 1):
                                                                            if (data['zip'] > 94931):
                                                                                return 'PR'
                                                                            if (data['zip'] <= 94931):
                                                                                if (data.get('vehicle_1_est_commute_miles') is None):
                                                                                    return 'PR'
                                                                                if (data['vehicle_1_est_commute_miles'] > 4):
                                                                                    return 'PR'
                                                                                if (data['vehicle_1_est_commute_miles'] <= 4):
                                                                                    return 'PR'
                                                                    if (data['bi_coverage_mixed'] <= 4):
                                                                        return 'NS'
                                                            if (data['zip'] <= 37669):
                                                                return 'PR'
                                                    if (data['zip'] <= 36448):
                                                        if (data['zip'] > 29752):
                                                            return 'NS'
                                                        if (data['zip'] <= 29752):
                                                            return 'PR'
                                                if (data['driver_1_credit_mixed'] <= 4):
                                                    return 'NS'
                                            if (data['zip'] <= 13741):
                                                return 'PR'
                    if (data['annualized_premium'] <= 680.61005):
                        if (data['annualized_premium'] > 102.0025):
                            if (data.get('driver_1_credit_mixed') is None):
                                return 'NS'
                            if (data['driver_1_credit_mixed'] > 6):
                                return 'PR'
                            if (data['driver_1_credit_mixed'] <= 6):
                                if (data.get('zip') is None):
                                    return 'NS'
                                if (data['zip'] > 13661):
                                    if (data['zip'] > 36073):
                                        if (data['zip'] > 82708):
                                            return 'NS'
                                        if (data['zip'] <= 82708):
                                            if (data.get('count_trucks') is None):
                                                return 'NS'
                                            if (data['count_trucks'] > 0):
                                                return 'PR'
                                            if (data['count_trucks'] <= 0):
                                                return 'NS'
                                    if (data['zip'] <= 36073):
                                        return 'NS'
                                if (data['zip'] <= 13661):
                                    return 'PR'
                        if (data['annualized_premium'] <= 102.0025):
                            return 'NS'



def get_commission_score(x):
    comm_max=3585 #95 percentile
    comm_min=1158 # 0 percentile
    index = (x-comm_min)/(comm_max-comm_min)
    index = (lambda x: 1 if x>1 else 0 if x<0 else x)(index)
    return index

def get_conversion_score(x):
    conv_min=.05
    conv_max=.2
    index = (x-conv_min)/(conv_max-conv_min)
    index = (lambda x: 1 if x>1 else 0 if x<0 else x)(index)
    return index



#final function for scoring leads
def score_lead(data,carrierlist,conversion_weight,commission_weight, retention_weight):
    try:
        data['prediction']=tier_prediction(data)
        data['expected_retention']=float(data.get('retention',0))
        data['expected_commission']=expected_commission(data,carrierlist)
        print data['expected_commission']
        data['commission_class']=get_commission_score(data.get('expected_commission'))
        data['expected_conversion']=conversion_prediction(data)
        data['conversion_class']=get_conversion_score(conversion_prediction(data))
        data['alqme_index']=100*((data['conversion_class']*conversion_weight + (data['commission_class']*commission_weight) + data['expected_retention']*retention_weight) /(commission_weight+conversion_weight+retention_weight))
        alqme_results= {'tier_prediction':data['prediction'],
                        'expected_retention': data['expected_retention'],
                        'expected_commission': data['expected_commission'],
                        'commission_class': data['commission_class'],
                        'expected_conversion': data['expected_conversion'],
                        'conversion_class':data['conversion_class'],
                        'alqme_index': data['alqme_index'],
                        'error': 'none'}
        return alqme_results
    except:
        data['prediction']='NS'
        data['expected_retention']=0
        data['expected_commission']=1800.01
        data['expected_conversion']=.120
        data['commission_class']=.3
        data['conversion_class']=.5
        data['alqme_index']=100*((data['conversion_class']*conversion_weight + (data['commission_class']*commission_weight) + data['expected_retention']*retention_weight)/(commission_weight+conversion_weight+retention_weight)        )
        alqme_results= {'tier_prediction':data['prediction'],
                        'expected_retention': data['expected_retention'],
                        'expected_commission': data['expected_commission'],
                        'commission_class': data['commission_class'],
                        'expected_conversion': data['expected_conversion'],
                        'conversion_class':data['conversion_class'],
                        'alqme_index': data['alqme_index'],
                        'error': 'error'}       
        return alqme_results



#bin function, helper function
def percentile_thresholds(data,bins):
    step = 10.0/bins
    if bins*step>100:
        return "Percentiles cannot be over 100"
    else:
        thresholds=pd.Series()
        i=1
        while i<=bins:
            thresholds = thresholds.append(pd.Series(np.percentile(data,i*10*step),index=[i]))
            i+=1
        return thresholds
