/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.goji.quote.rest.api.service;

import com.goji.quote.utils.SimplePair;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.goji.quote.rest.api.legacy.LegacyQuoteRequest;
import com.goji.quote.rs.dto.VehicleOwnership;
import com.goji.quote.utils.PithonInterface.StringPredicator;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * test code: -2: Exception, -1: Negative, 0: default, 1: success;
 * @author paul.ye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        loader = AnnotationConfigContextLoader.class,
        classes = TestConfig.class
)
@ActiveProfiles("local")
public class SkynetTest {
    
    private static final Logger LOG = Logger.getLogger(SkynetTest.class.getName());
    
    @Autowired
    SkyNetService skyNetService;
    
    @Test
    public void testTest(){
        assertNotNull(skyNetService);
    }
    
    /**
     * null, numeric safe.
     */
    @Test
    public void testGetNumericValueFromX(){
        Map<String, Object> x = sampleX1();
        int intRes = (int)(double)skyNetService.getGetNumericValFromX().get("int", 0.0, x);
        double doubleRes = skyNetService.getGetNumericValFromX().get("double", 0.0, x);
        double nullRes = skyNetService.getGetNumericValFromX().get("null", 0.0, x);
        double strRes = -2;
        double objRes = -2;
        try{
            objRes = skyNetService.getGetNumericValFromX().get("obj", 0.0, x);
            assertTrue(false);
            //should throw an exception if fail-safe string predicator failed.
        } catch(Exception ex){
            assertTrue(true);
        }
        try{
            strRes = skyNetService.getGetNumericValFromX().get("string", 0.0, x);
            assertTrue(false);
            //should throw an exception if fail-safe string predicator failed.
        } catch(Exception ex){
            assertTrue(true);
        }
        LOG.info("Test get Numeric from X:\nint: " + intRes + "\ndouble: " + doubleRes + "\nobj:" + objRes + "\nnull:" + nullRes + "\nstr:" + strRes);
    }
    
    /**
     * all safe.
     */
    @Test
    public void testGetStringValueFromX(){
        Map<String, Object> x = sampleX1();
        String strRes = skyNetService.getGetStringValFromX().get("string", "default", x);
        String doubleRes = skyNetService.getGetStringValFromX().get("double", "default", x);
        String intRes = skyNetService.getGetStringValFromX().get("int", "default", x);
        String objRes = skyNetService.getGetStringValFromX().get("obj", "default", x);
        String nullRes = skyNetService.getGetStringValFromX().get("null", "default", x);
        LOG.info("Test get String from X:\nint: " + intRes + "\ndouble: " + doubleRes + "\nobj:" + objRes + "\nnull:" + nullRes + "\nstr:" + strRes);
    }
    
    /**
     * null, numeric, predicated string safe.
     * @throws Exception 
     */
    @Test
    public void testArbitraryFields() throws Exception{
        Map<String, Object> x = sampleX1();
        StringPredicator sp = str -> "test".equalsIgnoreCase(str);
        double doubleRes = skyNetService.getArbitraryField("double", sp, 0.0, -1.0, x);
        double intRes = skyNetService.getArbitraryField("int", sp, 0.0, -1.0, x);
        double strRes = skyNetService.getArbitraryField("string", sp, 0.0, -1.0, x);
        double nullRes = skyNetService.getArbitraryField("null", sp, 0.0, -1.0, x);
        double str2Res = -2.0;
        double objRes = -2.0;
        try{
            str2Res = skyNetService.getArbitraryField("string2", sp, 0.0, -1.0, x);
            assertTrue(false);
            //should throw an exception if fail-safe string predicator failed.
        } catch(Exception ex){
            assertTrue(true);
        }
        try{
            objRes = skyNetService.getArbitraryField("obj", sp, 0.0, -1.0, x);
            assertTrue(false);
            //should throw an exception if fail-safe string predicator failed.
        } catch(Exception ex){
            assertTrue(true);
        }
        LOG.info("Test get Arbitrary from X:\nint: " + intRes + "\ndouble: " + doubleRes + "\nobj:" + objRes + "\nnull:" + nullRes + "\nstr:" + strRes + "\nstr2:" + str2Res);
    
    }
    
    @Test
    public void testGetHomeOwnership(){
        assertEquals(skyNetService.getHomeOwnership("rent"),"apartment");
        assertEquals(skyNetService.getHomeOwnership("rent".toUpperCase()),"apartment");
        assertNull(skyNetService.getHomeOwnership(null));
        assertEquals(skyNetService.getHomeOwnership(Math.random()+""),"home (owned)");
    }
    
    @Test
    public void testGetYearMakeModel(){
        Map<String, Object> x1 = sampleX1();
        //test against non-existing value
        String[] res1 = skyNetService.getYearMakeModel(x1, 5);
        assertEquals(res1[0],"2011");
        assertEquals(res1[1], "honda");
        assertEquals(res1[2], "civic");
        
        //test against null value
        x1.put("vehicle_1_make_new", "jeep");
        x1.put("vehicle_1_model_old", null);
        res1 = skyNetService.getYearMakeModel(x1, 5);
        assertEquals(res1[1], "honda");
        assertEquals(res1[2], "civic");
        
        //test against valid value
        x1.put("vehicle_1_model_old", "commander");
        res1 = skyNetService.getYearMakeModel(x1, 5);
        assertEquals(res1[1], "jeep");
        assertEquals(res1[2], "commander");
        
        //test against invalid value
        x1.put("vehicle_1_make_new", "non-exist make");
        res1 = skyNetService.getYearMakeModel(x1, 5);
        assertEquals(res1[1], "honda");
        assertEquals(res1[2], "civic");
    }
    
    @Test
    public void testBiUimCoverage(){
        Map<String, Object> x1 = sampleX1();
        x1.put("BI_coverage_new", "50/100");
        x1.put("UIM_coverage_new", "100/300");
        SimplePair sp = new SimplePair("BI_coverage", "defaultBILiability");
        
        //test against null or invalid key
        SimplePair spInvalidKey = new SimplePair("BI_noCoverage", "");
        assertNull(skyNetService.getBiUimCoverage(x1, spInvalidKey));
        assertNull(skyNetService.getBiUimCoverage(x1, null));
        
        //test normal case
        String[] res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0],"50/100");
        assertEquals(res[1],"100/300");
        
        //test against minimum, BI/UIM/mincheck with min will result both BI and UIM to state minimum
        x1.put("BI_coverage_new", "State minimum");
        res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0], "state minimum");
        assertEquals(res[1], "state minimum");
        x1.put("BI_coverage_new", "50/100");
        x1.put("UIM_coverage_new", "State minimum");
        res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0], "state minimum");
        assertEquals(res[1], "state minimum");
        x1.put("UIM_coverage_new", "100/300");
        x1.put("requested_coverage_new", "State minimum");
        res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0], "state minimum");
        assertEquals(res[1], "state minimum");
        
        //test against invalid value, BI/UIM not allowed will be set to default Liability
        x1.remove("requested_coverage_new");
        x1.put("BI_coverage_new", "5000000/100000000");
        res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0],"defaultBILiability");
        assertEquals(res[1],"100/300");
        x1.put("BI_coverage_new", "50/100");
        x1.put("UIM_coverage_new", "nonExistingLiability");
        res = skyNetService.getBiUimCoverage(x1, sp);
        assertEquals(res[0],"50/100");
        assertEquals(res[1],"defaultBILiability");
    }
    
    /**
     * PD coverage is only called when BI is set.
     */
    @Test
    public void TestPdCoverage(){
        int min = -1;
        int max = 500000;
        Map<String, Object> x = sampleX1();
        //test normal case
        x.put("PD_coverage_new", 500);
        assertEquals((int)skyNetService.getPdCoverage(x, "100/300", min, max), 500);
        assertEquals((int)skyNetService.getPdCoverage(x, "100/300", min, max), 500);
        
        //test against min
        x.put("PD_coverage_new", Integer.MIN_VALUE);
        assertEquals((int)skyNetService.getPdCoverage(x, "100/300", min, max), 100000);
        
        //test against state minimum
        x.put("PD_coverage_new", "State minimum");
        assertEquals((int)skyNetService.getPdCoverage(x, "100/300", min, max), -1);
        
        //test against unknown string/null, get String is null safe
        x.put("PD_coverage_new", "Unknown String");
        assertEquals((int)skyNetService.getPdCoverage(x, "250/500", min, max), 250000);
        
        //test against overflow case
        //BI overflow, fine
        x.put("PD_coverage_new", null);
        assertEquals((int)skyNetService.getPdCoverage(x, "1000/1000", min, max), 1000000);
        //PD overflow, set to BI
        x.put("PD_coverage_new", Integer.MAX_VALUE);
        assertEquals((int)skyNetService.getPdCoverage(x, "250/500", min, max), 250000);
    }
    
    @Test
    public void TestCompColl() throws Exception{
        Map<String, Object> x = sampleX1();
        x.put("desired_comprehensive_deductible", 500);
        x.put("desired_collision_deductible", 1000);
        LegacyQuoteRequest qd = new LegacyQuoteRequest();
        qd.setV1CostNew(10000);
        qd.setV1Age(2);
        qd.setV1Ownership(VehicleOwnership.OWNED);
        double[] compColl = skyNetService.getCompColl(x, qd);
        assertEquals((int)compColl[0], 500);
        assertEquals((int)compColl[1], 1000);
        
        //test against no coverage
        x.put("desired_comprehensive_deductible", "no coverage");
        x.put("desired_collision_deductible", "No Coverage");
        compColl = skyNetService.getCompColl(x, qd);
        assertEquals((int)compColl[0], 0);
        assertEquals((int)compColl[1], 0);
        
        //test against unknown string
        x.put("desired_collision_deductible", "Non existing coverage");
        try{
            compColl = skyNetService.getCompColl(x, qd);
            assertTrue(false);
        }catch(Exception ex){
            LOG.severe(ex.getMessage());
            assertTrue(true);
        }
    }
    
    @Test
    public void TestParseYearsPrior(){
        String input = "i-=ea=if1- 123 fpd=aa-se450f kflae-f=32k21 psfkae kff!@#$#@^ k-s=3534k "
                + "feafef0)*(^$@hifhu 01-2 )*(*043-Ojf pps_*3923-4- OP_@#_$#_-10-100";
        /**
         * valid words: 123, 012, 39234, 10100, mean = 12367
         * invalid words: aase450f 32k21 3534k feafef0 043Ojf
         */
        double mean = (123.0 + 12 + 39234 + 10100)/4;
        String nanInput = "fejaoef()#*$OEIFNEOS(#U%_@#_#KJF  EIOU @_)_)_KF KJE _E) (#@@JFJE )";
        assertEquals(skyNetService.parseYearsPrior(input), (int)mean);
        assertEquals(skyNetService.parseYearsPrior(nanInput), 0);
        assertEquals(skyNetService.parseYearsPrior("less than 1 Year"), 0);
    }
    
    @Test
    public void testCarCost() throws JsonProcessingException, IOException{
        int year = 2012;
        String make = "honda";
        String model = "civic";
        assertEquals((int)skyNetService.getCarCost(year, make, model), 20849);
    }
    
    @Test
    public void testTitle(){
        assertEquals(skyNetService.title("tada"), "Tada");
    }
    
    private Map<String, Object> sampleX1(){
        String testStr = "test";
        int testInt = 123;
        double testDouble = 123.0;
        LegacyQuoteRequest dummyObj = new LegacyQuoteRequest();
        
        Map<String, Object> MRX = new HashMap<>();
        MRX.put("string", testStr);
        MRX.put("string2", "test2");
        MRX.put("int", testInt);
        MRX.put("double", testDouble);
        MRX.put("obj", dummyObj);
        return MRX;
    }
    
}
