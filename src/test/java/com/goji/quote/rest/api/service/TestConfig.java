package com.goji.quote.rest.api.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.goji.quote.rest.api.config.QuoteRestApiConfig;
import io.dropwizard.jackson.Jackson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Yinzara
 */
@Configuration
@ComponentScan("com.goji")
public class TestConfig {

    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper mapper = Jackson.newObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        return mapper;
    }
    
    @Bean
    public QuoteRestApiConfig config() {
        return new QuoteRestApiConfig();
    }
}
